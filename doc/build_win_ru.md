# Инструкция по сборке для Windows #

Программа написана с использованием библиотеки Qt, поэтому на компьютере должен быть установлен соответствующий инструментарий ([http://www.qt.io/ru/download/](http://www.qt.io/ru/download/))
Сборка программы проводилась на версии Qt 5.4.1. Совместимость с более ранними версиями Qt не гарантируется.

Предполагается, что сборка программы производится из консоли Qt. (Меню пуск -> Все программы -> Qt 5.4.1 -> 5.4 -> MSVC 2013 OpenGL(64-bit) -> Qt 5.4 64-bit for Desktop)
В разных версиях Qt может быть по разному. Также git должен быть прописан в переменной PATH. 

Клонируем репозиторий в корень диска C:\ и переходим в него
```
#!
cd c:\
git clone https://iRyaz@bitbucket.org/iRyaz/leso4osc.git
cd leso4osc
```

Для сборки программы в корне репозитория должны быть: 

* libfftw3-3.dll ([http://www.fftw.org/install/windows.html](http://www.fftw.org/install/windows.html))
* libLESO4.dll ([https://bitbucket.org/iRyaz/leso4_api](https://bitbucket.org/iRyaz/leso4_api))

Утилита qmake сгенерирует make файл
```
#!
qmake LESO4Osc.pro -r -spec win32-g++ "CONFIG+=release
```

Теперь можно выполнить компиляцию программы
```
#!
mingw32-make
```

После компиляции leso4osc.exe будет лежать в директории ./release

# Создание инсталяционного пакета #

Инсталятор для Windows, создается с использованием Inno Setup ([http://www.jrsoftware.org/isdl.php](http://www.jrsoftware.org/isdl.php)). 
Скрипт для создания инсталятора лежит в директории "installer" и называется "winInst.iss". В нем указано, что откомпилированная программа 
со всеми библиотеками лежит в директории bin. Если она не создана, то ее нужно создать. 

В директории bin, вместе с LESOscope.exe должны обязательно быть:

* libLESO4.dll
* libfftw3-3.dll
* libgcc_s_dw2-1.dll
* libstdc++-6.dll
* CALIBR.exe 

CALIBR.exe - программа для калибровки прибора, распространяется вместе с libLESO4

Если используется не статическая версия Qt, то для работы программы потребуются также:

* libgcc_s_dw2-1.dll
* icudt53.dll
* icuin53.dll
* icuuc53.dll
* libstdc++-6.dll
* Qt5Core.dll
* Qt5Gui.dll
* Qt5PrintSupport.dll
* Qt5Widgets.dll

Эти библиотеки идут вместе с Qt.

Открываем winInst.iss с помощью Inno Seup и в меню нажимаем Build -> Complile
В корне будет создан файл типа "lesoscope-0.7b-setup" 
