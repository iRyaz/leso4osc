
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "lesoaboutdialog.h"

LESOAboutDialog::LESOAboutDialog(QWidget *parent) : QDialog(parent)
{
    setLayout(&vLayout);
    setFixedWidth(DIALOG_WIDTH);
    setFixedHeight(DIALOG_HEIGHT);
    aboutLabel = new QTextEdit;
    aboutLabel->setDisabled(false);
    aboutLabel->setReadOnly(true);
    QFile file(":/program_info/about_en.html");
    file.open(QFile::ReadOnly);
    QTextStream stream(&file);
    QString s;
    QString line;
    do {
        line = stream.readLine();
        s += line;
    } while (!line.isNull());
    aboutLabel->insertHtml(s);
    vLayout.addWidget(aboutLabel);
    okButton.setText(tr("OK"));
    buttonLayout.addSpacing(SPACE_POST_OK_BUTTON);
    buttonLayout.addWidget(&okButton);
    vLayout.addLayout(&buttonLayout);
    setModal(true);

    connect(&okButton, SIGNAL(clicked()), this, SLOT(close()));

    currentLang = LANG_EN;
    file.close();
}

LESOAboutDialog::~LESOAboutDialog()
{

}

void LESOAboutDialog::show()
{
    QFile file;
    aboutLabel->clear();

    switch(currentLang)
    {
    case LANG_EN: {
        file.setFileName(":/program_info/about_en.html");
        file.open(QFile::ReadOnly);
        QTextStream stream(&file);
        QString s;
        QString line;
        do {
            line = stream.readLine();
            s += line;
        } while (!line.isNull());
        aboutLabel->insertHtml(s); }
        break;
    case LANG_RU: {
        file.setFileName(":/program_info/about_ru.html");
        file.open(QFile::ReadOnly);
        QTextStream stream(&file);
        QString s;
        QString line;
        do {
            line = stream.readLine();
            s += line;
        } while (!line.isNull());
        aboutLabel->insertHtml(s); }
        break;
    }

    file.close();
    moveToBeginText();
    QDialog::show();
}

void LESOAboutDialog::moveToBeginText()
{
    QTextCursor cursor = aboutLabel->textCursor();
    cursor.movePosition(QTextCursor::Start);
    aboutLabel->setTextCursor(cursor);
}
