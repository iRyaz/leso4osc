<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ChannelPopupWidget</name>
    <message>
        <location filename="ui/channelpopupwidget.cpp" line="37"/>
        <location filename="ui/channelpopupwidget.cpp" line="118"/>
        <source>Signal Offset</source>
        <translation>Смещение сигнала</translation>
    </message>
    <message>
        <location filename="ui/channelpopupwidget.cpp" line="49"/>
        <location filename="ui/channelpopupwidget.cpp" line="117"/>
        <source>Amplifier channel</source>
        <translation>Усиление</translation>
    </message>
</context>
<context>
    <name>ChannelSourceComboBox</name>
    <message>
        <location filename="ui/toolbar/triggertoolbar.h" line="78"/>
        <location filename="ui/toolbar/triggertoolbar.h" line="127"/>
        <source>Channel </source>
        <translation>Канал</translation>
    </message>
</context>
<context>
    <name>ChannelToolBar</name>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="44"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="211"/>
        <source>Max Amplitude</source>
        <translation>Максимальная амплитуда</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="45"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="212"/>
        <source>Input mode</source>
        <translation>Режим входа</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="61"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="213"/>
        <source>Channel </source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="89"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="215"/>
        <source>Open</source>
        <translation>Открытый</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="90"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="216"/>
        <source>Close</source>
        <translation>Закрытый</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="95"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="218"/>
        <source>Off</source>
        <translation>Выкл.</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="96"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="219"/>
        <source>20 V</source>
        <translation>20 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="97"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="220"/>
        <source>8 V</source>
        <translation>8 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="98"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="221"/>
        <source>4 V</source>
        <translation>4 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="99"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="222"/>
        <source>2 V</source>
        <translation>2 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="100"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="223"/>
        <source>0.8 V</source>
        <translation>0,8 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="101"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="224"/>
        <source>0.4 V</source>
        <translation>0,4 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="102"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="225"/>
        <source>0.2 V</source>
        <translation>0,2 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="103"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="226"/>
        <source>0.08 V</source>
        <translation>0,08 В</translation>
    </message>
    <message>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="104"/>
        <location filename="ui/toolbar/channeltoolbar.cpp" line="227"/>
        <source>0.04 V</source>
        <translation>0,04 В</translation>
    </message>
</context>
<context>
    <name>ConnectDeviceDialog</name>
    <message>
        <location filename="ui/connectdevicedialog.ui" line="20"/>
        <source>Select device</source>
        <translation>Выбор устройства</translation>
    </message>
    <message>
        <location filename="ui/connectdevicedialog.ui" line="39"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="ui/connectdevicedialog.ui" line="48"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="ui/connectdevicedialog.ui" line="68"/>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="ui/connectdevicedialog.ui" line="81"/>
        <source>Show this dialog at startup</source>
        <translation>Показывать этот диалог при следующем запуске программы</translation>
    </message>
    <message>
        <location filename="ui/connectdevicedialog.cpp" line="115"/>
        <source>Demo Mode</source>
        <translation>Демо режим</translation>
    </message>
</context>
<context>
    <name>EditMeasureDialog</name>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="14"/>
        <source>EditMeasure</source>
        <translation>Редактировать измерение</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="28"/>
        <source>Channel</source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="35"/>
        <source>Measure</source>
        <translation>Измерение</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="42"/>
        <source>Capture num</source>
        <translation>Число захватов</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="49"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="91"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.ui" line="98"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="54"/>
        <location filename="ui/editmeasuredialog.cpp" line="148"/>
        <source>Amplitude</source>
        <translation>Амплитуда</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="55"/>
        <location filename="ui/editmeasuredialog.cpp" line="149"/>
        <source>Min Value</source>
        <translation>Минимальное значение</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="56"/>
        <location filename="ui/editmeasuredialog.cpp" line="150"/>
        <source>Max Value</source>
        <translation>Максимальное значение</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="57"/>
        <location filename="ui/editmeasuredialog.cpp" line="151"/>
        <source>Middle Value</source>
        <translation>Среднее значение</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="58"/>
        <location filename="ui/editmeasuredialog.cpp" line="152"/>
        <source>Middle Quad Value</source>
        <translation>Средне-квадратическое значение</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="59"/>
        <location filename="ui/editmeasuredialog.cpp" line="153"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="61"/>
        <location filename="ui/editmeasuredialog.cpp" line="155"/>
        <source>Channel A</source>
        <translation>Канал А</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="62"/>
        <location filename="ui/editmeasuredialog.cpp" line="156"/>
        <source>Channel B</source>
        <translation>Канал В</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="63"/>
        <location filename="ui/editmeasuredialog.cpp" line="157"/>
        <source>Channel C</source>
        <translation>Канал С</translation>
    </message>
    <message>
        <location filename="ui/editmeasuredialog.cpp" line="64"/>
        <location filename="ui/editmeasuredialog.cpp" line="158"/>
        <source>Channel D</source>
        <translation>Канал D</translation>
    </message>
</context>
<context>
    <name>LESOAboutDialog</name>
    <message>
        <location filename="lesoaboutdialog.cpp" line="47"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
</context>
<context>
    <name>LESOMainWindow</name>
    <message>
        <location filename="ui/lesomainwindow.ui" line="20"/>
        <source>LESO4 Oscilloscope</source>
        <translation>Осциллограф LESO4</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="46"/>
        <source>&amp;File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="50"/>
        <source>&amp;Save plot</source>
        <translation>Сохранить график</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="64"/>
        <source>&amp;View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="72"/>
        <source>&amp;Measure</source>
        <translation>Измерения</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="80"/>
        <source>&amp;Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="84"/>
        <source>&amp;Samples num</source>
        <translation>Количество отсчетов</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="93"/>
        <source>&amp;Language</source>
        <translation>Язык интерфейса</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="105"/>
        <source>&amp;Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="118"/>
        <source>&amp;Connect Device</source>
        <translation>Подключиться к устройству</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="123"/>
        <source>Print</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="128"/>
        <source>&amp;Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="140"/>
        <source>&amp;Oscilloscope</source>
        <translation>Осциллограф</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="152"/>
        <source>&amp;Spectr</source>
        <translation>Спектр</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="160"/>
        <source>&amp;Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="165"/>
        <source>&amp;Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="170"/>
        <source>&amp;Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="178"/>
        <source>&amp;Measurements Table</source>
        <translation>Таблица измерений</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="183"/>
        <source>&amp;Save signal diagram</source>
        <translation>Сохранить осциллограмму</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="188"/>
        <source>Save spectr &amp;diagram</source>
        <translation>Сохранить спектрограмму</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="199"/>
        <source>&amp;8192</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="207"/>
        <source>&amp;4096</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="215"/>
        <source>&amp;2048</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="223"/>
        <source>&amp;1024</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="231"/>
        <source>Save &amp;device settings</source>
        <translation>Сохранять настройки при следующем запуске </translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="236"/>
        <source>&amp;Reset settings</source>
        <translation>Сбросить настройки</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="241"/>
        <source>&amp;About</source>
        <translation>О Программе</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="252"/>
        <source>&amp;En</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.ui" line="260"/>
        <source>&amp;Ru</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.cpp" line="464"/>
        <source>Demo mode</source>
        <translation>Демо Режим</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.cpp" line="522"/>
        <location filename="ui/lesomainwindow.cpp" line="525"/>
        <source>Receive bytes: </source>
        <translation>Принятые байты: </translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.cpp" line="610"/>
        <source>LESOscope - Demo mode</source>
        <translation>LESOscope - Демо режим</translation>
    </message>
    <message>
        <location filename="ui/lesomainwindow.cpp" line="614"/>
        <source>LESOscope</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LESOTabWidget</name>
    <message>
        <location filename="ui/lesotabwidget.cpp" line="60"/>
        <source>Signal</source>
        <translation>Осциллограмма</translation>
    </message>
    <message>
        <location filename="ui/lesotabwidget.cpp" line="69"/>
        <source>Spectr</source>
        <translation>Спектр</translation>
    </message>
</context>
<context>
    <name>MeasureWidget</name>
    <message>
        <location filename="base/measurewidget.cpp" line="35"/>
        <location filename="base/measurewidget.cpp" line="322"/>
        <source>Channel</source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="35"/>
        <location filename="base/measurewidget.cpp" line="323"/>
        <source>Parameter</source>
        <translation>Параметр</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="36"/>
        <location filename="base/measurewidget.cpp" line="324"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="36"/>
        <location filename="base/measurewidget.cpp" line="325"/>
        <source>Capture num</source>
        <translation>Количество захватов</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="36"/>
        <location filename="base/measurewidget.cpp" line="326"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="49"/>
        <source>Measurements</source>
        <translation>Измерения</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="53"/>
        <location filename="base/measurewidget.cpp" line="318"/>
        <source>Add measurement</source>
        <translation>Добавить измерения</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="54"/>
        <location filename="base/measurewidget.cpp" line="319"/>
        <source>Edit measurement</source>
        <translation>Редактировать измерение</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="55"/>
        <location filename="base/measurewidget.cpp" line="320"/>
        <source>Remove measurement</source>
        <translation>Удалить измерение</translation>
    </message>
    <message>
        <location filename="base/measurewidget.cpp" line="146"/>
        <location filename="base/measurewidget.cpp" line="149"/>
        <location filename="base/measurewidget.cpp" line="152"/>
        <location filename="base/measurewidget.cpp" line="155"/>
        <location filename="base/measurewidget.cpp" line="159"/>
        <source> V</source>
        <translation>В</translation>
    </message>
</context>
<context>
    <name>PlayToolBar</name>
    <message>
        <location filename="ui/toolbar/playtoolbar.cpp" line="31"/>
        <location filename="ui/toolbar/playtoolbar.cpp" line="84"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="ui/toolbar/playtoolbar.cpp" line="32"/>
        <location filename="ui/toolbar/playtoolbar.cpp" line="85"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="base/common_func.cpp" line="60"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="62"/>
        <source>us</source>
        <translation>мкС</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="72"/>
        <source>Hz</source>
        <translation>Гц</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="76"/>
        <source>kHz</source>
        <translation>КГц</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="80"/>
        <source>MHz</source>
        <translation>МГц</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="92"/>
        <location filename="base/common_func.cpp" line="104"/>
        <source>Channel A</source>
        <translation>Канал А</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="95"/>
        <source>Channel B</source>
        <translation>Канал B</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="98"/>
        <source>Channel C</source>
        <translation>Канал C</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="101"/>
        <source>Channel D</source>
        <translation>Канал D</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="117"/>
        <location filename="base/common_func.cpp" line="136"/>
        <source>Amplitude</source>
        <translation>Амплитуда</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="120"/>
        <source>Minimum value</source>
        <translation>Минимальное значение</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="123"/>
        <source>Maximin value</source>
        <translation>Максимальное значение</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="126"/>
        <source>Middle value</source>
        <translation>Среднее значение</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="129"/>
        <source>Middle quad value</source>
        <translation>Средне-квадратическое значение</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="132"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="158"/>
        <source>V</source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="159"/>
        <source>mV</source>
        <translation>мВ</translation>
    </message>
    <message>
        <location filename="base/common_func.cpp" line="160"/>
        <source>uV</source>
        <translation>мкВ</translation>
    </message>
</context>
<context>
    <name>SettingsToolBar</name>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="45"/>
        <source>Device scan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="73"/>
        <source>1 ms/div</source>
        <translation>1 мс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="74"/>
        <source>0.5 ms/div</source>
        <translation>0,5 мс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="75"/>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="94"/>
        <source>0.2 ms/div</source>
        <translation>0,2 мс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="76"/>
        <source>0.1 ms/div</source>
        <translation>0,1 мс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="77"/>
        <source>500 us/div</source>
        <translation>500 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="78"/>
        <source>200 us/div</source>
        <translation>200 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="79"/>
        <source>100 us/div</source>
        <translation>100 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="80"/>
        <source>50 us/div</source>
        <translation>50 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="81"/>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="100"/>
        <source>2 us/div</source>
        <translation>2 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="82"/>
        <source>1 us/div</source>
        <translation>1 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="83"/>
        <source>0.5 us/div</source>
        <translation>0,5 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="85"/>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="104"/>
        <source>Time scan</source>
        <translation>Временная развертка</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="92"/>
        <source>0.8 ms/div</source>
        <translation>0,8 мс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="93"/>
        <source>0.4 ms/div</source>
        <translation>0,4 мс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="95"/>
        <source>80 us/div</source>
        <translation>80 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="96"/>
        <source>40 us/div</source>
        <translation>40 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="97"/>
        <source>20 us/div</source>
        <translation>20 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="98"/>
        <source>8 us/div</source>
        <translation>8 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="99"/>
        <source>4 us/div</source>
        <translation>4 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="101"/>
        <source>0.8 us/div</source>
        <translation>0,8 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="102"/>
        <source>0.4 us/div</source>
        <translation>0,4 мкс/Дел</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="110"/>
        <source>25 KHz</source>
        <translation>25 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="111"/>
        <source>50 KHz</source>
        <translation>50 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="112"/>
        <source>100 KHz</source>
        <translation>100 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="113"/>
        <source>250 KHz</source>
        <translation>250 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="114"/>
        <source>500 KHz</source>
        <translation>500 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="115"/>
        <source>1000 KHz</source>
        <translation>1000 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="116"/>
        <source>2500 KHz</source>
        <translation>2500 КГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="117"/>
        <source>5 MHz</source>
        <translation>5 МГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="118"/>
        <source>10 MHz</source>
        <translation>10 МГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="119"/>
        <source>25 MHz</source>
        <translation>25 МГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="120"/>
        <source>50 MHz</source>
        <translation>50 МГц</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="122"/>
        <source>Samples Freq</source>
        <translation>Частота дискретизации</translation>
    </message>
    <message>
        <location filename="ui/toolbar/settingstoolbar.cpp" line="209"/>
        <source>Control device</source>
        <translation>Управление устройством</translation>
    </message>
</context>
<context>
    <name>SignalPlotForm</name>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="44"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="798"/>
        <source>Time (us)</source>
        <translation>Время (мкс)</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="74"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="791"/>
        <source>Arrow mode</source>
        <translation>Обычный режим</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="77"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="792"/>
        <source>Move triggers lines</source>
        <translation>Перемещать триггер</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="80"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="793"/>
        <source>Show cursor</source>
        <translation>Курсоры</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="83"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="794"/>
        <source>Cursor source</source>
        <translation>Источник для курсора</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="86"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="795"/>
        <source>Reset cursors</source>
        <translation>Сброс курсоров</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="87"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="796"/>
        <source>Reset trigger</source>
        <translation>Сброс триггера</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="120"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="797"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="882"/>
        <source>Device not connected !</source>
        <translation>Устройство не подключено</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="177"/>
        <source>Time: </source>
        <translation>Время:</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="187"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="400"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="402"/>
        <location filename="plotting/signalplot/signalplotform.cpp" line="803"/>
        <source>Channel </source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="plotting/signalplot/signalplotform.cpp" line="189"/>
        <source> V</source>
        <translation>В</translation>
    </message>
</context>
<context>
    <name>SpectrPlotForm</name>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="60"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="914"/>
        <source>Frequency (kHz)</source>
        <translation>Частота (КГц)</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="61"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="915"/>
        <source>Amplitude</source>
        <translation>Амплитуда</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="76"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="917"/>
        <source>Window type</source>
        <translation>Окно сглаживания</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="77"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="918"/>
        <source>Rect</source>
        <translation>Прямоугольное окно</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="78"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="919"/>
        <source>Hamming</source>
        <translation>Окно Хэмминга</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="79"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="920"/>
        <source>Hann</source>
        <translation>Окно Ханна</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="80"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="921"/>
        <source>Nuatall</source>
        <translation>Окно Наталла</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="81"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="922"/>
        <source>Flat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="95"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="923"/>
        <source>Scale type</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="96"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="924"/>
        <source>Linear</source>
        <translation>Линейный</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="97"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="925"/>
        <source>dbm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="98"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="926"/>
        <source>dbi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="108"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="927"/>
        <source>Show cursor</source>
        <translation>Курсоры</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="111"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="928"/>
        <source>Cursor source</source>
        <translation>Источник для курсоров</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="117"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="929"/>
        <source>Reset cursors</source>
        <translation>Сброс курсоров</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="127"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="930"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="962"/>
        <source>Device not connected !</source>
        <translation>Устройство не подключено !</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="363"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="375"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="645"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="651"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="658"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="935"/>
        <source>Channel </source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="631"/>
        <source>Frequency: </source>
        <translation>Частота:</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="647"/>
        <source> V </source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="653"/>
        <source> dBu    </source>
        <translation></translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="654"/>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="661"/>
        <source> V</source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="plotting/spectrplot/spectrplotform.cpp" line="660"/>
        <source> dBm    </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TriggerToolBar</name>
    <message>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="32"/>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="200"/>
        <source>none</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="33"/>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="201"/>
        <source>auto</source>
        <translation>Программный триггер</translation>
    </message>
    <message>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="34"/>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="202"/>
        <source>Trigger mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="39"/>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="203"/>
        <source>Trigger source</source>
        <translation>Источник</translation>
    </message>
    <message>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="44"/>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="204"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="53"/>
        <location filename="ui/toolbar/triggertoolbar.cpp" line="205"/>
        <source>Trigger front</source>
        <translation>Фронт</translation>
    </message>
</context>
</TS>
