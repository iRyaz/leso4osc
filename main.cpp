
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "ui/lesomainwindow.h"
#include <QApplication>
#include <QStyle>
#include <QTranslator>
#include "include/osc_global.h"
#include "include/ui_config.h"

MEASURE_CHANNEL_t channelA;
MEASURE_CHANNEL_t channelB;
MEASURE_CHANNEL_t channelC;
MEASURE_CHANNEL_t channelD;

void channelsSetup()
{
    channelA.channelNum = CH_A;
    channelA.id = (char*)"A";
    channelA.color = CHANNEL_A_COLOR;
    channelA.ADCIconName = (char*)":/button_icons/channelAIcon.png";

    channelB.channelNum = CH_B;
    channelB.id = (char*)"B";
    channelB.color = CHANNEL_B_COLOR;
    channelB.ADCIconName = (char*)":/button_icons/channelBIcon.png";

    channelC.channelNum = CH_C;
    channelC.id = (char*)"C";
    channelC.color = CHANNEL_C_COLOR;
    channelC.ADCIconName = (char*)":/button_icons/channelCIcon.png";

    channelD.channelNum = CH_D;
    channelD.id = (char*)"D";
    channelD.color = CHANNEL_D_COLOR;
    channelD.ADCIconName = (char*)":/button_icons/channelDIcon.png";
}

int main(int argc, char *argv[])
{
    qDebug() << "=============================";
    qDebug() << "LESO4OSC virtual oscilloscope";
    qDebug() << "starting program ............";

    QDir dir;
    if(dir.mkdir(SETTINGS_DIR))
        qDebug() << "Create directory: " << SETTINGS_DIR; // Создание директории для сохранения настроек

    QApplication a(argc, argv);
    LESOMainWindow w;
    w.setStyleSheet("QStatusBar, QMainWindow{ background-color: #" +
                    QString::number(TOOL_BARS_COLOR, 16)  + ";}");
    channelsSetup();    // Задание параметров для каналов измерения
    w.registerChannel(&channelA);
    w.registerChannel(&channelB);
    w.registerChannel(&channelC);
    w.registerChannel(&channelD);
    w.show(true);
    return a.exec();
}
