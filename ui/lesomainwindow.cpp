
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "lesomainwindow.h"
#include "ui_lesomainwindow.h"

LESOMainWindow::LESOMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LESOMainWindow)
{   
    ui->setupUi(this);
    showStartupConnectDialog = true;
    setMinimumSize(QApplication::desktop()->screenGeometry().width()/2,
                   QApplication::desktop()->screenGeometry().height()/2);

    guiSettings = new QSettings(GUI_SETTINGS_FILE, QSettings::IniFormat);
    deviceSettings = new QSettings(DEVICE_SETTINGS_FILE, QSettings::IniFormat);
    settingsToolBarPtr = new SettingsToolBar(&signalSource);

    playToolBar = new PlayToolBar;

    mainTabWidget = new LESOTabWidget();
    mainTabWidget->signalPlot()->setTrigger(&deviceTrigger);

    addToolBarBreak();

    triggerToolBar = new TriggerToolBar(&deviceTrigger);

    measureTable = new MeasureWidget;

    rxBytesLabel = new QLabel;
    statusBar()->addWidget(playToolBar);
    statusBar()->addWidget(triggerToolBar);
    statusBar()->addWidget(settingsToolBarPtr);

#if SHOW_STATUS_LABEL_FLAG
    statusBar()->addWidget(rxBytesLabel);
    showRXBytesStatus(0);
#endif

    setCentralWidget(mainTabWidget);
    mainTabWidget->setMinimumHeight(10);

    connect(mainTabWidget, SIGNAL(tabChanged(int)), settingsToolBarPtr, SLOT(divTimeChanged(int)));
    connect(ui->actionOscilloscope, SIGNAL(toggled(bool)), this, SLOT(showSignalTab(bool)));
    connect(ui->actionSpectr, SIGNAL(toggled(bool)), this, SLOT(showSpectrTab(bool)));
    connect(&signalSource, SIGNAL(finished()), this, SLOT(readDataSlot()));
    connect(ui->actionReset_settings, SIGNAL(triggered()), settingsToolBarPtr, SLOT(reset()));
    connect(ui->actionReset_settings, SIGNAL(triggered()), triggerToolBar, SLOT(reset()));
    connect(settingsToolBarPtr, SIGNAL(changeDecimFactor(int)), mainTabWidget, SLOT(setAxisMaxTime(int)));
    connect(settingsToolBarPtr, SIGNAL(changeDecimFactor(int)), measureTable, SLOT(setDecimFactor(int)));
    connect(ui->actionConnectDevice, SIGNAL(triggered()), this, SLOT(connectDeviceSlot()));
    connect(mainTabWidget->signalPlot(), SIGNAL(changeAmpScan(MEASURE_CHANNEL_t*)), this, SLOT(setAmpScanChannel(MEASURE_CHANNEL_t*)));
    connect(settingsToolBarPtr, SIGNAL(changeScopeSize(int)), mainTabWidget, SLOT(setScopeSize(int)));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionSave_signal, SIGNAL(triggered()), mainTabWidget->signalPlot(), SLOT(saveFileSlot()));
    connect(ui->actionSave_spectr, SIGNAL(triggered()), mainTabWidget->spectrPlot(), SLOT(saveFileSlot()));
    connect(mainTabWidget->signalPlot(), SIGNAL(changeAmpScan(MEASURE_CHANNEL_t*)), this, SLOT(setAmpScanChannel(MEASURE_CHANNEL_t*)));
    connect(triggerToolBar, SIGNAL(levelTriggerSignal(double)), mainTabWidget->signalPlot(), SLOT(setLevelTrigger(double)));
    connect(mainTabWidget->signalPlot(), SIGNAL(changeTriggerLevel(double)),
            triggerToolBar, SLOT(updateTriggerLevelSpinBox(double)));

    connect(ui->actionAddMeasure, SIGNAL(triggered()), this, SLOT(addMeasureSlot()));
    connect(ui->actionSave_Device_settings, SIGNAL(triggered(bool)), this, SLOT(saveDeviceSettingsFlagToggle(bool)));
    connect(ui->actionDeleteMeasure, SIGNAL(triggered()), measureTable, SLOT(deleteSelectMeasure()));
    connect(ui->actionEditMeasure, SIGNAL(triggered()), this, SLOT(editMeasureSlot()));
    connect(measureTable->getAddMeasureAction(), SIGNAL(triggered()), this, SLOT(addMeasureSlot()));
    connect(measureTable->getEditMeasureAction(), SIGNAL(triggered()), this, SLOT(editMeasureSlot()));
    connect(ui->actionMeasureTable, SIGNAL(triggered(bool)), measureTable, SLOT(setVisible(bool)));
    connect(measureTable, SIGNAL(visibilityChanged(bool)), ui->actionMeasureTable, SLOT(setChecked(bool)));
    connect(mainTabWidget->spectrPlot(), SIGNAL(signalFrequencyMeasured(int,double)), measureTable, SLOT(setFrequencyMeasure(int,double)));

    connect(ui->samples8192, SIGNAL(triggered(bool)), this, SLOT(samples8192ChangeSlot(bool)));
    connect(ui->samples4096, SIGNAL(triggered(bool)), this, SLOT(samples4096ChangeSlot(bool)));
    connect(ui->samples2048, SIGNAL(triggered(bool)), this, SLOT(samples2048ChangeSlot(bool)));
    connect(ui->samples1024, SIGNAL(triggered(bool)), this, SLOT(samples1024ChangeSlot(bool)));

    aboutDialog = new LESOAboutDialog(this);
    connect(ui->actionAbout, SIGNAL(triggered()), aboutDialog, SLOT(show()));

    connect(ui->actionEn, SIGNAL(triggered(bool)), this, SLOT(changeEnLanguage(bool)));
    connect(ui->actionRu, SIGNAL(triggered(bool)), this, SLOT(changeRuLanguage(bool)));

    addDockWidget(Qt::BottomDockWidgetArea, measureTable);
    measureTable->setVisible(false);

    ui->actionOscilloscope->setChecked(true);
    ui->actionSpectr->setChecked(true);

    this->setFocus();
    saveDeviceSettingsFlag = false;
}

LESOMainWindow::~LESOMainWindow()
{
    signalSource.wait(5000);
    saveSettings();
    delete ui;
    delete guiSettings;

    qDebug() << "destory main windows";
}

void LESOMainWindow::show(bool settings_is_loaded = false)
{
    loadLocale();   // Загрузить локаль из файла настроек
    guiSettings->beginGroup("MAIN_WINDOW");
    // Показывать диалог выбора устройства при запуске программы
    showStartupConnectDialog = guiSettings->value("STARTUP_CONNECT_DIALOG", true).toBool();
    guiSettings->endGroup();
    // Если флаг установлен, то отобразить диалог выбора устройства
    // Если флаг сброшен, то просто запусть главное окно с неактивным интерфейсом
    if(showStartupConnectDialog)
        connectDeviceSlot(settings_is_loaded);
    else
        uiDisabled();
    QMainWindow::show();
}

void LESOMainWindow::samples8192ChangeSlot(bool toggled)
{
    if(toggled)
    {
        ui->samples4096->setChecked(false);
        ui->samples2048->setChecked(false);
        ui->samples1024->setChecked(false);
        signalSource.setPointNum(8192);
        qDebug() << "Set points - 8192";
    }
    else
    {
        ui->samples8192->setChecked(true);
        signalSource.setPointNum(8192);
    }
}

void LESOMainWindow::samples4096ChangeSlot(bool toggled)
{
    if(toggled)
    {
        ui->samples8192->setChecked(false);
        ui->samples2048->setChecked(false);
        ui->samples1024->setChecked(false);
        signalSource.setPointNum(4096);
        qDebug() << "Set points - 4096";
    }
    else
    {
        ui->samples8192->setChecked(true);
        signalSource.setPointNum(8192);
    }
}

void LESOMainWindow::samples2048ChangeSlot(bool toggled)
{
    if(toggled)
    {
        ui->samples4096->setChecked(false);
        ui->samples8192->setChecked(false);
        ui->samples1024->setChecked(false);
        signalSource.setPointNum(2048);
        qDebug() << "Set points - 2048";
    }
    else
    {
        ui->samples8192->setChecked(true);
        signalSource.setPointNum(8192);
    }
}

void LESOMainWindow::samples1024ChangeSlot(bool toggled)
{
    if(toggled)
    {
        ui->samples4096->setChecked(false);
        ui->samples2048->setChecked(false);
        ui->samples8192->setChecked(false);
        signalSource.setPointNum(1024);
        qDebug() << "Set points - 1024";
    }
    else
    {
        ui->samples8192->setChecked(true);
        signalSource.setPointNum(8192);
    }
}

void LESOMainWindow::loadSettings()
{
    measureTable->loadSettings();

    guiSettings->beginGroup("MAIN_WINDOW");
    int mainWindowWidth = guiSettings->value("WIDTH").toInt();
    int mainWindowHeight = guiSettings->value("HEIGHT").toInt();
    if(mainWindowWidth > minimumWidth() && mainWindowHeight > minimumHeight())
        resize(mainWindowWidth, mainWindowHeight);

    if(guiSettings->value("isMAXIMIZED").toBool())
        showMaximized();
    else
        move(qApp->desktop()->availableGeometry(this).center()-rect().center());

    if(guiSettings->value("DEVICE_SETTINGS_IS_SAVE").toBool())
    {
        ui->actionSave_Device_settings->setChecked(true);
        saveDeviceSettingsFlag = true;
    }
    else
    {
        ui->actionSave_Device_settings->setChecked(false);
        saveDeviceSettingsFlag = false;
    }

    guiSettings->endGroup();
    guiSettings->beginGroup("MEASURE_TABLE");
    measureTable->setVisible(guiSettings->value("MEASURES").toBool());
    int height = guiSettings->value("HEIGHT").toInt();
    measureTable->frameGeometry().setHeight(height);
    measureTable->updateGeometry();
    guiSettings->endGroup();

    deviceSettings->beginGroup("DEVICE_SETTINGS");
    settingsToolBarPtr->decimFactorComboBox->setCurrentIndex(deviceSettings->value("TIME_SCAN", 6).toInt());
    switch(deviceSettings->value("SAMPLES_NUM").toInt())
    {
    case 0: samples8192ChangeSlot(true); ui->samples8192->setChecked(true); break;
    case 1: samples4096ChangeSlot(true); ui->samples4096->setChecked(true); break;
    case 2: samples2048ChangeSlot(true); ui->samples2048->setChecked(true); break;
    case 3: samples1024ChangeSlot(true); ui->samples1024->setChecked(true); break;
    }
    deviceSettings->endGroup();

    for(int i(0); i < channelsUIList.size(); i++)
    {
        ChannelToolBar *channelToolBar = channelsUIList.at(i);
        deviceSettings->beginGroup(QString(channelToolBar->getChannel()->id));
        channelToolBar->inputModeComboBox->setCurrentIndex(deviceSettings->value("INPUT_MODE").toInt());
        channelToolBar->ampScanComboBox->setCurrentIndex(deviceSettings->value("AMP_SCAN").toInt());
        channelToolBar->getPopupMenu()->offsetSpinBox->setValue(deviceSettings->value("OFFSET").toDouble());
        channelToolBar->getPopupMenu()->ampSpinBox->setValue(deviceSettings->value("AMP", 1).toDouble());
        deviceSettings->endGroup();
    }

    deviceSettings->beginGroup("TRIGGER");
    triggerToolBar->triggerMode->setCurrentIndex(deviceSettings->value("MODE").toInt());
    triggerToolBar->levelSpinBox->setValue(deviceSettings->value("LEVEL").toDouble());
    triggerToolBar->triggerEdge->setCurrentIndex(deviceSettings->value("TRIGGER_FRONT").toInt());
    triggerToolBar->triggerSource->setCurrentIndex(deviceSettings->value("SOURCE_CH").toInt());
    deviceSettings->endGroup();

    mainTabWidget->spectrPlot()->loadSettings(guiSettings);
    mainTabWidget->signalPlot()->loadSettings(guiSettings);
}

void LESOMainWindow::loadLocale()
{
    guiSettings->beginGroup("MAIN_WINDOW");

    qDebug() << "Default system locale: " << QLocale::system().name();
    int defaultLocaleIndex = 0;
    if(QLocale::system().name() == "ru_RU")
        defaultLocaleIndex = 1;

    programLang = (UI_LANG)guiSettings->value("UI_LANG", defaultLocaleIndex).toInt();
    switch(programLang)
    {
    case 0: changeEnLanguage(true); ui->actionEn->setChecked(true); break;
    case 1: changeRuLanguage(true); ui->actionRu->setChecked(true); break;
    }

    guiSettings->endGroup();
}

void LESOMainWindow::saveSettings()
{
    guiSettings->beginGroup("MAIN_WINDOW");
    guiSettings->setValue("WIDTH", width());
    guiSettings->setValue("HEIGHT", height());
    guiSettings->setValue("isMAXIMIZED", this->isMaximized());
    guiSettings->setValue("DEVICE_SETTINGS_IS_SAVE", saveDeviceSettingsFlag);
    guiSettings->setValue("UI_LANG", programLang);
    guiSettings->setValue("STARTUP_CONNECT_DIALOG", showStartupConnectDialog);
    guiSettings->endGroup();
    guiSettings->beginGroup("MEASURE_TABLE");
    guiSettings->setValue("MEASURES", !measureTable->isHidden());
    guiSettings->setValue("HEIGHT", measureTable->size().height());
    guiSettings->endGroup();
    mainTabWidget->spectrPlot()->saveSettings(guiSettings);
    mainTabWidget->signalPlot()->saveSettings(guiSettings);
    guiSettings->sync();

    if(saveDeviceSettingsFlag)
    {
        int samples_num = signalSource.getSamplesNum();
        int samples_num_id = 0;
        switch(samples_num)
        {
        case 8192:
            samples_num_id = 0;
            break;
        case 4096:
            samples_num_id = 1;
            break;
        case 2048:
            samples_num_id = 2;
            break;
        case 1024:
            samples_num_id = 3;
            break;
        }

        deviceSettings->beginGroup("DEVICE_SETTINGS");
        deviceSettings->setValue("CHANNELS_NUM", signalSource.getChannelsNum());
        deviceSettings->setValue("TIME_SCAN", settingsToolBarPtr->getTimeScanIndex());
        deviceSettings->setValue("SAMPLES_NUM", samples_num_id);
        deviceSettings->endGroup();

        for(int i(0); i < channelsUIList.size(); i++)
        {
            ChannelToolBar *channelToolBar = channelsUIList.at(i);
            deviceSettings->beginGroup(QString(channelToolBar->getChannel()->id));
            deviceSettings->setValue("INPUT_MODE", channelToolBar->getInputMode());
            deviceSettings->setValue("AMP_SCAN", channelToolBar->getAmpScanIndex());
            deviceSettings->setValue("OFFSET", channelToolBar->getPopupMenu()->offsetSpinBox->value());
            deviceSettings->setValue("AMP", channelToolBar->getPopupMenu()->ampSpinBox->value());
            deviceSettings->endGroup();
        }

        deviceSettings->beginGroup("TRIGGER");
        deviceSettings->setValue("MODE", triggerToolBar->triggerMode->currentIndex());
        deviceSettings->setValue("LEVEL", deviceTrigger.level);
        deviceSettings->setValue("TRIGGER_FRONT", triggerToolBar->triggerEdge->currentIndex());
        deviceSettings->setValue("SOURCE_CH", triggerToolBar->triggerSource->currentIndex());
        deviceSettings->endGroup();

        deviceSettings->sync();
    }
}

void LESOMainWindow::registerChannel(MEASURE_CHANNEL_t *ch)
{
    ChannelToolBar *channelToolBar = new ChannelToolBar(&signalSource, ch);
    addToolBar(Qt::TopToolBarArea, channelToolBar);
    channelsUIList.append(channelToolBar);

    connect(channelToolBar, SIGNAL(changeAmpComboBox(MEASURE_CHANNEL_t*)), mainTabWidget, SLOT(setAxisMaxAmp(MEASURE_CHANNEL_t*)));
    connect(channelToolBar, SIGNAL(changeAmpComboBox(MEASURE_CHANNEL_t*)), triggerToolBar, SLOT(setTriggerLevelStep(MEASURE_CHANNEL_t*)));
    connect(ui->actionReset_settings, SIGNAL(triggered()), channelToolBar, SLOT(reset()));
    connect(channelToolBar, SIGNAL(channelEnableSignal()), triggerToolBar, SLOT(updateChannelSourceList()));

    mainTabWidget->addChannel(ch);
    measureTable->addChannel(ch);
    triggerToolBar->addChannel(ch);
}

void LESOMainWindow::showSignalTab(bool toggled)
{
    if(toggled)
        mainTabWidget->addDiagramSignal();
    else
        mainTabWidget->closeDiagramSignal();
}

void LESOMainWindow::showSpectrTab(bool toggled)
{
    if(toggled)
        mainTabWidget->addDiagramSpectr();
    else
        mainTabWidget->closeDiagramSpectr();
}

void LESOMainWindow::readDataSlot()
{
    if(signalSource.isInit())
    {
        uiEnabled();
#if SHOW_STATUS_LABEL_FLAG
        showRXBytesStatus(signalSource.getRXBytes());
#endif
        for(int i(0); i < channelsUIList.size(); i++)
        {
            channelsUIList.at(i)->getChannel()->sampleBufferSize = CH_SIZE;
            channelsUIList.at(i)->getChannel()->samplePtr = signalSource.getDataPtr((CHANNEL_t)i);
        }

        if(playToolBar->isStart())
            mainTabWidget->update();
        signalSource.start();
        measureTable->update();
    }
    else
    {
#if SHOW_STATUS_LABEL_FLAG
        showRXBytesStatus(0);
#endif
        uiDisabled();
    }
}

void LESOMainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Space)
    {
        if(playToolBar->isStart())
        {
            playToolBar->start(false);
        }
        else
        {
            playToolBar->start(true);
        }
    }

    if(event->key() == Qt::Key_K)
    {
        const char* diagramTypeName = typeid(*(mainTabWidget->currentWidget())).name();
        if(QString(diagramTypeName) == "14SignalPlotForm")
        {
            mainTabWidget->signalPlot()->visibleCursors(!(mainTabWidget->signalPlot()->isCursorVisible()));
        }

        if(QString(diagramTypeName) == "14SpectrPlotForm")
        {
            mainTabWidget->spectrPlot()->visibleCursors(!(mainTabWidget->spectrPlot()->isCursorVisible()));
        }

    }
}

void LESOMainWindow::connectDeviceSlot(bool settings_is_loaded)
{
    playToolBar->start(false);
    connectDialog = new ConnectDeviceDialog(&signalSource);
    connectDialog->setStartupFlag(showStartupConnectDialog);
    connectDialog->exec();

    if(settings_is_loaded)
    {
        loadSettings();
    }
    else
    {
        settingsToolBarPtr->resetButtonTriggered();
        signalSource.samplesNum = MAX_CHANNEL_BUFFER_SIZE;
        signalSource.samplesNumDevice = MAX_CHANNEL_BUFFER_SIZE;
        settingsToolBarPtr->setComboBoxFromDecimFactor(20);
        for(int i(0); i < channelsUIList.size(); i++)
        {
            channelsUIList.at(i)->enableChannel(false);
            channelsUIList.at(i)->getChannel()->enabled = false;
            channelsUIList.at(i)->setAmpScan(AMP_SCAN_1_500);
        }
    }

    if(signalSource.isDemoMode())
    {
        setWindowTitle("LESOscope " + QString(PROGRAM_VERSION) + " " + tr("Demo mode"));
        qDebug() << "Demo Mode";
    }
    else
    {
        this->setWindowTitle("LESOscope " + QString(PROGRAM_VERSION));
        qDebug() << "Open LESO4 device";
    }

    showStartupConnectDialog = connectDialog->isStartup();

    delete connectDialog;

    playToolBar->start(true);
    if(!signalSource.isRunning())
        signalSource.start();
}

// Сделать интерфейс программы активным
void LESOMainWindow::uiEnabled()
{
    playToolBar->setEnabled(true);

    for(int i(0); i < channelsUIList.size(); i++)
    {
        channelsUIList.at(i)->setEnabled(true);
    }

    settingsToolBarPtr->setEnabled(true);
    mainTabWidget->signalPlot()->setEnabled(true);
    mainTabWidget->spectrPlot()->setEnabled(true);
}

// Сделать интерфейс программы неактивным
void LESOMainWindow::uiDisabled()
{
    playToolBar->setEnabled(false);

    for(int i(0); i < channelsUIList.size(); i++)
    {
        channelsUIList.at(i)->setEnabled(false);
        channelsUIList.at(i)->enableChannel(false);
    }

    settingsToolBarPtr->setEnabled(false);
    mainTabWidget->signalPlot()->setEnabled(false);
    mainTabWidget->spectrPlot()->setEnabled(false);
}

void LESOMainWindow::setAmpScanChannel(MEASURE_CHANNEL_t *ch)
{
    signalSource.setAmplitudeScan(ch->channelNum, ch->ampScan);
}

void LESOMainWindow::showRXBytesStatus(int rx)
{
    if(rx == 0)
        rxBytesLabel->setText("<font size = 4 color=\"#FF0000\">"
            + tr("Receive bytes: ") + QString::number(rx) + "</font>");
    else
        rxBytesLabel->setText("<font size = 4 color=\"#CCA100\">"
            + tr("Receive bytes: ") + QString::number(rx) + "</font>");
}

void LESOMainWindow::savePlot()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList filters;
    filters << JPG_FORMAT_FILTER
        << BMP_FORMAT_FILTER;
    dialog.setNameFilters(filters);
    QStringList files;
    if(dialog.exec())
    {
        files = dialog.selectedFiles();
        qDebug() << "Save to file: " << files.at(0);
        if(dialog.selectedNameFilter().compare(QString(JPG_FORMAT_FILTER)))
                mainTabWidget->signalPlot()->saveFile(files.at(0), JPG_FORMAT);
        else
        {
            if(dialog.selectedNameFilter().compare(QString(BMP_FORMAT_FILTER)))
                    mainTabWidget->signalPlot()->saveFile(files.at(0), BMP_FORMAT);
        }
    }
}

void LESOMainWindow::addMeasureSlot()
{
    measureTable->setVisible(true);
    measureDialog = new EditMeasureDialog(measureTable, this);
    measureDialog->showDialog(EditMeasureDialog::NEW_MEASURE);
    delete measureDialog;
}

void LESOMainWindow::editMeasureSlot()
{
    measureTable->setVisible(true);
    measureDialog = new EditMeasureDialog(measureTable, this);
    measureDialog->showDialog(EditMeasureDialog::EDIT_MEASURE);
    delete measureDialog;
}

void LESOMainWindow::changeEnLanguage(bool changed)
{
    if(changed)
    {
        ui->actionRu->setChecked(false);
        translator.load(":/translations/LESO4_en.qm");
        QCoreApplication::installTranslator(&translator);
        ui->retranslateUi(this);
        programLang = LANG_EN;
        aboutDialog->setLang(LESOAboutDialog::LANG_EN);
    }
    else
    {
        ui->actionEn->setChecked(true);
    }
}

void LESOMainWindow::changeRuLanguage(bool changed)
{
    if(changed)
    {
        ui->actionEn->setChecked(false);
        translator.load(":/translations/LESO4_ru.qm");
        QCoreApplication::installTranslator(&translator);
        triggerToolBar->setTranslator(&translator);
        ui->retranslateUi(this);
        programLang = LANG_RU;
        aboutDialog->setLang(LESOAboutDialog::LANG_RU);
    }
    else
    {
        ui->actionEn->setChecked(true);
        changeEnLanguage(true);
    }
}

void LESOMainWindow::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        if(signalSource.isDemoMode())
        {
            setWindowTitle(tr("LESOscope - Demo mode"));
        }
        else
        {
            this->setWindowTitle(tr("LESOscope"));
        }
    }
    else
        QWidget::changeEvent(event);
}

QMenu *LESOMainWindow::createPopupMenu()
{
    return NULL;
}
