/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef CHANNELPOPUPWIDGET_H
#define CHANNELPOPUPWIDGET_H

#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QGroupBox>
#include <QPushButton>
#include <QCheckBox>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>

#include "base/common_func.h"
#include "include/osc_global.h"
#include "include/ui_config.h"

/*
 * Меню дополнительных настроек для канала
 * Пока содержит два параметра:
 *  - Усиление;
 *  - Смещение (добавление постоянной составляющей);
*/
class ChannelPopupWidget : public QMenu
{
    Q_OBJECT

    QHBoxLayout HLayout;
    QVBoxLayout *VLayout;
    QVBoxLayout LabelLayout;
    QVBoxLayout ControlLayout;
    QVBoxLayout groupBoxLayout;

    QGroupBox *cursorsGroupBox;

    QLabel *offsetLabel;
    QDoubleSpinBox *offsetSpinBox;

    QLabel *ampLabel;
    QDoubleSpinBox *ampSpinBox;

    MEASURE_CHANNEL_t *channel;

    double getSignalLevelStep(AMP_SCAN_t ampDivIndex);

protected:
    void changeEvent(QEvent *event);
    bool eventFilter(QObject *target, QEvent *event);

public:
    explicit ChannelPopupWidget(MEASURE_CHANNEL_t *chPtr, QWidget *parent = 0);
    ~ChannelPopupWidget();

public slots:
    void setSignalOffset(double);
    void setSignalAmp(double);
    void setOffsetStep(MEASURE_CHANNEL_t *channel);
    void reset();

    friend class LESOMainWindow;
};

#endif // CHANNELPOPUPWIDGET_H
