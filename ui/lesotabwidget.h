
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef LESOTABWIDGET_H
#define LESOTABWIDGET_H

#include <QTabWidget>
#include <QDebug>
#include <QVector>

#include "include/osc_global.h"

#include "plotting/signalplot/signalplotform.h"
#include "plotting/spectrplot/spectrplotform.h"

/*
 * Центральный виджет главного окна программы,
 * пока на нем только две вкладки, спектр и осциллограмма
 * При переключении вкладок посылается сигнал void tabChanged(int tabType);
 * В котором tabType - тип диаграммы расположенной на вкладке
 *  - 0 - Осциллограмма;
 *  - 1 - Спектрограмма;
*/
class LESOTabWidget : public QTabWidget
{
    Q_OBJECT

    bool signaTabFlag;
    bool spectrTabFlag;
    int signalTabIndex;
    int spectrTabIndex;
    int currentDecimFactor;

    SignalPlotForm *signalTab;
    SpectrPlotForm *spectrTab;

    double current_samples_frequency;
    int scopeSize;

public:
    LESOTabWidget();
    ~LESOTabWidget();

    void addDiagramSpectr();
    void addDiagramSignal();
    void closeDiagramSignal();
    void closeDiagramSpectr();

    SignalPlotForm *signalPlot() { return signalTab; }
    SpectrPlotForm *spectrPlot() { return spectrTab; }

    void addChannel(MEASURE_CHANNEL_t *ch);

public slots:
    void update();
    void setAxisMaxTime(int decim_factor);
    void setAxisMaxAmp(MEASURE_CHANNEL_t *channel);
    void setScopeSize(int);

    void tabChangedInternalSlot(int index);

signals:
    /*
     * Сигнал, отправляемый во время смены вкладки
     * Содержит в себе тип виджета, который находится во вкладке
     * 0 - Осциллограмма
     * 1 - Спектр
     * Используется, чтобы поменять элементы управления в главном окне
    */
    void tabChanged(int tabType);

};

#endif // LESOTABWIDGET_H
