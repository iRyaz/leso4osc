/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "channelpopupwidget.h"

ChannelPopupWidget::ChannelPopupWidget(MEASURE_CHANNEL_t *chPtr, QWidget *parent) :
    QMenu(parent)
{
    channel = chPtr;
    setWindowFlags(Qt::Popup);
    int channel_color = ((channel->color)&0xFFFFFF00)>>8;

    setStyleSheet("QLabel, QDoubleSpinBox, QComboBox, QCheckBox { color:#" + QString::number(channel_color, 16)  + "}" +
                  "QMenu { background: #" + QString::number(TOOL_BARS_COLOR, 16) + "}");

    offsetLabel = new QLabel(tr("Signal Offset"));
    offsetSpinBox = new QDoubleSpinBox;
    offsetSpinBox->setMaximum(20);
    offsetSpinBox->setMinimum(-20);
    offsetSpinBox->setSingleStep(0.1f);
    offsetSpinBox->setValue(0);
    offsetSpinBox->installEventFilter(this);
    LabelLayout.addWidget(offsetLabel);
    ControlLayout.addWidget(offsetSpinBox);
    connect(offsetSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setSignalOffset(double)));
    setSignalOffset(0);

    ampLabel = new QLabel(tr("Amplifier channel"));
    ampSpinBox = new QDoubleSpinBox;
    ampSpinBox->setMinimum(0.01f);
    ampSpinBox->setSingleStep(0.1f);
    ampSpinBox->setValue(1.0f);
    ampSpinBox->installEventFilter(this);
    LabelLayout.addWidget(ampLabel);
    ControlLayout.addWidget(ampSpinBox);
    connect(ampSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setSignalAmp(double)));
    setSignalAmp(1);

    HLayout.addLayout(&LabelLayout);
    HLayout.addLayout(&ControlLayout);

    VLayout = new QVBoxLayout;
    VLayout->addLayout(&HLayout);
    setLayout(VLayout);
}

ChannelPopupWidget::~ChannelPopupWidget()
{
}

void ChannelPopupWidget::setSignalOffset(double val)
{
    channel->offset = val;
}

void ChannelPopupWidget::setSignalAmp(double val)
{
    channel->amp = val;
}

double ChannelPopupWidget::getSignalLevelStep(AMP_SCAN_t ampDivIndex)
{
    double ret = 1.0f;

    switch(ampDivIndex)
    {
    case AMP_SCAN_1_500: ret = 1.0f; break;
    case AMP_SCAN_1_200: ret = 0.5f; break;
    case AMP_SCAN_1_100: ret = 0.25f; break;
    case AMP_SCAN_1_50: ret = 0.1; break;
    case AMP_SCAN_1_20: ret = 0.05f; break;
    case AMP_SCAN_1_10: ret = 0.025f; break;
    case AMP_SCAN_1_5: ret = 0.01f; break;
    case AMP_SCAN_1_2: ret = 0.005; break;
    case AMP_SCAN_1_1: ret = 0.0025; break;
    }

    return ret;
}

void ChannelPopupWidget::setOffsetStep(MEASURE_CHANNEL_t *channel)
{
    offsetSpinBox->setSingleStep(getSignalLevelStep(channel->ampScan));
}

void ChannelPopupWidget::reset()
{
    ampSpinBox->setValue(1);
    offsetSpinBox->setValue(0);
}

void ChannelPopupWidget::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ampLabel->setText(tr("Amplifier channel"));
        offsetLabel->setText(tr("Signal Offset"));
    }
    else
        QWidget::changeEvent(event);
}

bool ChannelPopupWidget::eventFilter(QObject *target, QEvent *event)
{
    return eventKeyStub(target, event);
}
