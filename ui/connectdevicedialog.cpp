
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "connectdevicedialog.h"
#include "ui_connectdevicedialog.h"

ConnectDeviceDialog::ConnectDeviceDialog(SignalSource *s, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectDeviceDialog)
{
    showStartup = true;
    signalSource = s;
    ui->setupUi(this);
    setStyleSheet("QDialog { background: #" + QString::number(TOOL_BARS_COLOR, 16) + "}");
    setFixedSize(QSize(485, 268));
    setModal(true);

    deviceNum = 1;
    deviceName = DEMO_MODE_DESCRIPTOR;

    ui->deviceList->setIconSize(QSize(DEFAULT_LIST_ITEM_ICON, DEFAULT_LIST_ITEM_ICON));
    updateDeviceList();

    connect(ui->okButton, SIGNAL(clicked()), this, SLOT(selectButtonSlot()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(cancelButtonSlot()));
    connect(ui->updateButton, SIGNAL(clicked()), this, SLOT(updateButtonSlot()));
    connect(ui->show_dialog, SIGNAL(clicked(bool)), this, SLOT(showChecked(bool)));
    connect(ui->deviceList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this,
            SLOT(itemDoubleClicked(QListWidgetItem*)));
}

ConnectDeviceDialog::~ConnectDeviceDialog()
{
    delete ui;
}

void ConnectDeviceDialog::showChecked(bool checked)
{
    showStartup = checked;
}

void ConnectDeviceDialog::setStartupFlag(bool startup)
{
    showStartup = startup;
    ui->show_dialog->setChecked(startup);
}

void ConnectDeviceDialog::selectButtonSlot()
{
    if(ui->deviceList->currentRow() == 0)
    {
        signalSource->init(QString(DEMO_MODE_DESCRIPTOR));
    }
    else
    {
        signalSource->init(QString((char*)ui->deviceList->selectedItems().at(0)->text().toStdString().data()));
    }
    close();
}

void ConnectDeviceDialog::cancelButtonSlot()
{
    close();
}

int ConnectDeviceDialog::getDeviceNum()
{
    return deviceNum;
}

void ConnectDeviceDialog::itemDoubleClicked(QListWidgetItem *item)
{
    Q_UNUSED(item)
    if(ui->deviceList->currentRow() == 0)
    {
        signalSource->init(QString(DEMO_MODE_DESCRIPTOR));
    }
    else
    {
        signalSource->init(QString((char*)ui->deviceList->selectedItems().at(0)->text().toStdString().data()));
    }
    close();
}

void ConnectDeviceDialog::updateButtonSlot()
{
    updateDeviceList();
}

void ConnectDeviceDialog::updateDeviceList()
{
    ui->deviceList->clear();
    ui->deviceList->addItem(new QListWidgetItem(QIcon(":/main_icons/leso4.2.png"), tr("Demo Mode")));

    if(signalSource->init(QString("LESO4.1_ER")) == SignalSource::SS_OK)
    {
        ui->deviceList->addItem(new QListWidgetItem(QIcon(":/main_icons/leso4.2_Dev.png"), "LESO4.1_ER"));
        deviceNum++;
    }
    if(signalSource->init(QString("LESO4.2")) == SignalSource::SS_OK)
    {
        ui->deviceList->addItem(new QListWidgetItem(QIcon(":/main_icons/leso4.2_Dev.png"), "LESO4.2"));
        deviceNum++;
    }

    signalSource->closeDevice();
}

void ConnectDeviceDialog::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
        QWidget::changeEvent(event);
}
