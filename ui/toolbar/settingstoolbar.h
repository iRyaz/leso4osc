/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SETTINGSTOOLBAR_H
#define SETTINGSTOOLBAR_H

#include <QToolBar>
#include <QSpinBox>
#include <QComboBox>
#include <QToolButton>
#include <QString>
#include <QEvent>
#include <QKeyEvent>
#include <QLayout>

#include "lib/leso4.h"
#include "include/osc_global.h"
#include "include/ui_config.h"
#include "base/signalsource.h"

#define DEFAULT_POINT_NUM 8192

#define POINT_NUM_TEXT "Point number"
#define DECIM_FACTOR_TEXT "Decim factor"

/*
 * Панель настроек прибора
 * Пока на ней находится выпадающий список, где выбирается временная развертка
 * Список развертки может, показывать время на деление или список частот дискретизации
 * в зависимости от того какая диаграмма просматривается, либо осциллограмма, либо спектр,
 * этот режим устанавливается слотом void divTimeChanged(int diagramType);
 *  - если аргумент diagramType = 0, то в выпадаюцем списке Время/Дел;
 *  - если аргумент diagramType = 1, то в выпадаюцем списке частоты дискретизации;
*/
class SettingsToolBar : public QToolBar
{
    Q_OBJECT


    QComboBox *decimFactorComboBox;
    QToolBar *settingsToolBarPtr;

    void initComboBox();

    SignalSource *signalSource;
    int timeScanIndex;

    enum DIAGRAM_TYPE
    {
        SIGNAL_DIAGRAM = 0,
        SPECTR_DIAGRAM = 1
    };

    DIAGRAM_TYPE currentDiagram;    // Тип графика к которому привязана эта панель

public:
    SettingsToolBar(SignalSource *_signalSource, QToolBar *parent = 0);
    ~SettingsToolBar();
    int getTimeScanIndex() { return timeScanIndex; }

public slots:
    void pointNumChanged(int comboBoxIndex);
    void decimFactorChange(int);
    void scopeSizeChange(int);
    void setComboBoxFromDecimFactor(int decim_factor);
    void showSpectrMenu();
    void resetButtonTriggered();
    void reset();

    /*
     * Связать эту панель инструментов к
     * 0 - осциллограмме
     * 1 - спектру
    */
    void divTimeChanged(int diagramType);

signals:
    void changeDecimFactor(int _decim_factor);
    void changeScopeSize(int _scopeSize);

protected:
    bool eventFilter(QObject *target, QEvent *event);
    void changeEvent(QEvent *event);

    friend class LESOMainWindow;
};

#endif // SETTINGSTOOLBAR_H
