/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "triggertoolbar.h"

TriggerToolBar::TriggerToolBar(TRIGGER_t *_trigger)
{
    setMovable(false);
    trigger = _trigger;
    triggerMode = new QComboBox;
    triggerMode->addItem(tr("none"));
    triggerMode->addItem(tr("auto"));
    triggerMode->setToolTip(tr("Trigger mode"));
    triggerMode->installEventFilter(this);
    addWidget(triggerMode);

    triggerSource = new ChannelSourceComboBox;
    triggerSource->setToolTip(tr("Trigger source"));
    triggerSource->installEventFilter(this);
    addWidget(triggerSource);

    levelSpinBox = new QDoubleSpinBox;
    levelSpinBox->setToolTip(tr("Level"));
    levelSpinBox->setDecimals(3);
    levelSpinBox->setRange(-20, 20);
    levelSpinBox->installEventFilter(this);
    addWidget(levelSpinBox);

    triggerEdge = new QComboBox;
    triggerEdge->addItem(QIcon(":/button_icons/trigger_rising_edge.png"), "");
    triggerEdge->addItem(QIcon(":/button_icons/trigger_falling_edge.png"), "");
    triggerEdge->setToolTip(tr("Trigger front"));
    triggerEdge->installEventFilter(this);
    addWidget(triggerEdge);

    connect(levelSpinBox, SIGNAL(valueChanged(double)), this, SLOT(changeLevelTrigger(double)));
    connect(triggerEdge, SIGNAL(currentIndexChanged(int)), this, SLOT(changeTriggerEdge(int)));
    connect(triggerSource, SIGNAL(currentIndexChanged(int)), this, SLOT(changeTriggerSource(int)));
    connect(triggerMode, SIGNAL(currentIndexChanged(int)), this, SLOT(changeTriggerMode(int)));

    triggerMode->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    triggerSource->setSizeAdjustPolicy(QComboBox::AdjustToContents);

    changeTriggerMode(0);

    int base_time_color = (BASE_TIME_COLOR&0xFFFFFF00)>>8;
    triggerMode->setStyleSheet("QComboBox { color: #" + QString::number(base_time_color, 16) + "}");
    triggerSource->setStyleSheet("QComboBox { color: #" + QString::number(base_time_color, 16) + "}");
    levelSpinBox->setStyleSheet("QDoubleSpinBox { color: #" + QString::number(base_time_color, 16) + "}");
    triggerEdge->setStyleSheet("QComboBox { color: #" + QString::number(base_time_color, 16) + "}");

    this->layout()->setSpacing(TOOL_BAR_SPACING);

}

TriggerToolBar::~TriggerToolBar()
{

}

void TriggerToolBar::addChannel(MEASURE_CHANNEL_t *ch)
{
    triggerSource->addChannel(ch);
}

void TriggerToolBar::updateChannelSourceList()
{
    triggerSource->update();
}

void TriggerToolBar::changeTriggerMode(int comboBoxIndex)
{
    switch(comboBoxIndex)
    {
    case 0:
        trigger->en = false;
        levelSpinBox->setEnabled(false);
        triggerSource->setEnabled(false);
        triggerEdge->setEnabled(false);
        break;
    case 1:
        trigger->en = true;
        levelSpinBox->setEnabled(true);
        triggerSource->setEnabled(true);
        triggerEdge->setEnabled(true);
        emit levelTriggerSignal(levelSpinBox->value());
        break;
    }
}

void TriggerToolBar::changeTriggerSource(int comboBoxIndex)
{
    trigger->ch = (CHANNEL_t)triggerSource->getCurrentChannel(comboBoxIndex);

}

void TriggerToolBar::changeTriggerEdge(int comboBoxIndex)
{
    switch(comboBoxIndex)
    {
    case 0:
        trigger->edge = RISING_EDGE;
        break;
    case 1:
        trigger->edge = FALLING_EDGE;
        break;
    }
}

double TriggerToolBar::getLevelStep(AMP_SCAN_t ampDivIndex)
{
    double ret = 1.0f;

    switch(ampDivIndex)
    {
    case AMP_SCAN_1_500: ret = 1.0f; break;
    case AMP_SCAN_1_200: ret = 0.5f; break;
    case AMP_SCAN_1_100: ret = 0.25f; break;
    case AMP_SCAN_1_50: ret = 0.1; break;
    case AMP_SCAN_1_20: ret = 0.05f; break;
    case AMP_SCAN_1_10: ret = 0.025f; break;
    case AMP_SCAN_1_5: ret = 0.01f; break;
    case AMP_SCAN_1_2: ret = 0.005; break;
    case AMP_SCAN_1_1: ret = 0.0025; break;
    }

    return ret;
}

void TriggerToolBar::changeLevelTrigger(double level)
{
    emit levelTriggerSignal(level);
    trigger->level = level;
}

void TriggerToolBar::setTriggerLevelStep(MEASURE_CHANNEL_t *ch)
{
    levelSpinBox->setSingleStep(getLevelStep(ch->ampScan));
}

bool TriggerToolBar::eventFilter(QObject *target, QEvent *event)
{
   return eventKeyStub(target, event);
}

void TriggerToolBar::updateTriggerLevelSpinBox(double level)
{
    levelSpinBox->setValue(level);
}

void TriggerToolBar::reset()
{
    levelSpinBox->setValue(0);
    triggerEdge->setCurrentIndex(0);
    triggerMode->setCurrentIndex(0);
    triggerSource->setCurrentIndex(0);
}

void TriggerToolBar::setTranslator(QTranslator *t)
{
    QCoreApplication::installTranslator(t);
}

void TriggerToolBar::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        triggerMode->setItemText(0, tr("none"));
        triggerMode->setItemText(1, tr("auto"));
        triggerMode->setToolTip(tr("Trigger mode"));
        triggerSource->setToolTip(tr("Trigger source"));
        levelSpinBox->setToolTip(tr("Level"));
        triggerEdge->setToolTip(tr("Trigger front"));
    }
    else
        QWidget::changeEvent(event);
}
