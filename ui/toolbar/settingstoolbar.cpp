/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "settingstoolbar.h"

SettingsToolBar::SettingsToolBar(SignalSource *_signalSource, QToolBar *parent) : QToolBar(parent)
{
    signalSource = _signalSource;

    setMovable(false);
    decimFactorComboBox = new QComboBox;
    addWidget(decimFactorComboBox);

    connect(decimFactorComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(decimFactorChange(int)));

    initComboBox();

    decimFactorComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    decimFactorComboBox->installEventFilter(this);

    int base_time_color = (BASE_TIME_COLOR&0xFFFFFF00)>>8;
    decimFactorComboBox->setStyleSheet("QComboBox{ color: #" + QString::number(base_time_color, 16) + "}");
    this->layout()->setSpacing(TOOL_BAR_SPACING);
    setWindowTitle(tr("Device scan"));

    timeScanIndex = 0;

    currentDiagram = SIGNAL_DIAGRAM;
}

SettingsToolBar::~SettingsToolBar()
{
}

void SettingsToolBar::showSpectrMenu()
{

}

void SettingsToolBar::resetButtonTriggered()
{
    decimFactorComboBox->setCurrentIndex(6);
}

void SettingsToolBar::reset()
{
    decimFactorComboBox->setCurrentIndex(6);
}

void SettingsToolBar::initComboBox()
{
    decimFactorComboBox->addItem(tr("1 ms/div"));
    decimFactorComboBox->addItem(tr("0.5 ms/div"));
    decimFactorComboBox->addItem(tr("0.2 ms/div"));
    decimFactorComboBox->addItem(tr("0.1 ms/div"));
    decimFactorComboBox->addItem(tr("500 us/div"));
    decimFactorComboBox->addItem(tr("200 us/div"));
    decimFactorComboBox->addItem(tr("100 us/div"));
    decimFactorComboBox->addItem(tr("50 us/div"));
    decimFactorComboBox->addItem(tr("2 us/div"));
    decimFactorComboBox->addItem(tr("1 us/div"));
    decimFactorComboBox->addItem(tr("0.5 us/div"));
    decimFactorComboBox->installEventFilter(this);
    decimFactorComboBox->setToolTip(tr("Time scan"));
}

void SettingsToolBar::divTimeChanged(int diagramType)
{
    if(diagramType == 0)
    {
        decimFactorComboBox->setItemText(0, tr("0.8 ms/div"));
        decimFactorComboBox->setItemText(1, tr("0.4 ms/div"));
        decimFactorComboBox->setItemText(2, tr("0.2 ms/div"));
        decimFactorComboBox->setItemText(3, tr("80 us/div"));
        decimFactorComboBox->setItemText(4, tr("40 us/div"));
        decimFactorComboBox->setItemText(5, tr("20 us/div"));
        decimFactorComboBox->setItemText(6, tr("8 us/div"));
        decimFactorComboBox->setItemText(7, tr("4 us/div"));
        decimFactorComboBox->setItemText(8, tr("2 us/div"));
        decimFactorComboBox->setItemText(9, tr("0.8 us/div"));
        decimFactorComboBox->setItemText(10, tr("0.4 us/div"));
        decimFactorComboBox->installEventFilter(this);
        decimFactorComboBox->setToolTip(tr("Time scan"));
        currentDiagram = SIGNAL_DIAGRAM;
    }

    if(diagramType == 1)
    {
        decimFactorComboBox->setItemText(0, tr("25 KHz"));
        decimFactorComboBox->setItemText(1, tr("50 KHz"));
        decimFactorComboBox->setItemText(2, tr("100 KHz"));
        decimFactorComboBox->setItemText(3, tr("250 KHz"));
        decimFactorComboBox->setItemText(4, tr("500 KHz"));
        decimFactorComboBox->setItemText(5, tr("1000 KHz"));
        decimFactorComboBox->setItemText(6, tr("2500 KHz"));
        decimFactorComboBox->setItemText(7, tr("5 MHz"));
        decimFactorComboBox->setItemText(8, tr("10 MHz"));
        decimFactorComboBox->setItemText(9, tr("25 MHz"));
        decimFactorComboBox->setItemText(10, tr("50 MHz"));
        decimFactorComboBox->installEventFilter(this);
        decimFactorComboBox->setToolTip(tr("Samples Freq"));
        currentDiagram = SPECTR_DIAGRAM;
    }
}

void SettingsToolBar::pointNumChanged(int comboBoxIndex)
{
    int pointNum = 8192;

    switch(comboBoxIndex)
    {
    case 0: pointNum = 8192; break;
    case 1: pointNum = 4096; break;
    case 2: pointNum = 2048; break;
    case 3: pointNum = 1024; break;
    }
    qDebug() << "Set point num = " << pointNum;
    signalSource->setPointNum(pointNum);
}

void SettingsToolBar::decimFactorChange(int index)
{
    sample_frequency decim_factor = sampe_frequency_2500kHz;

    switch(index)
    {
    case 0: decim_factor = sampe_frequency_25kHz; break;
    case 1: decim_factor = sampe_frequency_50kHz; break;
    case 2: decim_factor = sampe_frequency_100kHz; break;
    case 3: decim_factor = sampe_frequency_250kHz; break;
    case 4: decim_factor = sampe_frequency_500kHz; break;
    case 5: decim_factor = sampe_frequency_1000kHz; break;
    case 6: decim_factor = sampe_frequency_2500kHz; break;
    case 7: decim_factor = sampe_frequency_5MHz; break;
    case 8: decim_factor = sampe_frequency_10MHz; break;
    case 9: decim_factor = sampe_frequency_25MHz; break;
    case 10: decim_factor = sampe_frequency_50MHz; break;
    }

    signalSource->setSamplesFrequncy(decim_factor);
    emit changeDecimFactor(decim_factor);
    timeScanIndex = index;
}

void SettingsToolBar::scopeSizeChange(int s)
{
    emit changeScopeSize(s);
}

void SettingsToolBar::setComboBoxFromDecimFactor(int _decim_factor)
{
    switch(_decim_factor)
    {
    case 2000: decimFactorComboBox->setCurrentIndex(0); break;
    case 1000: decimFactorComboBox->setCurrentIndex(1); break;
    case 500: decimFactorComboBox->setCurrentIndex(2); break;
    case 200: decimFactorComboBox->setCurrentIndex(3); break;
    case 100: decimFactorComboBox->setCurrentIndex(4); break;
    case 50: decimFactorComboBox->setCurrentIndex(5); break;
    case 20: decimFactorComboBox->setCurrentIndex(6); break;
    case 10: decimFactorComboBox->setCurrentIndex(7); break;
    case 5: decimFactorComboBox->setCurrentIndex(8); break;
    case 2: decimFactorComboBox->setCurrentIndex(9); break;
    case 1: decimFactorComboBox->setCurrentIndex(10); break;
    }
}

bool SettingsToolBar::eventFilter(QObject *target, QEvent *event)
{
    return eventKeyStub(target, event);
}

void SettingsToolBar::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        setWindowTitle(tr("Control device"));
        if(currentDiagram == SIGNAL_DIAGRAM)
            divTimeChanged(0);
        if(currentDiagram == SPECTR_DIAGRAM)
            divTimeChanged(1);
    }
    else
        QWidget::changeEvent(event);
}
