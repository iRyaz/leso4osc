/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef PLAYTOOLBAR_H
#define PLAYTOOLBAR_H

#include <QToolBar>
#include <QAction>
#include <QIcon>
#include <QTimer>
#include <QEvent>
#include <QLayout>

#include "include/osc_global.h"
#include "include/ui_config.h"

#define PLAY_BUTTON_SIZE 24 // Размер кнопок управления

/*
 * Тулбар, управляющий остановкой/возобновлением экрана
 * Имеет две взаимосвязанные кнопки старт и стоп
*/
class PlayToolBar : public QToolBar
{
    Q_OBJECT

    QAction *startAction;
    QAction *stopAction;
    bool startFlag;

protected:
    void changeEvent(QEvent *event);

public:
    PlayToolBar(QToolBar *parent = 0);
    ~PlayToolBar();

    bool isStart() { return startFlag; }

public slots:
    void start(bool toggled);
    void stop(bool toggled);
};

#endif // PLAYTOOLBAR_H
