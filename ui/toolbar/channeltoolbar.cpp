/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "channeltoolbar.h"

ChannelToolBar::ChannelToolBar(SignalSource *control, MEASURE_CHANNEL_t *ch) : popupMenu(ch)
{
    device_control = control;
    channel = ch;

    setMovable(false);
    setFloatable(true);
    ampScanComboBox = new QComboBox;
    inputModeComboBox = new QComboBox;
    channelMenuAction = new QToolButton;
    channelMenuAction->setIcon(QIcon(channel->ADCIconName));
    channelMenuAction->setCheckable(false);

    ampScanComboBox->installEventFilter(this);
    inputModeComboBox->installEventFilter(this);
    channelMenuAction->installEventFilter(this);

    ampScanComboBox->setToolTip(tr("Max Amplitude"));
    inputModeComboBox->setToolTip(tr("Input mode"));

    int channel_color = ((channel->color)&0xFFFFFF00)>>8;

    ampScanComboBox->setStyleSheet("QComboBox { color: #" +
        QString::number(channel_color, 16) + ";" + "border-color: blue; " + "}");

    inputModeComboBox->setStyleSheet("QComboBox {color: #" + QString::number(channel_color, 16) + "; }");

    addWidget(channelMenuAction);
    addWidget(ampScanComboBox);
    addWidget(inputModeComboBox);

    InitInputModeComboBox();
    InitAmpScanComboBox();

    setWindowTitle(tr("Channel ") + QString(channel->id));

    connect(ampScanComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setAmpScan(int)));
    connect(inputModeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeInputMode(int)));
    connect(channelMenuAction, SIGNAL(clicked()), channelMenuAction, SLOT(showMenu()));

    en_flag = &(channel->enabled);

    setAmpScan(ampScanComboBox->currentIndex());
    channelMenuAction->setMenu(&popupMenu);

    ampScanComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    inputModeComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);

    ampScanIndex = 0;
    inputModeIndex = 0;

    layout()->setSpacing(TOOL_BAR_SPACING);
}

ChannelToolBar::~ChannelToolBar()
{
    delete ampScanComboBox;
    delete inputModeComboBox;
}

void ChannelToolBar::InitInputModeComboBox()
{
    inputModeComboBox->addItem(tr("Open"));
    inputModeComboBox->addItem(tr("Close"));
}

void ChannelToolBar::InitAmpScanComboBox()
{
    ampScanComboBox->addItem(tr("Off"));
    ampScanComboBox->addItem(tr("20 V"));
    ampScanComboBox->addItem(tr("8 V"));
    ampScanComboBox->addItem(tr("4 V"));
    ampScanComboBox->addItem(tr("2 V"));
    ampScanComboBox->addItem(tr("0.8 V"));
    ampScanComboBox->addItem(tr("0.4 V"));
    ampScanComboBox->addItem(tr("0.2 V"));
    ampScanComboBox->addItem(tr("0.08 V"));
    ampScanComboBox->addItem(tr("0.04 V"));
}

void ChannelToolBar::enableChannel(bool en)
{
    device_control->toggleChannel(channel->channelNum, en);
    *en_flag = en;

    emit channelEnableSignal();
}

void ChannelToolBar::setAmpScan(int index)
{
    ampScanComboBox->setCurrentIndex(index);
    AMP_SCAN_t ampIndex = AMP_SCAN_1_500;
    ampScanIndex = index;

    switch(index)
    {
    case 0:
        ampIndex = AMP_SCAN_1_500;
        enableChannel(false);
        break;
    case 1:
        ampIndex = AMP_SCAN_1_500;
        enableChannel(true);
        break;
    case 2:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_200;
        break;
    case 3:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_100;
        break;
    case 4:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_50;
        break;
    case 5:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_20;
        break;
    case 6:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_10;
        break;
    case 7:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_5;
        break;
    case 8:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_2;
        break;
    case 9:
        enableChannel(true);
        ampIndex = AMP_SCAN_1_1;
        break;
    }

    device_control->setAmplitudeScan(channel->channelNum, ampIndex);
    channel->ampScan = ampIndex;
    popupMenu.setOffsetStep(channel);
    emit changeAmpComboBox(channel);
}

void ChannelToolBar::changeInputMode(int index)
{
    inputModeIndex = index;
    if(index == 0)
        device_control->setCloseInput(channel->channelNum, false);
    else
        device_control->setCloseInput(channel->channelNum, true);
}

bool ChannelToolBar::eventFilter(QObject *target, QEvent *event)
{
    return eventKeyStub(target, event);
}

void ChannelToolBar::reset()
{
    inputModeComboBox->setCurrentIndex(0);
    ampScanComboBox->setCurrentIndex(0);
    channel->ampScan = AMP_SCAN_1_500;
    channel->enabled = false;
    channel->inputMode = INPUT_MODE_OPEN;
    device_control->setAmplitudeScan(channel->channelNum, channel->ampScan);
    popupMenu.reset();
}

void ChannelToolBar::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        ampScanComboBox->setToolTip(tr("Max Amplitude"));
        inputModeComboBox->setToolTip(tr("Input mode"));
        setWindowTitle(tr("Channel ") + QString(channel->id));

        inputModeComboBox->setItemText(0,tr("Open"));
        inputModeComboBox->setItemText(1, tr("Close"));

        ampScanComboBox->setItemText(0, tr("Off"));
        ampScanComboBox->setItemText(1, tr("20 V"));
        ampScanComboBox->setItemText(2, tr("8 V"));
        ampScanComboBox->setItemText(3, tr("4 V"));
        ampScanComboBox->setItemText(4, tr("2 V"));
        ampScanComboBox->setItemText(5, tr("0.8 V"));
        ampScanComboBox->setItemText(6, tr("0.4 V"));
        ampScanComboBox->setItemText(7, tr("0.2 V"));
        ampScanComboBox->setItemText(8, tr("0.08 V"));
        ampScanComboBox->setItemText(9, tr("0.04 V"));
    }
    else
        QWidget::changeEvent(event);
}
