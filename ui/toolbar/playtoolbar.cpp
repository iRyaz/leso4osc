/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "playtoolbar.h"

PlayToolBar::PlayToolBar(QToolBar *parent) : QToolBar(parent)
{
    setMovable(false);

    startAction = addAction(QIcon(":/button_icons/start_button.png"), tr("Start"));
    stopAction = addAction(QIcon(":/button_icons/stop_button.png"), tr("Stop"));

    startAction->setCheckable(true);
    stopAction->setCheckable(true);

    connect(startAction, SIGNAL(toggled(bool)), this, SLOT(start(bool)));
    connect(stopAction, SIGNAL(toggled(bool)), this, SLOT(stop(bool)));

    startAction->setChecked(true);
    startFlag = true;

    setIconSize(QSize(PLAY_BUTTON_SIZE, PLAY_BUTTON_SIZE));
    this->layout()->setSpacing(TOOL_BAR_SPACING);
}

PlayToolBar::~PlayToolBar()
{

}

void PlayToolBar::start(bool toggled)
{
    if(toggled)
    {
        stopAction->setChecked(false);
        startFlag = true;
    }
    else
    {
        stopAction->setChecked(true);
        startFlag = false;
    }
}

void PlayToolBar::stop(bool toggled)
{
    if(toggled)
    {
        startAction->setChecked(false);
        startFlag = false;
    }
    else
    {
        startFlag = true;
        startAction->setChecked(true);
    }
}

void PlayToolBar::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        startAction->setText(tr("Start"));
        stopAction->setText(tr("Stop"));
    }
    else
        QWidget::changeEvent(event);
}

