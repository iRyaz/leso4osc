/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef CHANNELTOOLBAR_H
#define CHANNELTOOLBAR_H

#include <QToolBar>
#include <QCheckBox>
#include <QComboBox>
#include <QAction>
#include <QToolButton>
#include <QDebug>
#include <QKeyEvent>

#include "include/osc_global.h"
#include "base/signalsource.h"
#include "../channelpopupwidget.h"

/*
 * Панель настроек одного канала измерения
*/
class ChannelToolBar : public QToolBar
{

Q_OBJECT

    SignalSource *device_control;   // Источник сигнала
    QToolButton *channelMenuAction; // Меню дополнительных настроек (кнопка вызова)
    ChannelPopupWidget popupMenu;   // Меню дополнительных настроек
    QComboBox *ampScanComboBox;     // Выпадающий список выбора разверток по амплитуде
    QComboBox *inputModeComboBox;   // Режим входа (Открытый/Закрытый)

    void InitAmpScanComboBox();
    void InitInputModeComboBox();

    QString getChannelChar(CHANNEL_t channel);
    QString getChannelColor(CHANNEL_t channel);

    MEASURE_CHANNEL_t *channel;

    bool *en_flag;  // Указатель на переменную, показывающую включен ли канал

    int ampScanIndex;   // Текущая развертка по амплитуде
    int inputModeIndex; // Текущий режим входа

public:
    ChannelToolBar(SignalSource *control, MEASURE_CHANNEL_t *ch);
    ~ChannelToolBar();
    MEASURE_CHANNEL_t *getChannel() { return channel; }
    ChannelPopupWidget *getPopupMenu() { return &popupMenu; }
    int getAmpScanIndex() { return ampScanIndex; }
    int getInputMode() { return inputModeIndex; }

public slots:
    void enableChannel(bool en);    // Включить/Выключить канал
    void setAmpScan(int index);     // Установить развертку по амплитуде
    void changeInputMode(int index);    // Сменить режим входа
    void reset();   // Сбросить настройки канала

signals:
    void changeAmpComboBox(MEASURE_CHANNEL_t*);
    void channelEnableSignal();

    void spectrCursorEn(int, bool);
    void signalCursorEn(int, bool);

protected:
    bool eventFilter(QObject *target, QEvent *event);
    void changeEvent(QEvent *event);

    friend class LESOMainWindow;
};

#endif // CHANNELTOOLBAR_H
