/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef TRIGGERTOOLBAR_H
#define TRIGGERTOOLBAR_H

#include <QToolBar>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QAction>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QTranslator>
#include <QCoreApplication>
#include <QLayout>

#include "base/common_func.h"
#include "include/osc_global.h"
#include "include/ui_config.h"

/*
 * Выпадающий список, где выбирается источник для триггера из списка каналов
 * Хранит в себе номер канала CHANNEL_t,
 * этот номер канала независит от индекса (местоположение канала в выпадающем списке)
*/
class ChannelSourceComboBox : public QComboBox
{
    Q_OBJECT

    struct ComboBoxChannel_t
    {
        ComboBoxChannel_t(MEASURE_CHANNEL_t *_ch) {
            ch = _ch;
            comboBoxIndex = -1;
        }

        MEASURE_CHANNEL_t *ch;
        int comboBoxIndex;
    };

    QList<ComboBoxChannel_t*> channelLst;

public:
    ChannelSourceComboBox()
    {
        setStyleSheet("width: 100");
    }

    void addChannel(MEASURE_CHANNEL_t *ch)
    {
        ComboBoxChannel_t *combo = new ComboBoxChannel_t(ch);
        if(ch->enabled)
        {
            combo->comboBoxIndex = count();
            addItem(QIcon(ch->ADCIconName), tr("Channel ") + QString(ch->id));
        }

        channelLst.append(combo);
    }

    int getCurrentChannel()
    {
        return getCurrentChannel(currentIndex());
    }

    int getCurrentChannel(int currentIndex)
    {
        int ret = 0;
        for(int i(0); i < channelLst.size(); i++)
        {
            if(channelLst.at(i)->comboBoxIndex == currentIndex)
                ret = channelLst.at(i)->ch->channelNum;
        }

        return ret;
    }

    void setCurrentChannel(int chNum)
    {
        for(int i(0); i < channelLst.size(); i++)
        {
            if(channelLst.at(i)->ch->channelNum == chNum && channelLst.at(i)->ch->enabled)
            {
                setCurrentIndex(i);
                break;
            }
        }
    }

    void update()
    {
        int currentChannel = getCurrentChannel();
        int currentChannelIndex = 0;
        clear();
        for(int i(0); i < channelLst.size(); i++)
        {
            if(channelLst.at(i)->ch->enabled)
            {
                MEASURE_CHANNEL_t *ch = channelLst.at(i)->ch;
                channelLst.at(i)->comboBoxIndex = count();
                if(ch->channelNum == currentChannel)
                    currentChannelIndex = count();

                addItem(QIcon(ch->ADCIconName), tr("Channel ") + QString(ch->id));
            }
            else
            {
                channelLst.at(i)->comboBoxIndex = -1;
            }
        }

        setCurrentIndex(currentChannelIndex);
    }

protected:
    void changeEvent(QEvent *event)
    {
        if (event->type() == QEvent::LanguageChange)
        {
            update();
        }
        else
            QWidget::changeEvent(event);
    }
};

// Панель инструментов для управления триггером
class TriggerToolBar : public QToolBar
{
    Q_OBJECT

    QDoubleSpinBox *levelSpinBox;   // Уровень триггера
    QComboBox *triggerEdge; // Фронт триггера
    QComboBox *triggerMode; // Режим триггера
    ChannelSourceComboBox *triggerSource;   // Выпадающий список, выбор источника для триггера
    TRIGGER_t *trigger; // Указатель на сам виртуальный триггер
    double getLevelStep(AMP_SCAN_t ampDivIndex);    // Возвратить шаг уровня триггера в зависимости от положения аттенюатора

protected:
    void changeEvent(QEvent *event);

public:
    TriggerToolBar(TRIGGER_t *_trigger);
    ~TriggerToolBar();
    QDoubleSpinBox *levelTriggerSpinBox() { return levelSpinBox; }
    void addChannel(MEASURE_CHANNEL_t *ch); // Добавить канала

public slots:
    void changeTriggerMode(int comboBoxIndex);
    void changeTriggerEdge(int comboBoxIndex);
    void changeTriggerSource(int comboBoxIndex);

    void changeLevelTrigger(double);
    void updateTriggerLevelSpinBox(double level);
    void setTriggerLevelStep(MEASURE_CHANNEL_t *ch);
    void reset();   // Сбросить настройки
    void updateChannelSourceList();
    void setTranslator(QTranslator *t);

protected:
    bool eventFilter(QObject *target, QEvent *event);

signals:
    void levelTriggerSignal(double);

    friend class LESOMainWindow;

};

#endif // TRIGGERTOOLBAR_H
