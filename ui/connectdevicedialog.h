/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef CONNECTDEVICEDIALOG_H
#define CONNECTDEVICEDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QDebug>
#include <QTimer>
#include <QListWidgetItem>

#include "base/signalsource.h"
#include "include/osc_global.h"
#include "include/ui_config.h"

#define DEFAULT_LIST_ITEM_ICON 64

namespace Ui {
class ConnectDeviceDialog;
}

class ConnectDeviceDialog : public QDialog
{
    Q_OBJECT

    QComboBox *deviceComboBox;
    int selectType;
    int deviceNum;
    SignalSource *signalSource;
    const char *deviceName;

    void updateDeviceList();
    bool showStartup;

public:
    explicit ConnectDeviceDialog(SignalSource *s, QWidget *parent = 0);
    ~ConnectDeviceDialog();

    int getDeviceNum();
    const char *getDeviceName() { return deviceName; }
    bool isStartup() { return showStartup; }
    void setStartupFlag(bool startup);

public slots:
    void selectButtonSlot();
    void cancelButtonSlot();
    void updateButtonSlot();
    void itemDoubleClicked(QListWidgetItem *item);
    void showChecked(bool checked);

protected:
    void changeEvent(QEvent *event);

private:
    Ui::ConnectDeviceDialog *ui;
};

#endif // CONNECTDEVICEDIALOG_H
