/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef LESOMAINWINDOW_H
#define LESOMAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QMdiSubWindow>
#include <QDir>
#include <QSettings>
#include <QTranslator>

#include "connectdevicedialog.h"
#include "plotting/signalplot/signalplotform.h"
#include "plotting/spectrplot/spectrplotform.h"
#include "ui/lesotabwidget.h"
#include "ui/toolbar/playtoolbar.h"
#include "ui/toolbar/triggertoolbar.h"
#include "ui/toolbar/settingstoolbar.h"
#include "ui/toolbar/channeltoolbar.h"
#include "base/signalsource.h"
#include "base/measurewidget.h"
#include "editmeasuredialog.h"
#include "lesoaboutdialog.h"
#include "include/osc_global.h"

#define GUI_SETTINGS_FILE "settings/gui.settings"
#define DEVICE_SETTINGS_FILE "settings/device.settings"
#define CH_SIZE signalSource.getChannelBufferSize()

#define SHOW_STATUS_LABEL_FLAG 0

namespace Ui {
class LESOMainWindow;
}

class LESOMainWindow : public QMainWindow
{
    Q_OBJECT

    enum UI_LANG { LANG_EN = 0, LANG_RU = 1 };  // Язык программы
    UI_LANG programLang;            // Текущий язык
    QTranslator translator;

    LESOAboutDialog *aboutDialog;   // Диалог о программе

    QSettings *guiSettings;         // Класс хранящий настройки интерфейса программы
    QSettings *deviceSettings;      // Класс хранящий настройки устройства (Развертка, тип входа, и.т.д)

    MeasureWidget *measureTable;    // Таблица измерений

    // Панели инструментов в главном окне программы
    SettingsToolBar *settingsToolBarPtr;
    PlayToolBar *playToolBar;
    TriggerToolBar *triggerToolBar;
    QList<ChannelToolBar*> channelsUIList;

    TRIGGER_t deviceTrigger;    // Триггер устройства
    SignalSource signalSource;  // Источник отсчетов (Устройство или программная эмуляция)

    // Статус программы
    QLabel *rxBytesLabel;
    void showRXBytesStatus(int rx); // Вывести с строку статуса количество принятых байтов от прибора

    LESOTabWidget *mainTabWidget;   // Центральный виджет основного окна

    ConnectDeviceDialog *connectDialog; // Диалог выбора устройства
    EditMeasureDialog *measureDialog;   // Диалог добавления измерения

    bool saveDeviceSettingsFlag;// Если true, то настройки устройства сохраняются для следующего запуска

    bool showStartupConnectDialog;  // Показывать диалог выбора девайса при стрте программы

public:
    explicit LESOMainWindow(QWidget *parent = 0);
    ~LESOMainWindow();
    void registerChannel(MEASURE_CHANNEL_t *ch);    // Добавить измерительный канал устройства в программу

    void loadSettings();    // Загрузить все настройки из файлов
    void loadLocale();      // Установить сохраненный язык интерфейса
    void saveSettings();    // Сохранить настройки в файл

    QMenu *createPopupMenu();   // Функция заглушка

public slots:
    void showSpectrTab(bool toggled);   // Отобразить вкладку спектра
    void showSignalTab(bool toggled);   // Отобразить вкладку сигнала

    void readDataSlot();                // Вызывается когда все данные прочитаны с устройства

    void connectDeviceSlot(bool settings_is_loaded = false);// Вызов диалога подключения устройств

    void uiEnabled();   // Сделать UI активным
    void uiDisabled();  // Сделать UI неактивным

    void setAmpScanChannel(MEASURE_CHANNEL_t *ch); // Переключить аттенюатор на самом устройстве

    void savePlot();    // Сохранить график

    void addMeasureSlot();  // Добавления измерения
    void editMeasureSlot(); // Редактирования измерения

    // Выбор нужного количества отсчетов
    void samples8192ChangeSlot(bool);
    void samples4096ChangeSlot(bool);
    void samples2048ChangeSlot(bool);
    void samples1024ChangeSlot(bool);

    // Флаг разрешения сохранения настроек устройства
    void saveDeviceSettingsFlagToggle(bool on) { saveDeviceSettingsFlag = on; }

    // Выбор языка UI
    void changeEnLanguage(bool);
    void changeRuLanguage(bool);

    // Показать главное окно программы
    // Если settings_is_loaded = true, то загрузить настройки из файла
    void show(bool settings_is_loaded);

private:
    Ui::LESOMainWindow *ui;

protected:
    void keyPressEvent(QKeyEvent *event);
    void changeEvent(QEvent *event);
};

#endif // LESOMAINWINDOW_H
