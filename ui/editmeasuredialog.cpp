/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "editmeasuredialog.h"
#include "ui_editmeasuredialog.h"

EditMeasureDialog::EditMeasureDialog(MeasureWidget *measureTablePtr, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditMeasureDialog)
{
    table = measureTablePtr;

    ui->setupUi(this);

    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->addButton, SIGNAL(clicked()), this, SLOT(addMeasure()));

    initComboBox();

    ui->selectMeasureComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    ui->selectChannelComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);

    setStyleSheet("QDialog { background: #" + QString::number(TOOL_BARS_COLOR, 16) + "}");
}

EditMeasureDialog::~EditMeasureDialog()
{
    delete ui;
}

void EditMeasureDialog::initComboBox()
{
    ui->selectMeasureComboBox->addItem(tr("Amplitude"));
    ui->selectMeasureComboBox->addItem(tr("Min Value"));
    ui->selectMeasureComboBox->addItem(tr("Max Value"));
    ui->selectMeasureComboBox->addItem(tr("Middle Value"));
    ui->selectMeasureComboBox->addItem(tr("Middle Quad Value"));
    ui->selectMeasureComboBox->addItem(tr("Frequency"));

    ui->selectChannelComboBox->addItem(tr("Channel A"));
    ui->selectChannelComboBox->addItem(tr("Channel B"));
    ui->selectChannelComboBox->addItem(tr("Channel C"));
    ui->selectChannelComboBox->addItem(tr("Channel D"));
}

void EditMeasureDialog::addMeasure()
{
    MEASURE_t addedMeasure;

    CHANNELS_LST *channelsLst = table->getChannels();

    switch(ui->selectChannelComboBox->currentIndex())
    {
    case CH_A: addedMeasure.channel = channelsLst->at(0); break;
    case CH_B: addedMeasure.channel = channelsLst->at(1); break;
    case CH_C: addedMeasure.channel = channelsLst->at(2); break;
    case CH_D: addedMeasure.channel = channelsLst->at(3); break;
    }

    switch(ui->selectMeasureComboBox->currentIndex())
    {
    case 0: addedMeasure.type = AMPLITUDE; break;
    case 1: addedMeasure.type = MIN_VALUE; break;
    case 2: addedMeasure.type = MAX_VALUE; break;
    case 3: addedMeasure.type = MIDDLE_VALUE; break;
    case 4: addedMeasure.type = MIDDLE_QUAD_VALUE; break;
    case 5: addedMeasure.type = FREQUENCY; break;
    }

    addedMeasure.captureNum = ui->captureSpinBox->value();
    addedMeasure.comment = ui->commentLineEdit->text();

    if(mode == EditMeasureDialog::NEW_MEASURE)
    {
        table->addMeasure(addedMeasure);
        table->updateTable();
    }

    if(mode == EditMeasureDialog::EDIT_MEASURE)
    {
        int rowNum = table->getSelectMeasureNum();
        table->getMeasureList()->replace(rowNum, addedMeasure);
        table->updateTable();
    }

    close();
}

void EditMeasureDialog::loadMeasureSettings()
{
    if(table->getMeasureList()->size() == 0)
    {
        ui->addButton->setEnabled(false);
        return;
    }

    ui->addButton->setEnabled(true);
    int rowNum = table->getSelectMeasureNum();
    MEASURE_t currentMeasure = table->getMeasureList()->at(rowNum);
    ui->selectChannelComboBox->setCurrentIndex(currentMeasure.channel->channelNum);
    ui->selectMeasureComboBox->setCurrentIndex(currentMeasure.type);
    ui->captureSpinBox->setValue(currentMeasure.captureNum);
    ui->commentLineEdit->setText(currentMeasure.comment);

}

void EditMeasureDialog::showDialog(DIALOG_MODE _mode)
{
    mode = _mode;

    if(_mode == EditMeasureDialog::NEW_MEASURE)
    {
        exec();
    }

    if(_mode == EditMeasureDialog::EDIT_MEASURE)
    {
        loadMeasureSettings();
        exec();
    }
}

void EditMeasureDialog::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        ui->selectMeasureComboBox->setItemText(0, tr("Amplitude"));
        ui->selectMeasureComboBox->setItemText(1, tr("Min Value"));
        ui->selectMeasureComboBox->setItemText(2, tr("Max Value"));
        ui->selectMeasureComboBox->setItemText(3, tr("Middle Value"));
        ui->selectMeasureComboBox->setItemText(4, tr("Middle Quad Value"));
        ui->selectMeasureComboBox->setItemText(5, tr("Frequency"));

        ui->selectChannelComboBox->setItemText(0, tr("Channel A"));
        ui->selectChannelComboBox->setItemText(1, tr("Channel B"));
        ui->selectChannelComboBox->setItemText(2, tr("Channel C"));
        ui->selectChannelComboBox->setItemText(3, tr("Channel D"));
    }
    else
        QWidget::changeEvent(event);
}
