/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef EDITMEASUREDIALOG_H
#define EDITMEASUREDIALOG_H

#include <QDialog>

#include "base/measurewidget.h"
#include "include/ui_config.h"

namespace Ui {
class EditMeasureDialog;
}

class EditMeasureDialog : public QDialog
{
    Q_OBJECT

    void initComboBox();

    MeasureWidget *table;

protected:
    void changeEvent(QEvent *event);

public:
    explicit EditMeasureDialog(MeasureWidget *measureTablePtr, QWidget *parent = 0);
    ~EditMeasureDialog();

    enum DIALOG_MODE { NEW_MEASURE, EDIT_MEASURE };
    DIALOG_MODE mode;
    void loadMeasureSettings();

private:
    Ui::EditMeasureDialog *ui;

public slots:
    void addMeasure();
    void showDialog(DIALOG_MODE _mode);

};

#endif // EDITMEASUREDIALOG_H
