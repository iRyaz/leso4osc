
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "lesotabwidget.h"

LESOTabWidget::LESOTabWidget()
{
    signaTabFlag = false;
    spectrTabFlag = false;

    signalTab = new SignalPlotForm;
    spectrTab = new SpectrPlotForm;

    setMovable(true);

    currentDecimFactor = 2000;
    setAxisMaxTime(2000);

    scopeSize = SCOPE_SIZE;

    connect(this, SIGNAL(currentChanged(int)), this, SLOT(tabChangedInternalSlot(int)));
}

LESOTabWidget::~LESOTabWidget()
{
}

void LESOTabWidget::addChannel(MEASURE_CHANNEL_t *ch)
{
    signalTab->addChannel(ch);
    spectrTab->addChannel(ch);
}

void LESOTabWidget::addDiagramSignal()
{
    if(signaTabFlag == false)
    {
        addTab(signalTab, QIcon(":/button_icons/time_diagram.png"), tr("Signal"));
        signalTab->show();
    }
}

void LESOTabWidget::addDiagramSpectr()
{
    if(spectrTabFlag == false)
    {
        addTab(spectrTab, QIcon(":/button_icons/spectr_diagram.png"), tr("Spectr"));
    }
}

void LESOTabWidget::closeDiagramSignal()
{
    removeTab(indexOf(signalTab));
}

void LESOTabWidget::closeDiagramSpectr()
{
    removeTab(indexOf(spectrTab));
}


void LESOTabWidget::setAxisMaxTime(int decim_factor)
{
    currentDecimFactor = decim_factor;
    current_samples_frequency = BASE_FREQUENCY/decim_factor;
}

void LESOTabWidget::setAxisMaxAmp(MEASURE_CHANNEL_t *channel)
{
    signalTab->setMaxAmpDiv(channel);
    spectrTab->setMaxAmpDiv(channel);
}

void LESOTabWidget::update()
{
    signalTab->update(currentDecimFactor);
    spectrTab->update(currentDecimFactor);
}

void LESOTabWidget::setScopeSize(int s)
{
    scopeSize = s;
}

void LESOTabWidget::tabChangedInternalSlot(int index)
{
    if(widget(index) == signalTab)
        emit tabChanged(0);
    if(widget(index) == spectrTab)
        emit tabChanged(1);
}

