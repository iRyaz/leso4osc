
/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details
\version 0.3 
\date 20.12.2015
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef LESO4_EXPORT_H
#define LESO4_EXPORT_H

#ifdef _WINDOWS
#ifdef BUILDING_LESO_LIB_DLL
#define LESO_DLL __declspec(dllexport)
#else
#define LESO_DLL __declspec(dllimport)
#endif
#else
#define LESO_DLL
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define LESO4_DEVICE_NAME "LESO4.1_ER"
#define DIV_NUM 9			//!< Количество диапазонов по амплитуде

typedef enum LESO4_STATUS_t
{
	LESO4_OK = 0,			///! Все хорошо
	LESO4_ERROR = -1		///! Произошла ошибка
} LESO4_STATUS;

/*!
	\param[in] descriptor указатель на строку дескриптор устройства
	\return возвращает -1 если не удалось открыть устройство
*/
LESO4_STATUS LESO_DLL leso4Open(const char *descriptor);

int LESO_DLL leso4IsOpen();

#define SAMPLES_8K		(8192)		//!< Количество отсчетов для канала 8192
#define SAMPLES_4K		(4096)		//!< Количество отсчетов для канала 4096
#define SAMPLES_2K		(2048)		//!< Количество отсчетов для канала 2048
#define SAMPLES_1K		(1024)		//!< Количество отсчетов для канала 1024

/*!
	Установить количество получаемых отсчетов для прибора
	\param[in] samples_num количество отсчетов для одного канала
	\return -1 если не получилось связаться с прибором
*/
LESO4_STATUS LESO_DLL leso4SetSamplesNum(int samples_num);

/*!
	Функция возвращает текущее количество отсчетов
*/
int LESO_DLL leso4GetSamplesNum();

#define CHANNEL_A		(0)		//!< Канал A
#define CHANNEL_B		(1)		//!< Канал B
#define CHANNEL_C		(2)		//!< Канал C
#define CHANNEL_D		(3)		//!< Канал D

#define CHANNEL_NUM		(4)		//!< Число каналов

/*!
	Включить канал
	\param[in] channel номер канала
*/
LESO4_STATUS LESO_DLL leso4EnableChannel(int channel);

/*!
	Выключить канал
	\param[in] channel номер канала
*/
LESO4_STATUS LESO_DLL leso4DisableChannel(int channel);

/*!
	\param[in] channel номер канала
	\return true - если канал включен
*/
int LESO_DLL leso4IsChannelEnable(int channel);

/*
 	 Эквивалентная частота		Коэффициент
 	 дискретизации				децимации
*/
typedef enum 
{
	sampe_frequency_50MHz	=	1,
	sampe_frequency_25MHz	=	2,
	sampe_frequency_10MHz	=	5,
	sampe_frequency_5MHz	=	10,
	sampe_frequency_2500kHz =	20,
	sampe_frequency_1000kHz =	50,
	sampe_frequency_500kHz	=	100,
	sampe_frequency_250kHz	=	200,
	sampe_frequency_100kHz	=	500,
	sampe_frequency_50kHz	=	1000,
	sampe_frequency_25kHz	=	2000,
	sampe_frequency_END,
} sample_frequency;

/*!
	Установить развертку по времени
	\param[in] samp_freq коэффициент децимации
*/
LESO4_STATUS LESO_DLL leso4SetSamplFreq(sample_frequency freq);

/*!
	\return получить текущее значение коэффициента децимации
*/
int LESO_DLL leso4GetSamplFreq();

typedef enum 
{
	max_voltage_20V,		//!< Делитель сигнала 1:500
	max_voltage_8V,			//!< Делитель сигнала 1:200
	max_voltage_4V,			//!< Делитель сигнала 1:100
	max_voltage_2V,			//!< Делитель сигнала 1:50
	max_voltage_800mV,		//!< Делитель сигнала 1:20
	max_voltage_400mV,		//!< Делитель сигнала 1:10
	max_voltage_200mV,		//!< Делитель сигнала 1:5
	max_voltage_80mV,		//!< Делитель сигнала 1:2
	max_voltage_40mV,		//!< Делитель сигнала 1:1
	max_voltage_END
} max_voltage;

/*!
	Переключить делитель для заданного канала
	\param[in] channel номер канала
	\param[in] ampDiv максимальное значение входного сигнала
*/

LESO4_STATUS LESO_DLL leso4SetAmplitudeScan(int channel, max_voltage ampDiv);

/*!
	Получить текущий коэффициент для заданного канала
	\param[in] channel номер канала
	\return номер развертки
*/
max_voltage LESO_DLL leso4GetAmplitudeScan(int channel);

#define INPUT_MODE_OPEN		(1)		//!< Режим открытый вход
#define INPUT_MODE_CLOSE	(0)		//!< Режим закрытый вход

typedef enum 
{
	input_mode_close,
	input_mode_open
} input_mode;

/*!
	\param[in] channel номер канала
	\param[in] mode режим канала открытый/закрытый
*/
LESO4_STATUS LESO_DLL leso4SetInputMode(int channel, input_mode mode);

/*!
	\param[in] channel номер канала
	\return режим канала открытый/закрытый, для заданного канала
*/
int LESO_DLL leso4GetInputMode(int channel);

/*!
	Сбросить устройство
*/
LESO4_STATUS LESO_DLL leso4Reset();

/*!
	Сбросить микросхему FTDI
*/
LESO4_STATUS LESO_DLL leso4ResetFTDI();

/*!
	Получить номер версии прибора
*/
int LESO_DLL leso4GetVersion();

/*!
	Прочитать данные из устройства во внутренний буфер
	\return количество прочитанных байтов
*/
int LESO_DLL leso4ReadFIFO();

#define NO_ERROR_CODE 0										//!< Ошибок нет
#define ERROR_CODE_DEVICE_NOT_OPEN -1						//!< Устройство не удалось открыть
#define ERROR_CODE_DEVICE_NOT_FOUND -2						//!< Устройство не найдено
#define ERROR_CODE_FTDI_LIB_NOT_LOADED -3					//!< Библиотека FTDI не подключена

/*!
	Получить код последней ошибки
	\return код ошибки, который меньше единицы
*/
int LESO_DLL leso4GetLastErrorCode();

/*!
	\brief Функция предоставляющая доступ к массиву отсчетов выбранного канала.

	Функция копирует в массив buff из внутреннего массива отчеты указанного канала.
	Если buff неинициализирован, либо указан как NULL, то функция фозвращает указатель
	на внутренний буфер, в противном случае на буфер buff.
	\param[in] channel номер канала
	\param[out] buff указатель на массив-контейнер
	\return указатель на массив
*/
void* LESO_DLL leso4GetData(double* buff, int channel);

/*!
	Получить размер массива отсчетов одного канала.
	Количество реально считанных отсчетов.
*/
int LESO_DLL leso4GetChannelFIFOSize();

/*!
	Закрыть устройство
*/
void LESO_DLL leso4CloseDevice();

#ifdef __cplusplus
}
#endif

#endif
