/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef DIAGRAMGRID
#define DIAGRAMGRID

#include <QPen>
#include "include/ui_config.h"

struct DiagramGrid
{
    DiagramGrid(QPen _gridPen, QPen _zeroPen, QPen _subGridPen) : gridPen(_gridPen),
        zeroLineGridPen(_zeroPen), subGridPen(_subGridPen) {}

    DiagramGrid()
    {
        gridPen.setBrush(QColor((DEFAULT_GRID_COLOR & 0xFF000000)>>24,
                                (DEFAULT_GRID_COLOR & 0x00FF0000)>>16,
                                (DEFAULT_GRID_COLOR & 0x0000FF00)>>8,
                                (DEFAULT_GRID_COLOR & 0x000000FF)));

        gridPen.setWidth(DEFAULT_GRID_PEN_WIDTH);
        gridPen.setStyle(DEFAULT_GRID_PEN_STYLE);

        zeroLineGridPen.setBrush(QColor((BASE_TIME_COLOR & 0xFF000000)>>24,
                                        (BASE_TIME_COLOR & 0x00FF0000)>>16,
                                        (BASE_TIME_COLOR & 0x0000FF00)>>8,
                                        (BASE_TIME_COLOR & 0x000000FF)));

        zeroLineGridPen.setWidthF(DEFAULT_ZERO_LINE_GRID_WIDTH);
        zeroLineGridPen.setStyle(DEFAULT_ZERO_LINE_GRID_STYLE);

        subGridPen.setBrush(QColor((DEFAULT_SUB_GRID_COLOR & 0xFF000000)>>24,
                                   (DEFAULT_SUB_GRID_COLOR & 0x00FF0000)>>16,
                                   (DEFAULT_SUB_GRID_COLOR & 0x0000FF00)>>8,
                                   (DEFAULT_SUB_GRID_COLOR & 0x000000FF)));

        subGridPen.setWidth(DEFAULT_SUB_GRID_WIDTH);
        subGridPen.setStyle(DEFAULT_SUB_GRID_STYLE);
    }

    QPen gridPen;
    QPen zeroLineGridPen;
    QPen subGridPen;
};

#endif // DIAGRAMGRID

