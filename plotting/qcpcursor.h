/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.4                                                  **
****************************************************************************/

#ifndef QCPCURSOR_H
#define QCPCURSOR_H

#include <QPen>
#include <QPointF>
#include <QMouseEvent>
#include "include/osc_global.h"
#include "base/common_func.h"
#include "lib/qcustomplot.h"
#include "include/ui_config.h"
#include "base/common_func.h"
#include "include/ui_config.h"

#define DELTA_CHAR 0x394

#define CURSORS_LAYER_NAME "c_layer"
#define TEXT_LABEL_LAYER_NAME "text_label"

/*
 *  PlotCursor - наследник класс QCPAbstrtactItem библиотеки QCustomPlot
 *  Состоит из вертикальной и горизонтальной линии, между которыми рисуется "прицел"
 *  Горизонтальная и вертикальная линия создаются классами QCPItemLine. Прицел рисуется
 *  средствами QCPPainter (наследник класса QPainter). Методы getLim и getTimeLim, являются
 *  чисто виртуальными, их нужно определить в классе наследнике. Эти методы возвращают число константу,
 *  зависящее, от текущей развертки по амплитуде(getLim) и времени(getTimeLim). Эта константа, определяет
 *  на каких координатах, курсор можно захватить мышкой для перемещения. Например в диаграмме спектра
 *  и осциллограмме сигнала Оси X имеют разную размерность, поэтому наследется класс курсоров для
 *  осциллограммы и отдельно для спектра.
 *  Цвет прицела зависит от канала к которому привязан курсор
*/

class PlotCursor : public QCPAbstractItem
{
    QCustomPlot *plot;  // Указатель на класс QCustomPlot
    QCPItemLine *hLine; // Горизонтальная линия
    QCPItemLine *vLine; // Вертикальная линия
    QCPAxis *yAxis;     // Указатель на ось Y, текущего графика
    QPen cursorPen;     // Цвет линий курсоров

    bool selectHLine;   // Горизонтальная линия выбрана ?
    bool selectVLine;   // Вертикальная линия выбрана ?
    bool moveHLine;     // Горизонтальная линия перемещается ?
    bool moveVLine;     // Вертикальная линия перемещается ?
    bool visible;       // Курсоры видно на экране ?

    double currentX;    // Текущие положение курсора по X
    double currentY;    // Текущие положение курсора по Y
    int decim_factor;   // Коэффициент децимации
    AMP_SCAN_t ampScan; // Развертка по амплитуде
    int cursorColor;    // Цвет прицела

    virtual double getLim(AMP_SCAN_t ampScan) = 0;  // Получить диапазон по Y
    virtual double getTimeLim(int d) = 0;           // Получить диапазон по X

public:
    PlotCursor(QCustomPlot *parent);
    bool isVisible() { return visible; }
    void setCursorPointColor(int color) { cursorColor = color; }
    double getX() { return currentX; }
    double getY() { return currentY; }
    QCPAxis *getYAxis() { return yAxis; }
    bool isSelect() { return selectHLine || selectVLine; }  // Выбран ли курсор ?
    bool isMove() { return moveHLine || moveVLine; }        // Перемещается ли курсор ?
    void setDecimFactor(int factor) { decim_factor = factor; }  // Установить развертку по времени для курсора
    void setAmpScan(AMP_SCAN_t scan) { ampScan = scan; }    // Установить развертку по амплитуде для курсора
    void setYAxis(QCPAxis *a);                              // Задать ось Y
    void setCoords(double pixelX, double pixelY);           // Переместить курсор в заданные координаты в пикселях
    void setCoords(QPointF &c);
    void setAxisCoords(double X, double Y);                 // Переместить курсор в заданные координаты по осям
    void setVisible(bool _visible);                         // Включить/выключить отображение курсора на экране

    /*
     * возвращает 1 если любая линия курсора выбрана,
     * в противном случае 0
    */
    int selectCursor(QMouseEvent *event);                   // Вызывается в обработчике перемещеия курсора

    void selectCursorLeftButton(QMouseEvent *event);        // Вызывается в обработчике нажатия кнопки мыши по графику
    void releaseButton(QMouseEvent *event);                 // Вызывается в обработчике отпускания кнопки мыши
    void moveCursor(QMouseEvent *event);                    // Вызывается в обработчике перемещения мишы по графику
    double selectTest(const QPointF &pos, bool onlySelectable, QVariant *details=0) const;

protected:
     void draw(QCPPainter *painter);    //!< Обработчик рисования
};

/*
 *  Класс текстовой метки для двух курсоров в верхнем правом углу экрана
*/

class CursorTextLabel
{
    QCustomPlot *plot;          // Указатель на класс QCustomPlot
    QCPItemText *textLabel;     // QCPTextItem текстовая метка
    PlotCursor *firstCursor;    // Первый курсор
    PlotCursor *secondCursor;   // Второй курсор
    bool visible;               // Показать ?
    QCPAxis *xAxis;             // Ось X
    QCPAxis *yAxis;             // Ось Y

public:
    CursorTextLabel(QCustomPlot *customPlot, PlotCursor *cursor1, PlotCursor *cursor2);
    void setVisible(bool _v);
    bool isVsisble() { return visible; }
    void setYAxis(QCPAxis *a);
    QCPAxis *getYAxis() { return yAxis; }
    void setTextColor(int color);       // Установить цвет текста
    virtual void update();              // Обновить позицию
};

/*
 *  Класс объединяющий два курсора с меткой
 *  Параметер шаблона CURSOR_TYPE это производный класс от
 *  PlotCursor. Классы CURSOR_TYPE создаются в конструкторе и
 *  уничтожаются в деструкторе
*/
template <class CURSOR_TYPE> class CursorsPair
{
    CURSOR_TYPE *firstCursor;               // Первый курсор
    CURSOR_TYPE *secondCursor;              // Второй курсор
    CursorTextLabel *textLabel;             // Текстовая метка
    QCustomPlot *plot;                      // Указатель на QCustomPlot
    QCPAxis *xAxis;                         // Ось X
    QCPAxis *yAxis;                         // Ось Y
    AMP_SCAN_t currentAmpScan;              // Текущая развертка по амплитуде
    int currentDecimFactor;                 // Текущая развертка по времени (коэффициент децимации)
    bool visible;
    MEASURE_CHANNEL_t *sourceChannel;       // Канал с которыми связаны курсоры

public:
    CursorsPair(QCustomPlot *_plot)
    {
        plot = _plot;
        firstCursor = new CURSOR_TYPE(plot);
        secondCursor = new CURSOR_TYPE(plot);

        firstCursor->setVisible(false);
        secondCursor->setVisible(false);

        currentAmpScan = AMP_SCAN_1_500;
        currentDecimFactor = 2000;

        xAxis = plot->xAxis;
        yAxis = plot->yAxis;
        visible = false;
        textLabel = new CursorTextLabel(plot, firstCursor, secondCursor);

        plot->addLayer(CURSORS_LAYER_NAME, plot->layer("main"), QCustomPlot::limAbove);
        firstCursor->setLayer(CURSORS_LAYER_NAME);
        secondCursor->setLayer(CURSORS_LAYER_NAME);
    }

    ~CursorsPair()
    {
        delete firstCursor;
        delete secondCursor;
    }

    bool isVisible() { return visible; }
    void setSourceChannel(MEASURE_CHANNEL_t *ch) { sourceChannel = ch; } // Установить исходный канал
    QCPAxis *getYAxis() { return yAxis; }

    void setYAxis(QCPAxis *axis) // Установить ось Y
    {
        yAxis = axis;
        firstCursor->setYAxis(yAxis);
        secondCursor->setYAxis(yAxis);
    }

    void setDecimFactor(int decim_factor) // Установить коэффициент децимации
    {
        currentDecimFactor = decim_factor;
        firstCursor->setDecimFactor(currentDecimFactor);
        secondCursor->setDecimFactor(currentDecimFactor);
    }

    void setAmpScan(AMP_SCAN_t scan) // Установить развертку по амплитуде
    {
        currentAmpScan = scan;
        firstCursor->setAmpScan(scan);
        secondCursor->setAmpScan(scan);
    }

    void setVisible(bool show) // Включить/Выключить отображение курсора
    {
        visible = show;
        firstCursor->setVisible(visible);
        secondCursor->setVisible(visible);
        textLabel->setVisible(visible);
    }

    void update() // Обновить
    {
        currentAmpScan = sourceChannel->ampScan;
        int channel_color = ((sourceChannel->color)&0xFFFFFF00)>>8;
        textLabel->setTextColor(channel_color);
        firstCursor->setAmpScan(currentAmpScan);
        firstCursor->setCursorPointColor(channel_color);
        secondCursor->setAmpScan(currentAmpScan);
        secondCursor->setCursorPointColor(channel_color);
        firstCursor->setDecimFactor(currentDecimFactor);
        secondCursor->setDecimFactor(currentDecimFactor);
        textLabel->update();
    }

    void showCursor(bool visible) // Сбросить местоположение курсоров и показать их по центру
    {
        setVisible(visible);
        double rangeY = firstCursor->getYAxis()->range().upper +
                               firstCursor->getYAxis()->range().lower;

        firstCursor->setAxisCoords(plot->xAxis->range().center()/2,
                              firstCursor->getYAxis()->range().center()/2);
        firstCursor->setAxisCoords(plot->xAxis->range().center()/2, rangeY/2);

        secondCursor->setAxisCoords(plot->xAxis->range().center(),
                                secondCursor->getYAxis()->range().center()/2);
        secondCursor->setAxisCoords(plot->xAxis->range().center(), rangeY/4);

        textLabel->setVisible(visible);
    }

    void resetCursors() // Сбросить местоположение курсоров
    {
        showCursor(visible);
        plot->replot(QCustomPlot::rpHint);
    }

    void mousePress(QMouseEvent *event) // Вызывается в обработчике нажатия кнопки мыши
    {
        if(visible)
        {
            firstCursor->selectCursorLeftButton(event);
            secondCursor->selectCursorLeftButton(event);
        }
    }

    void mouseMove(QMouseEvent *event) // Вызывается в обработчике перемещения мыши по графику
    {
        if(visible)
        {
            firstCursor->selectCursor(event);
            firstCursor->moveCursor(event);
            if(firstCursor->isMove())
                textLabel->update();

            if(!firstCursor->isSelect())
            {
                secondCursor->selectCursor(event);
                secondCursor->moveCursor(event);
                if(secondCursor->isMove())
                    textLabel->update();
            }
        }
    }

    void mouseRelease(QMouseEvent *event) // Вызывается в обработчике при отпускании кнопки мыши
    {
        if(visible)
        {
            firstCursor->releaseButton(event);
            secondCursor->releaseButton(event);
        }
    }

    friend class SignalPlotForm;
    friend class SpectrPlotForm;
};

#endif // QCPCURSOR_H
