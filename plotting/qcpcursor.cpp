/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.4                                                  **
****************************************************************************/

#include "qcpcursor.h"

PlotCursor::PlotCursor(QCustomPlot *parent) : QCPAbstractItem(parent)
{
    plot = parent;
    yAxis = NULL;
    cursorPen.setStyle(CURSOR_LINE_STYLE);
    cursorPen.setWidthF(CURSOR_LINE_WIDTH);
    cursorPen.setColor(QColor((CURSOR_LINE_COLOR&0xFF000000)>>24,
                              (CURSOR_LINE_COLOR&0x00FF0000)>>16,
                              (CURSOR_LINE_COLOR&0x0000FF00)>>8,
                              (CURSOR_LINE_COLOR&0x000000FF)));

    hLine = new QCPItemLine(plot);
    hLine->setVisible(false);
    hLine->setPen(cursorPen);
    hLine->setAntialiased(false);
    vLine = new QCPItemLine(plot);
    vLine->setVisible(false);
    vLine->setPen(cursorPen);
    vLine->setAntialiased(false);
    setAntialiased(false);

    selectHLine = false;
    selectVLine = false;
    moveHLine = false;
    moveVLine = false;
    currentX = 0;
    currentY = 0;

    decim_factor = 2000;
    ampScan = AMP_SCAN_1_500;
    visible = true;
    cursorColor = 0x0000FF;
}

void PlotCursor::setYAxis(QCPAxis *a) {
    yAxis = a;
    hLine->start->setAxes(plot->xAxis, a);
    hLine->end->setAxes(plot->xAxis, a);

    vLine->start->setAxes(plot->xAxis, a);
    vLine->end->setAxes(plot->xAxis, a);
}

void PlotCursor::setCoords(double pixelX, double pixelY)
{
    double x = 0, y = 0;
    x = plot->xAxis->pixelToCoord(pixelX);
    y = yAxis->pixelToCoord(pixelY);
    vLine->start->setCoords(x, QCPRange::minRange);
    vLine->end->setCoords(x, QCPRange::maxRange);
    hLine->start->setCoords(QCPRange::minRange, y);
    hLine->end->setCoords(QCPRange::maxRange, y);
    currentX = x;
    currentY = y;
}

void PlotCursor::setAxisCoords(double X, double Y)
{
    vLine->start->setCoords(X, QCPRange::minRange);
    vLine->end->setCoords(X, QCPRange::maxRange);
    hLine->start->setCoords(QCPRange::minRange, Y);
    hLine->end->setCoords(QCPRange::maxRange, Y);
    currentX = X;
    currentY = Y;
}

void PlotCursor::setCoords(QPointF &c)
{
    setCoords(c.x(), c.y());
}

void PlotCursor::setVisible(bool _visible)
{
    visible = _visible;
    hLine->setVisible(visible);
    vLine->setVisible(visible);
    if(visible == false)
    {
        selectHLine = false;
        selectVLine = false;
    }
}

int PlotCursor::selectCursor(QMouseEvent *event)
{
    double X = plot->xAxis->pixelToCoord(event->pos().x());

    /* Если курсор мыши приблизился к вертикальной линии на
     * расстояние getTimeLim, то вертикальная линия
     * выбирается для перемещения. Стиль курсора мыши меняется
     * на SizeHorCursor и устанавливается флаг selectVLine
     * Тогда функция сразу возвращает 1
    */
    if(module(X - currentX) <= getTimeLim(decim_factor))
    {
        plot->setCursor(Qt::SizeHorCursor);
        selectVLine = true;
        selectHLine = !selectVLine;
        return 1;
    }
    else
    {
        selectVLine = false;
        plot->setCursor(Qt::ArrowCursor);
    }

    double Y = yAxis->pixelToCoord(event->pos().y());
    /* Если курсор мыши приблизился к горизонтальной линии на
     * расстояние getLim, то горизонтальная линия
     * выбирается для перемещения. Стиль курсора мыши меняется
     * на SizeVerCursor и устанавливается флаг selectHLine
     * Тогда функция сразу возвращает 1
    */
    if(module(Y - currentY) <= getLim(ampScan))
    {
        plot->setCursor(Qt::SizeVerCursor);
        selectHLine = true;
        selectVLine = !selectHLine;
        return 1;
    }
    else
    {
        plot->setCursor(Qt::ArrowCursor);
        selectHLine = false;
    }

    /* Если не выбрана ни одна линия курсора
     * то возвращается 0
    */
    return 0;
}

void PlotCursor::selectCursorLeftButton(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        if(selectHLine) // Линия курсоры выбрана, то устанавливается флаг move
        {
            moveHLine = true;
        }
        else // Тоже самое и с вертикальной линией
        {
            moveHLine = false;
            if(selectVLine)
            {
                moveVLine = true;
                // Перемещать можно только одну линию
                // поэтому другой флаг, сбрасывается
                moveHLine = !moveVLine;
            }
        }
    }
}

void PlotCursor::releaseButton(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) // Сброс всех флагов
    {
        selectHLine = false;
        selectVLine = false;
        moveHLine = false;
        moveVLine = false;
    }
}

void PlotCursor::moveCursor(QMouseEvent *event)
{
    double Y = yAxis->pixelToCoord(event->pos().y());       // Позиция мыши Y в координатах оси
    double X = plot->xAxis->pixelToCoord(event->pos().x()); // Позиция мыши X в координатах оси

    /*
     * Если moveHFlag выбран, то переместить горизонтальную линию
     * курсора в полученную позицию
    */
    if(moveHLine)
    {
        hLine->start->setCoords(0, Y);
        hLine->end->setCoords(plot->xAxis->range().maxRange, Y);
        currentY = Y;
        plot->replot(REPLOT_FLAG);
    }

    /*
     * Если moveVFlag выбран, то переместить вертикальную линию
     * курсора в полученную позицию
    */
    if(moveVLine)
    {
        vLine->start->setCoords(X, 0);
        vLine->end->setCoords(X, yAxis->range().maxRange);
        currentX = X;
        plot->replot(REPLOT_FLAG);
    }
}

void PlotCursor::draw(QCPPainter *painter)
{
    if(visible)
    {
        QPointF center(plot->xAxis->coordToPixel(currentX), yAxis->coordToPixel(currentY));
        int r = (cursorColor & 0xFF0000) >> 16 ;
        int g = (cursorColor & 0x00FF00) >> 8;
        int b = (cursorColor & 0x0000FF);
        QColor cursorPointColor(r, g, b, CURSOR_POINT_TRANSPARENT);
        QColor penColor(r, g, b);
        QPen pointPen(penColor);
        pointPen.setWidthF(CURSOR_POINT_PEN_WIDTH);
        painter->setBrush(cursorPointColor);
        painter->setPen(pointPen);
        // Нарисовать круг в точке куда указывает курсор
        painter->drawEllipse(center, CURSOR_POINT_RADIUS, CURSOR_POINT_RADIUS);
    }
}

double PlotCursor::selectTest(const QPointF &pos, bool onlySelectable, QVariant *details) const
{
    Q_UNUSED(pos)
    Q_UNUSED(onlySelectable)
    Q_UNUSED(details)
    return 0;
}

CursorTextLabel::CursorTextLabel(QCustomPlot *customPlot, PlotCursor *cursor1, PlotCursor *cursor2)
{
    plot = customPlot;
    firstCursor = cursor1;
    secondCursor = cursor2;
    textLabel = new QCPItemText(plot);
    textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignRight);
    QPen textPen;
    textPen.setColor(BASE_TIME_COLOR);
    textPen.setWidth(1);
    textLabel->setPen(textPen);
    textLabel->setBrush(QColor((CURSOR_TEXT_LABEL_COLOR & 0xFF000000)>>24,
                               (CURSOR_TEXT_LABEL_COLOR & 0x00FF0000)>>16,
                               (CURSOR_TEXT_LABEL_COLOR & 0x0000FF00)>>8,
                               (CURSOR_TEXT_LABEL_COLOR & 0x000000FF)));

    textLabel->setColor(Qt::red);
    textLabel->setAntialiased(false);
    visible = false;
    textLabel->setVisible(visible);
    yAxis = plot->yAxis;
    xAxis = plot->xAxis;
    plot->addLayer(TEXT_LABEL_LAYER_NAME, customPlot->layer(CURSORS_LAYER_NAME), QCustomPlot::limAbove);
    textLabel->setLayer(TEXT_LABEL_LAYER_NAME);
}

void CursorTextLabel::setVisible(bool _v)
{
    visible = _v;
    textLabel->setVisible(visible);
}

void CursorTextLabel::update()
{
    /* При переключении временной развертки
     * текстовая метка будет смещаться, поэтому ее
     * позицию надо обновлять. В этой функии тектовая метка
     * устанавливается в верхнем правом угле экрана
    */
    double upperX = plot->xAxis->range().upper;
    double upperY = yAxis->range().upper;

    double deltaX = module(firstCursor->getX() - secondCursor->getX());
    double deltaY = module(firstCursor->getY() - secondCursor->getY());

    textLabel->position->setCoords(upperX, upperY);
    textLabel->setTextAlignment(Qt::AlignLeft);
    textLabel->setText("  X1: " + QString::number(firstCursor->getX()) + "\n" +
                       "  Y1: " + QString::number(firstCursor->getY()) + "\n" +
                       "  X2: " + QString::number(secondCursor->getX()) + "\n" +
                       "  Y2: " + QString::number(secondCursor->getY()) + "\n" +
                       "  " + QChar(DELTA_CHAR) + "X: " + QString::number(deltaX) + "\n" +
                       "  " + QChar(DELTA_CHAR) + "Y: " + QString::number(deltaY) + "\n");
}


void CursorTextLabel::setTextColor(int color)
{
    textLabel->setColor(color);
}
