/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SIGNALPLOT_H
#define SIGNALPLOT_H

#include <QAction>

#include "lib/qcustomplot.h"
#include "include/osc_global.h"
#include "include/ui_config.h"

#define US_COEFF 1000000    // Коэффициент отображение временной оси в микросекунды
#define INTERP_COEFF 10     // Коэффициент интерполяции графика

#define LINE_WIDTH PLOT_ITEM_LINE_WIDTH
#define SIGNAL_BACKGROUND_COLOR PLOT_BACKGROUND_COLOR
#define TRIGGER_LINE_STYLE TRIGGER_LINE_QT_STYLE

#define DEFAULT_RANGE_AMP 20    // Оси Y по умолчанию

#define MIN_BYTE_NUMS 512       // Минимальное количество отсчетов необходимых для обработки

enum AXIS_RANGE {s = 1, ms = 1000, us = 1000000};

/*
 *  Структура, которая отображает триггер на графике
*/
struct TRIGGER_IMAGE
{
    TRIGGER_t *deviceTrigger;   // Указатель на TRIGGER_t
    QCPItemLine *triggerLine;   // Указатель на QCPItemLine
    bool selectTriggerFlag;     // Линия триггера выбрана ?
    bool moveTriggerFlag;       // Линия триггера перемещается ?
    bool triggered;             // Триггер сработал ?
    int triggerOffset;          // Смещение в буфере с которого начинать чтение отсчетов
};

/*
 * Структура, которая данные осциллограммы сигнала для одного канала
*/
struct OSC_ITEM
{
    MEASURE_CHANNEL_t* channelStruct;   // Указатель на MEASURE_CHANNEL_t
    QCPAxis *amplitudeAxis;             // Указатель на ось Y, соответствующую канала
    QAction *cursorSourceAction;        // Элемент меню, для выбора этого канала как исходного для курсоров
    QVector<double> *outSamples;        // Указатель на вектор интерполированных отсчетов, которые можно выводить на экран
    double *samplesIn;                  // Указатель на буфер отсчетов, которые только что считаны в девайса
};

#endif // SIGNALPLOT_H

