
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.4                                                  **
****************************************************************************/

#include "signalplotform.h"

SignalPlotForm::SignalPlotForm(QWidget *parent) :
    QWidget(parent)
{

    signalPlot = new QCustomPlot(this);
    layout.addWidget(signalPlot);
    setLayout(&layout);
    samplesFreq = 50000000;

    signalPlot->addLayer("cursors_layer", signalPlot->layer("main"), QCustomPlot::limBelow);

    signalPlot->setBackground(QColor((PLOT_BACKGROUND_COLOR&0xFF000000)>>24,
                                     (PLOT_BACKGROUND_COLOR&0x00FF0000)>>16,
                                     (PLOT_BACKGROUND_COLOR&0x0000FF00)>>8,
                                     (PLOT_BACKGROUND_COLOR&0x000000FF)));

    signalPlot->xAxis->setLabel(tr("Time (us)"));
    signalPlot->xAxis->setLabelColor(QColor((X_AXIS_LABEL_COLOR&0xFF000000)>>24,
                                            (X_AXIS_LABEL_COLOR&0x00FF0000)>>16,
                                            (X_AXIS_LABEL_COLOR&0x0000FF00)>>8,
                                            (X_AXIS_LABEL_COLOR&0x000000FF)));

    signalPlot->xAxis->setTickLabelColor(QColor((X_AXIS_TICK_COLOR&0xFF000000)>>24,
                                                (X_AXIS_TICK_COLOR&0x00FF0000)>>16,
                                                (X_AXIS_TICK_COLOR&0x0000FF00)>>8,
                                                (X_AXIS_TICK_COLOR&0x000000FF)));

    signalPlot->xAxis->grid()->setVisible(true);
    signalPlot->yAxis->grid()->setVisible(false);
    signalPlot->yAxis->setVisible(false);

    xAxisPen.setColor(QColor((BASE_TIME_COLOR&0xFF000000)>>24,
                             (BASE_TIME_COLOR&0x00FF0000)>>16,
                             (BASE_TIME_COLOR&0x0000FF00)>>8,
                             (BASE_TIME_COLOR&0x000000FF)));

    xAxisPen.setWidthF(LINE_WIDTH);

    signalPlot->xAxis->setBasePen(xAxisPen);
    signalPlot->xAxis->setAntialiased(false);

    connect(signalPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress(QMouseEvent*)));
    connect(signalPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMove(QMouseEvent*)));
    connect(signalPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseRelease(QMouseEvent*)));
    setInteraction(ARROW_INTERACTION_MODE);

    arrowAction = graphModeMenu.addAction(tr("Arrow mode"));
    arrowAction->setCheckable(true);
    arrowAction->setChecked(true);
    triggerAction = graphModeMenu.addAction(tr("Move triggers lines"));
    triggerAction->setCheckable(true);
    triggerAction->setChecked(false);
    showCursorAction = graphModeMenu.addAction(tr("Show cursor"));
    showCursorAction->setCheckable(true);
    showCursorAction->setChecked(false);
    selectCursorSourceMenu.setTitle(tr("Cursor source"));
    graphModeMenu.addMenu(&selectCursorSourceMenu);
    graphModeMenu.addSeparator();
    resetCursorAction = graphModeMenu.addAction(tr("Reset cursors"));
    resetTriggerAction = graphModeMenu.addAction(tr("Reset trigger"));

    connect(arrowAction, SIGNAL(triggered(bool)), this, SLOT(setArrowModeInteractions(bool)));
    connect(triggerAction, SIGNAL(triggered(bool)), this, SLOT(setTriggerModeInteractions(bool)));
    connect(resetCursorAction, SIGNAL(triggered()), this, SLOT(resetCursor()));
    connect(resetTriggerAction, SIGNAL(triggered()), this, SLOT(resetTrigger()));
    connect(showCursorAction, SIGNAL(toggled(bool)), this, SLOT(showCursor(bool)));
    connect(&selectCursorSourceMenu, SIGNAL(triggered(QAction*)), this, SLOT(selectCursorSourceSlot(QAction*)));

    QPen triggerPen;
    triggerPen.setColor(QColor((TRIGGER_LINE_COLOR & 0xFF000000)>>24,
                               (TRIGGER_LINE_COLOR & 0x00FF0000)>>16,
                               (TRIGGER_LINE_COLOR & 0x0000FF00)>>8,
                               (TRIGGER_LINE_COLOR & 0x000000FF)));

    triggerPen.setWidthF(TRIGGER_LINE_WIDTH);
    triggerPen.setStyle(TRIGGER_LINE_STYLE);
    trigger.triggerOffset = 0;
    trigger.triggered = false;
    trigger.moveTriggerFlag = false;
    trigger.selectTriggerFlag = false;
    trigger.triggerLine = new QCPItemLine(signalPlot);
    trigger.triggerLine->setPen(triggerPen);
    trigger.triggerLine->start->setAxes(signalPlot->xAxis, signalPlot->yAxis);
    trigger.triggerLine->end->setAxes(signalPlot->xAxis, signalPlot->yAxis);
    trigger.triggerLine->start->setCoords(0, 0);
    trigger.triggerLine->end->setCoords(signalPlot->xAxis->range().maxRange, 0);
    trigger.triggerLine->setVisible(false);

    plotCursors = new CursorsPair<SignalPlotCursor>(signalPlot);
    showCursorAction->setEnabled(false);

    notDeviceWarningText = new QCPItemText(signalPlot);
    notDeviceWarningText->setText(tr("Device not connected !"));
    QFont warningFont("Warning", WARNING_LABEL_TEXT_SIZE, QFont::Bold);
    notDeviceWarningText->setFont(warningFont);
    notDeviceWarningText->setColor(QColor((WARNING_COLOR_TEXT & 0xFF000000)>> 24,
                                          (WARNING_COLOR_TEXT & 0x00FF0000)>> 16,
                                          (WARNING_COLOR_TEXT & 0x0000FF00)>> 8,
                                           WARNING_COLOR_TEXT & 0x000000FF));
    QPen warningPen;
    warningPen.setColor(QColor((WARNING_COLOR_RECT & 0xFF000000)>> 24,
                               (WARNING_COLOR_RECT & 0x00FF0000)>> 16,
                               (WARNING_COLOR_RECT & 0x0000FF00)>> 8,
                                WARNING_COLOR_TEXT & 0x000000FF));
    warningPen.setStyle(Qt::DashLine);

    warningPen.setWidthF(WARNING_RECT_LINE_WIDTH);
    notDeviceWarningText->setPen(warningPen);
    notDeviceWarningText->setVisible(false);
    notDeviceWarningText->setPositionAlignment(Qt::AlignCenter | Qt::AlignTop);
    signalPlot->addItem(notDeviceWarningText);
    notDeviceWarningText->position->setType(QCPItemPosition::ptAxisRectRatio);
    notDeviceWarningText->position->setCoords(0.5, 0.5);

    connect(&signalThread, SIGNAL(finished()), this, SLOT(threadFinish()));
}

SignalPlotForm::~SignalPlotForm()
{
    signalThread.wait(5000);
    for(int i(0); i < itemLst.size(); i++)
    {
        delete itemLst.at(i).outSamples;
        delete itemLst.at(i).samplesIn;
    }

    qDebug() << "destroy class SignalPlotForm ";
}

void SignalPlotForm::setInteraction(GRAPH_INTERACTION_MODE mode)
{
    interactionMode = mode;
    switch(interactionMode)
    {
    case ARROW_INTERACTION_MODE:
        signalPlot->setCursor(Qt::ArrowCursor);
        break;
    case TRIGGER_MOVE_INTERACTION_MODE:
        signalPlot->setCursor(Qt::ArrowCursor);
        break;
    }
}

void SignalPlotForm::arrowModeInteraction(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        QCPAxis *channelXAxis = signalPlot->xAxis;

        QString messageToolTip = tr("Time: ") +
                getTime(channelXAxis->pixelToCoord(event->x())/US_COEFF) + "\n";

        for(int i(0); i < itemLst.size(); i++)
        {
            MEASURE_CHANNEL_t *ch = itemLst.at(i).channelStruct;
            if(ch->enabled)
            {
                QCPAxis *channelYAxis = itemLst.at(i).amplitudeAxis;
                messageToolTip +=
                    tr("Channel ") + QString(ch->id) + ": "  +
                    QString::number(ceil(channelYAxis->pixelToCoord(event->pos().y()), ch->ampScan)) +
                    tr(" V") + " \n";
            }
        }

        QToolTip::showText(event->globalPos(), messageToolTip);
    }
}

double SignalPlotForm::getMovePoint(AMP_SCAN_t ampScan)
{
    double ret = 1.0f;

    switch(ampScan)
    {
    case AMP_SCAN_1_500:
        ret = 1.0f;
        break;
    case AMP_SCAN_1_200:
        ret = 0.5f;
        break;
    case AMP_SCAN_1_100:
        ret = 0.2f;
        break;
    case AMP_SCAN_1_50:
        ret = 0.1f;
        break;
    case AMP_SCAN_1_20:
        ret = 0.05f;
        break;
    case AMP_SCAN_1_10:
        ret = 0.02f;
        break;
    case AMP_SCAN_1_5:
        ret = 0.01f;
        break;
    case AMP_SCAN_1_2:
        ret = 0.005f;
        break;
    case AMP_SCAN_1_1:
        ret = 0.002f;
        break;
    }

    return ret;
}

void SignalPlotForm::moveTriggerInteraction(QMouseEvent *event)
{
    QCPAxis *amplitudeAxis = itemLst.at(trigger.deviceTrigger->ch).amplitudeAxis;
    int channelNum =trigger.deviceTrigger->ch;
    double currentY = event->pos().y();
    if(itemLst.at(channelNum).channelStruct->enabled && trigger.deviceTrigger->en)
    {
        if(abs(amplitudeAxis->pixelToCoord(currentY) - trigger.triggerLine->start->coords().y()) <=
                    getMovePoint(itemLst.at(channelNum).channelStruct->ampScan))
        {
            signalPlot->setCursor(Qt::SizeVerCursor);
            trigger.selectTriggerFlag = true;
        }
    }
}

void SignalPlotForm::cursorsModeInteraction(QMouseEvent *event)
{
    plotCursors->mousePress(event);
}

void SignalPlotForm::mousePress(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        switch(interactionMode)
        {
        case ARROW_INTERACTION_MODE: {

            if(plotCursors->isVisible())
                plotCursors->mousePress(event);
            if(!plotCursors->firstCursor->isSelect() && !plotCursors->secondCursor->isSelect())
                arrowModeInteraction(event);
        }
            break;
        case TRIGGER_MOVE_INTERACTION_MODE:
        {
            moveTriggerInteraction(event);
            if(plotCursors->isVisible())
            {
                if(!trigger.selectTriggerFlag)
                    plotCursors->mousePress(event);
            }
        }
            break;
        }
    }

    if(event->button() == Qt::RightButton)
    {
        graphModeMenu.popup(event->globalPos());
    }
}

void SignalPlotForm::mouseMove(QMouseEvent *event)
{

    if(plotCursors->isVisible() && interactionMode != TRIGGER_MOVE_INTERACTION_MODE)
    {
        plotCursors->mouseMove(event);
    }

    if(interactionMode == TRIGGER_MOVE_INTERACTION_MODE)
    {
        QCPAxis *amplitudeAxis = itemLst.at(trigger.deviceTrigger->ch).amplitudeAxis;
        int channelNum = trigger.deviceTrigger->ch;
        double currentY = event->pos().y();
        if(abs(amplitudeAxis->pixelToCoord(currentY) - trigger.triggerLine->start->coords().y()) <=
            getMovePoint(itemLst.at(channelNum).channelStruct->ampScan) && itemLst.at(channelNum).channelStruct->enabled)
        {
            signalPlot->setCursor(Qt::SizeVerCursor);
        }
        else
        {
            if(!plotCursors->firstCursor->isSelect() && !plotCursors->secondCursor->isSelect())
                signalPlot->setCursor(Qt::ArrowCursor);
        }

        if(trigger.selectTriggerFlag == true)
        {
            double currentY = event->pos().y();
            trigger.moveTriggerFlag = true;
            trigger.triggerLine->start->setCoords(0, amplitudeAxis->pixelToCoord(currentY));
            trigger.triggerLine->end->setCoords(signalPlot->xAxis->range().maxRange,
                                                amplitudeAxis->pixelToCoord(currentY));
            emit changeTriggerLevel(amplitudeAxis->pixelToCoord(currentY));
            signalPlot->replot(REPLOT_FLAG);
        }
    }
}

void SignalPlotForm::mouseRelease(QMouseEvent *event)
{
    if(interactionMode == TRIGGER_MOVE_INTERACTION_MODE)
    {
        if(trigger.selectTriggerFlag == true)
        {
            trigger.selectTriggerFlag = false;
            trigger.moveTriggerFlag = false;
            double newTriggerLevel = trigger.triggerLine->start->coords().y();
            trigger.deviceTrigger->level = newTriggerLevel;
            trigger.triggerLine->start->setCoords(0, newTriggerLevel);
            trigger.triggerLine->end->setCoords(signalPlot->xAxis->range().maxRange,
                                                newTriggerLevel);
            signalPlot->setCursor(Qt::ArrowCursor);
            signalPlot->replot(REPLOT_FLAG);
            emit changeTriggerLevel(newTriggerLevel);
        }
    }

    if(plotCursors->isVisible())
    {
        plotCursors->mouseRelease(event);
    }
}

void SignalPlotForm::addChannel(MEASURE_CHANNEL_t *ch)
{
    OSC_ITEM newItem;

    int color_int;
    QCPAxis *axis;
    QCPGraph *g;
    QPen pen;

    pen.setWidthF(LINE_WIDTH);

    color_int = (ch->color&0xFFFFFF00)>>8;
    pen.setColor(QColor((color_int>>16)&0xFF, (color_int>>8)&0xFF, color_int&0xFF));

    /* Ось Y первого графика ставится с левой стороны,
     * следующего с правой стороны, третьего опять с левой, и.т.п
    */
    if((signalPlot->graphCount())%2)
        axis = signalPlot->axisRects().at(0)->addAxis(QCPAxis::atRight);
    else
        axis = signalPlot->axisRects().at(0)->addAxis(QCPAxis::atLeft);

    axis->setVisible(false);
    axis->setAntialiased(false);
    setupGrid(signalPlot->xAxis, axis, grid);
    axis->grid()->setVisible(true);
    g = signalPlot->addGraph();
    g->setAdaptiveSampling(false);
    g->rescaleAxes(true);

    axis->setAutoTickStep(false);
    axis->setTickStep(5.0f);
    axis->setRange(-DEFAULT_RANGE_AMP, DEFAULT_RANGE_AMP);
    newItem.amplitudeAxis = axis;

    axis->setBasePen(pen);
    axis->setTickLabelColor(QColor((color_int & 0xFF0000)>>16,
                                   (color_int & 0x00FF00)>>8,
                                   (color_int & 0x0000FF)));
    axis->setTickPen(pen);
    axis->setSubTickPen(pen);
    axis->setAntialiased(false);

    g->setAntialiased(false);
    g->setAntialiasedFill(false);
    g->setPen(pen);
    g->setValueAxis(axis);
    g->setKeyAxis(signalPlot->xAxis);
    g->rescaleKeyAxis(true);
    g->setName(tr("Channel ") + QString(ch->id));
    newItem.channelStruct = ch;
    newItem.cursorSourceAction = selectCursorSourceMenu.addAction(tr("Channel ") + QString(ch->id));
    newItem.cursorSourceAction->setCheckable(true);

    // Создание буферов для входных отсчетов и для отсчетов прошедшие интерполяцию
    newItem.outSamples = new QVector<double>;
    newItem.samplesIn = new double[MAX_CHANNEL_BUFFER_SIZE];

    // Самый первый канал ставится как основной
    if(ch->channelNum == 0)
    {
        newItem.cursorSourceAction->setChecked(true);
        plotCursors->setSourceChannel(ch);
        plotCursors->setYAxis(axis);
    }

    itemLst.append(newItem);
}

void SignalPlotForm::setupGrid(QCPAxis *baseAxisX, QCPAxis *baseAxisY, DiagramGrid &initGrid)
{
    baseAxisX->grid()->setSubGridVisible(true);
    baseAxisX->grid()->setPen(initGrid.gridPen);
    baseAxisX->grid()->setSubGridPen(initGrid.gridPen);
    baseAxisX->grid()->setZeroLinePen(initGrid.zeroLineGridPen);

    baseAxisY->grid()->setSubGridVisible(false);
    baseAxisY->grid()->setPen(initGrid.gridPen);
    baseAxisY->grid()->setSubGridPen(initGrid.subGridPen);
    baseAxisY->grid()->setZeroLinePen(initGrid.zeroLineGridPen);

    baseAxisX->grid()->setAntialiased(false);
    baseAxisY->grid()->setAntialiased(false);
}

void SignalPlotForm::setLevelTrigger(double level)
{
    QCPAxis *triggerAxis = itemLst.at(trigger.deviceTrigger->ch).amplitudeAxis;
    QCPItemLine *triggerLine = trigger.triggerLine;
    triggerLine->start->setAxes(signalPlot->xAxis, triggerAxis);
    triggerLine->end->setAxes(signalPlot->xAxis, triggerAxis);
    triggerLine->start->setCoords(0, level);
    triggerLine->end->setCoords(signalPlot->xAxis->range().maxRange, level);
}

void SignalPlotForm::setMaxAmpDiv(MEASURE_CHANNEL_t* channel)
{
    double maxAmp = MAX_AMP_DIV_1_500;
    switch(channel->ampScan)
    {
    case AMP_SCAN_1_500:
        maxAmp = MAX_AMP_DIV_1_500;
        break;
    case AMP_SCAN_1_200:
        maxAmp = MAX_AMP_DIV_1_200;
        break;
    case AMP_SCAN_1_100:
        maxAmp = MAX_AMP_DIV_1_100;
        break;
    case AMP_SCAN_1_50:
        maxAmp = MAX_AMP_DIV_1_50;
        break;
    case AMP_SCAN_1_20:
        maxAmp = MAX_AMP_DIV_1_20;
        break;
    case AMP_SCAN_1_10:
        maxAmp = MAX_AMP_DIV_1_10;
        break;
    case AMP_SCAN_1_5:
        maxAmp = MAX_AMP_DIV_1_5;
        break;
    case AMP_SCAN_1_2:
        maxAmp = MAX_AMP_DIV_1_2;
        break;
    case AMP_SCAN_1_1:
        maxAmp = MAX_AMP_DIV_1_1;
        break;
    }

    if(channel->channelNum < signalPlot->graphCount())
    {
        itemLst.at(channel->channelNum).amplitudeAxis->setTickStep(maxAmp/4);
        itemLst.at(channel->channelNum).amplitudeAxis->setRange(-maxAmp, maxAmp);
    }
    else
    {
        qDebug() << "Channel: " << channel->channelNum << " " <<
                    "not found in plot class";
    }
}

void SignalPlotForm::threadFinish() // Завершение интерполяции
{
    // Вывести данные на графики
    for(int i(0); i < itemLst.size(); i++)
    {
        signalPlot->graph(i)->clearData();
        signalPlot->graph(i)->setData(signalThread.getScopePoints(), *(itemLst.at(i).outSamples));
    }

    signalPlot->xAxis->rescale();
    trigger.triggered = false;

    // Обновить ось Y к которой привязана линия триггера
    QCPAxis *triggerAxis = itemLst.at(trigger.deviceTrigger->ch).amplitudeAxis;
    if(itemLst.at(trigger.deviceTrigger->ch).channelStruct->enabled)
    {
        trigger.triggerLine->setVisible(trigger.deviceTrigger->en);
        trigger.triggerLine->start->setAxes(signalPlot->xAxis, triggerAxis);
        trigger.triggerLine->end->setAxes(signalPlot->xAxis, triggerAxis);
    }
    else
    {
        trigger.triggerLine->setVisible(false);
    }

    double interpTimeStep = signalThread.getInterpTimeStep(); // Шаг интерполяции
    signalPlot->xAxis->setAutoTickStep(false);
    signalPlot->xAxis->setSubTickCount(0);
    signalPlot->xAxis->setAutoSubTicks(false);
    signalPlot->xAxis->setTickStep(interpTimeStep*INTERP_COEFF*(SCOPE_SIZE/10));
    signalPlot->xAxis->setRange(0, interpTimeStep*SCOPE_SIZE*INTERP_COEFF);

    bool allChannelsDiasables = true;
    for(int i(0); i < itemLst.size(); i++)
    {
        int n = itemLst.at(i).channelStruct->channelNum;
        if(itemLst.at(i).channelStruct->enabled)
        {
            allChannelsDiasables = false;
            if(itemLst.at(i).amplitudeAxis == plotCursors->getYAxis())
                showCursorAction->setEnabled(true);
        }
        else
        {
            if(itemLst.at(i).amplitudeAxis == plotCursors->getYAxis())
            {
                showCursorAction->setEnabled(false);
                showCursorAction->setChecked(false);
                plotCursors->setVisible(false);
            }

            signalPlot->graph(n)->clearData();
            if(itemLst.at(i).channelStruct->channelNum == trigger.deviceTrigger->ch)
            {
                showCursorAction->setChecked(false);
                showCursorAction->setEnabled(false);
            }
        }

        if(itemLst.at(i).amplitudeAxis == plotCursors->getYAxis())
        {
            plotCursors->setAmpScan(itemLst.at(i).channelStruct->ampScan);
        }
    }

    if(allChannelsDiasables) // Если все каналы отключены, то все равно показать Y первого канала
        itemLst.at(0).amplitudeAxis->setVisible(true);

    signalPlot->replot(REPLOT_FLAG); // Перерисовать график
    signalThread.resetBusy();
}

void SignalPlotForm::update(int decim_factor)
{
    if(itemLst.at(0).channelStruct->sampleBufferSize < MIN_BYTE_NUMS) return;
    if(signalThread.isRunning()) return;
    if(signalThread.isBusy()) return;
    if(!isEnabled()) return;

    plotCursors->setDecimFactor(decim_factor);
    plotCursors->update();

    bool allChannelsDisables = true;
    int bufSize = itemLst.at(0).channelStruct->sampleBufferSize;

    for(int i(0); i < itemLst.size(); i++)
    {
        int n = itemLst.at(i).channelStruct->channelNum;
        setMaxAmpDiv(itemLst.at(i).channelStruct);
        if(itemLst.at(i).channelStruct->enabled)
        {
            allChannelsDisables = false;
            signalPlot->graph(n)->setVisible(true);
            itemLst.at(i).amplitudeAxis->setVisible(true);
            for(int j(0); j < bufSize; j++)
                itemLst.at(i).samplesIn[j] = itemLst.at(i).channelStruct->samplePtr[j];
        }
        else
        {
            signalPlot->graph(i)->setVisible(false);
            itemLst.at(i).amplitudeAxis->setVisible(false);
        }
    }

    signalThread.setChannels(&itemLst);
    signalThread.setDecimFactor(decim_factor);
    signalThread.setTrigger(&trigger);
    signalThread.setBusy();
    signalThread.start();

    if(allChannelsDisables)
        itemLst.at(0).amplitudeAxis->setVisible(true);
}

void SignalPlotForm::clearGraphics()
{
    for(int i(0); i < signalPlot->graphCount(); i++)
    {
        signalPlot->graph(i)->clearData();
    }
}

void SignalPlotForm::saveFileSlot()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList filters;
    filters << JPG_FORMAT_FILTER
        << BMP_FORMAT_FILTER;
    dialog.setNameFilters(filters);
    QStringList files;
    if(dialog.exec())
    {
        files = dialog.selectedFiles();
        qDebug() << "Save to file: " << files.at(0);
        if(dialog.selectedNameFilter().compare(QString(JPG_FORMAT_FILTER)))
                saveFile(files.at(0), JPG_FORMAT);
        else
        {
            if(dialog.selectedNameFilter().compare(QString(BMP_FORMAT_FILTER)))
                    saveFile(files.at(0), BMP_FORMAT);
        }
    }
}

bool SignalPlotForm::saveFile(const QString &fileName, FILE_FORMAT type)
{
    bool ret = false;

    switch(type)
    {
    case PNG_FORMAT:
        ret = signalPlot->savePng(fileName);
        break;
    case PDF_FORMAT:
        ret = signalPlot->savePdf(fileName, true);
        break;
    case JPG_FORMAT:
        ret = signalPlot->saveJpg(fileName);
        break;
    case BMP_FORMAT:
        ret = signalPlot->saveBmp(fileName);
        break;
    }

    return ret;
}

void SignalPlotForm::setArrowModeInteractions(bool en)
{
    if(en)
    {
        setInteraction(ARROW_INTERACTION_MODE);
        triggerAction->setChecked(false);
    }
    else
    {
        graphModeMenu.actions().at(0)->setChecked(true);
    }
}

void SignalPlotForm::setTriggerModeInteractions(bool en)
{
    trigger.moveTriggerFlag = false;
    trigger.selectTriggerFlag = false;
    if(en)
    {
        setInteraction(TRIGGER_MOVE_INTERACTION_MODE);
        arrowAction->setChecked(false);
    }
    else
    {
        graphModeMenu.actions().at(0)->setChecked(true);
        setInteraction(ARROW_INTERACTION_MODE);
    }
}

void SignalPlotForm::setCursorModeInteractions(bool en)
{
    if(en)
    {
        arrowAction->setChecked(false);
        triggerAction->setChecked(false);
    }
    else
    {
        graphModeMenu.actions().at(0)->setChecked(true);
        setInteraction(ARROW_INTERACTION_MODE);
    }
}

void SignalPlotForm::resetCursor()
{
    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
            selectCursorSourceSlot(itemLst.at(i).cursorSourceAction);
    }

    plotCursors->resetCursors();
}

void SignalPlotForm::resetTrigger()
{
    for(int i(0); i < itemLst.size(); i++)
    {
        trigger.deviceTrigger->level = 0;
        emit changeTriggerLevel(0);
        setLevelTrigger(0);
    }
}

void SignalPlotForm::showCursor(bool visible)
{
    if(visible == false)
    {
        plotCursors->setVisible(false);
        signalPlot->setCursor(Qt::ArrowCursor);
    }

    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
            selectCursorSourceSlot(itemLst.at(i).cursorSourceAction);
    }

    plotCursors->showCursor(visible);
}

void SignalPlotForm::selectCursorSourceSlot(QAction *action)
{
    bool allChannelUncheck = false;
    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
            allChannelUncheck = true;

        if(itemLst.at(i).cursorSourceAction == action)
        {
            QCPAxis *axis= itemLst.at(i).amplitudeAxis;
            plotCursors->setYAxis(axis);
            plotCursors->setAmpScan(itemLst.at(i).channelStruct->ampScan);
            plotCursors->setSourceChannel(itemLst.at(i).channelStruct);
            plotCursors->resetCursors();
            if(!itemLst.at(i).channelStruct->enabled)
            {
                showCursorAction->setChecked(false);
                showCursorAction->setEnabled(false);
            }
        }
        else
        {
            itemLst.at(i).cursorSourceAction->setChecked(false);
        }
    }

    if(allChannelUncheck == false)
    {
        itemLst.at(0).cursorSourceAction->setChecked(true);
        selectCursorSourceMenu.actions().at(0)->setChecked(true);

        QCPAxis *axis = itemLst.at(0).amplitudeAxis;
        plotCursors->setYAxis(axis);
        plotCursors->setAmpScan(itemLst.at(0).channelStruct->ampScan);
        plotCursors->setSourceChannel(itemLst.at(0).channelStruct);
        plotCursors->resetCursors();
        if(!itemLst.at(0).channelStruct->enabled)
        {
            showCursorAction->setChecked(false);
            showCursorAction->setEnabled(false);
        }
    }
}

void SignalPlotForm::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        arrowAction->setText(tr("Arrow mode"));
        triggerAction->setText(tr("Move triggers lines"));
        showCursorAction->setText(tr("Show cursor"));
        selectCursorSourceMenu.setTitle(tr("Cursor source"));
        resetCursorAction->setText(tr("Reset cursors"));
        resetTriggerAction->setText(tr("Reset trigger"));
        notDeviceWarningText->setText(tr("Device not connected !"));
        signalPlot->xAxis->setLabel(tr("Time (us)"));

        for(int i(0); i < itemLst.size(); i++)
        {
            QString id = QString(itemLst.at(i).channelStruct->id);
            itemLst.at(i).cursorSourceAction->setText(tr("Channel ") + id);
        }
    }
    else
        QWidget::changeEvent(event);
}

void SignalPlotForm::loadSettings(QSettings *settingsClass)
{
    settingsClass->beginGroup("OSC");

    if(settingsClass->value("CURSOR_EN").toBool())
    {
        int srcChannel = settingsClass->value("CURSOR_SRC_CHANNEL_NUM").toDouble();
        if(itemLst.at(srcChannel).channelStruct->enabled)
        {
            plotCursors->setVisible(true);
            showCursorAction->setChecked(true);
            itemLst.at(srcChannel).cursorSourceAction->setChecked(true);
            selectCursorSourceSlot(itemLst.at(srcChannel).cursorSourceAction);
            double firstX = settingsClass->value("CURSOR_FIRST_X").toDouble();
            double firstY = settingsClass->value("CURSOR_FIRST_Y").toDouble();
            double secondX = settingsClass->value("CURSOR_SECOND_X").toDouble();
            double secondY = settingsClass->value("CURSOR_SECOND_Y").toDouble();
            plotCursors->firstCursor->setAxisCoords(firstX, firstY);
            plotCursors->secondCursor->setAxisCoords(secondX, secondY);
        }
        else
        {
            plotCursors->setVisible(false);
            showCursorAction->setChecked(false);
        }
    }

    settingsClass->endGroup();
}

void SignalPlotForm::saveSettings(QSettings *settingsClass)
{
    int cursorSrcChannel = 0;
    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
        {
            cursorSrcChannel = i;
            break;
        }
    }

    settingsClass->beginGroup("OSC");
    settingsClass->setValue("CURSOR_EN", plotCursors->isVisible());
    settingsClass->setValue("CURSOR_SRC_CHANNEL_NUM", cursorSrcChannel);
    settingsClass->setValue("CURSOR_FIRST_X", plotCursors->firstCursor->getX());
    settingsClass->setValue("CURSOR_FIRST_Y", plotCursors->firstCursor->getY());
    settingsClass->setValue("CURSOR_SECOND_X", plotCursors->secondCursor->getX());
    settingsClass->setValue("CURSOR_SECOND_Y", plotCursors->secondCursor->getY());
    settingsClass->endGroup();
}

void SignalPlotForm::setEnabled(bool en)
{
    bool allChannelDisable = true;
    QWidget::setEnabled(en);
    trigger.triggerLine->setVisible(en);

    for(int i(0); i < itemLst.size(); i++)
    {
        signalPlot->graph(i)->setVisible(en);
        itemLst.at(i).amplitudeAxis->grid()->setVisible(en);
        if(itemLst.at(i).channelStruct->enabled)
        {
            allChannelDisable = false;
            break;
        }
    }

    if(allChannelDisable)
        itemLst.at(0).amplitudeAxis->setVisible(true);

    notDeviceWarningText->setText(tr("Device not connected !"));
    notDeviceWarningText->setVisible(!en);
    signalPlot->xAxis->grid()->setVisible(en);
    itemLst.at(0).amplitudeAxis->grid()->setVisible(en);
    if(!en)
    {
        plotCursors->setVisible(false);
        signalPlot->replot();
    }
}

void SignalPlotForm::visibleCursors(bool show)
{
    showCursorAction->setChecked(show);
}

bool SignalPlotForm::isCursorVisible()
{
    return plotCursors->isVisible();
}
