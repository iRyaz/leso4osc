#include "signalplotcursor.h"

SignalPlotCursor::SignalPlotCursor(QCustomPlot *plot) : PlotCursor(plot)
{

}

double SignalPlotCursor::getLim(AMP_SCAN_t ampScan)
{
    double ret = 0;
    switch(ampScan)
    {
    case AMP_SCAN_1_500:
        ret = 1;
        break;
    case AMP_SCAN_1_200:
        ret = 0.5;
        break;
    case AMP_SCAN_1_100:
        ret = 0.2;
        break;
    case AMP_SCAN_1_50:
        ret = 0.1;
        break;
    case AMP_SCAN_1_20:
        ret = 0.05;
        break;
    case AMP_SCAN_1_10:
        ret = 0.02;
        break;
    case AMP_SCAN_1_5:
        ret = 0.01;
        break;
    case AMP_SCAN_1_2:
        ret = 0.005;
        break;
    case AMP_SCAN_1_1:
        ret = 0.001;
        break;
    }

    return ret;
}

double SignalPlotCursor::getTimeLim(int d)
{
    double ret = 10;

    switch(d)
    {
    case 2000:
        ret = 50;
        break;
    case 1000:
        ret = 20;
        break;
    case 500:
        ret = 10;
        break;
    case 200:
        ret = 5;
        break;
    case 100:
        ret = 2;
        break;
    case 50:
        ret = 1;
        break;
    case 20:
        ret = 0.5;
        break;
    case 10:
        ret = 0.2;
        break;
    case 5:
        ret = 0.1;
        break;
    case 2:
        ret = 0.05;
        break;
    case 1:
        ret = 0.01;
        break;
    }

    return ret;
}
