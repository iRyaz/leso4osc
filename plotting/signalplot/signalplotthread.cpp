#include "signalplotthread.h"

SignalPlotThread::SignalPlotThread()
{
    channelLstPtr = NULL;
    decimFactor = 1;

    timeStep = US_COEFF/(BASE_FREQUENCY/decimFactor);
    interpTimeStep = timeStep/INTERP_COEFF;
    busyFlag = false;
}

SignalPlotThread::~SignalPlotThread()
{

}

void SignalPlotThread::setDecimFactor(int decim_factor)
{
    if(decim_factor <= 0)
    {
        qDebug() << "Decimation factor <= 0 !!!!!!!";
        return;
    }

    decimFactor = decim_factor;
    timeStep = US_COEFF/(BASE_FREQUENCY/decimFactor);
    interpTimeStep = timeStep/INTERP_COEFF;
}

void SignalPlotThread::run()
{
    if(channelLstPtr == NULL)
    {
        qDebug() << "Class SignalPlotThread - not init channels lst";
        return;
    }

    // Очистить векторов
    timePoints.clear();
    samplesY.clear();
    scopePoints.clear();

    int bufSize = channelLstPtr->at(0).channelStruct->sampleBufferSize;

    for(int i(0); i < channelLstPtr->size(); i++)
        channelLstPtr->at(i).outSamples->clear();

    // Инициализировать вектор значениями
    for(int i(0); i < (bufSize - BUFFER_OFFSET); i++)
        timePoints.push_back(i*timeStep);

    // Инициализировать вектор интерполированными значениями
    for(int i(0); i < SCOPE_SIZE*INTERP_COEFF; i++)
        scopePoints.append(i*interpTimeStep);

    // Копирование значений в вектор sampesY
    for(int j(BUFFER_OFFSET); j < bufSize; j++)
        samplesY.push_back(channelLstPtr->at(trigger->deviceTrigger->ch).samplesIn[j]);

    // Исходный канал для триггера (канал синхронизации)
    MEASURE_CHANNEL_t *triggeredChannel = channelLstPtr->at(trigger->deviceTrigger->ch).channelStruct;

    // Передача точек классу сплайну
    splineClass.set_points(timePoints, samplesY);

    // Указатель на вектор отсчетов канала синхронизации
    QVector<double> *ampY = channelLstPtr->at(trigger->deviceTrigger->ch).outSamples;

    // Интерполяция отсчетов для канала синхронизации
    addChannelData(triggeredChannel, interpTimeStep, splineClass, ampY);

    // Вычисление интерполяции отсчетов для остальных каналов
    for(int i(0); i < channelLstPtr->size(); i++)
    {
        samplesY.clear();
        for(int j(BUFFER_OFFSET); j < bufSize; j++)
            samplesY.push_back(channelLstPtr->at(i).samplesIn[j]);
        splineClass.set_points(timePoints, samplesY);
        addChannelData(channelLstPtr->at(i).channelStruct,
                       interpTimeStep, splineClass, channelLstPtr->at(i).outSamples);
    }
}

void SignalPlotThread::addChannelData(MEASURE_CHANNEL_t *channel, double interpStep,
                                      tk::spline &splineClass, QVector<double> *y)
{
    // Провести интерполяцию отсчетов (вычислить промежуточные точки)
    int interpSamplesSize = INTERP_COEFF*MAX_CHANNEL_BUFFER_SIZE;
    double interpBuffer[interpSamplesSize];
    for(int h(0); h < interpSamplesSize; h++)
    {
        interpBuffer[h] = splineClass(interpStep*h);
    }

    /* Если текущий канал, является каналом синхронизации, то
     * вычислить для него смещение в соответствии с тригером, уже с отсчетами прошедших интерполяцию
     * Затем установить флаг triggered в true и сохранить смещение в переменной triggeredOffset
    */
    if(trigger->deviceTrigger->ch == channel->channelNum && channel->enabled && trigger->triggered == false)
    {
        trigger->triggerOffset = signalTrigger(trigger->deviceTrigger, &interpBuffer[0],
                            interpSamplesSize, channel->offset, channel->amp);
        trigger->triggered = true;
    }

    if(interpSamplesSize < trigger->triggerOffset)
    {
        trigger->triggerOffset = 0;
        trigger->triggered = false;
        return;
    }

    /* Если после добавления смещения к буферу, кол-вл остальных отсчетов
     * меньше чем размер экрана, то синхронизация не удалась, так как экран на половину пустой
    */
    if(interpSamplesSize - trigger->triggerOffset < SCOPE_SIZE*INTERP_COEFF)
    {
        trigger->triggerOffset = 0;
        trigger->triggered = false;
    }

    /* Скопировать интерполированные отсчеты в буфер выходных отсчетов канала
     * Копирование отсчетов начинается с полученного смещения, для всех каналов
    */
    for(int i(trigger->triggerOffset); i < interpSamplesSize; i++)
    {
        y->append(channel->offset + (interpBuffer[i] * channel->amp));
    }
}

int SignalPlotThread::signalTrigger(TRIGGER_t *tr, double *data, int data_size, double offset, double amp)
{
    if(tr->en)
    {
        if(tr->edge == RISING_EDGE) // Если выбран фронт передний
        {
            for(int ii(BUFFER_OFFSET); ii < data_size; ii++)
            {
                double data_t = data[ii];
                double data_prev = data[ii-1];
                data_t = data_t*amp + offset;
                data_prev = data_prev*amp + offset;

                if(data_t >= tr->level) // Если следующие отсчеты превышают уровень триггера
                {
                    if(data_prev <= tr->level)
                    {
                        return ii;
                    }
                }
            }
        }

        if(tr->edge == FALLING_EDGE) // Если выбран задний фронт
        {
            for(int ii(BUFFER_OFFSET); ii < data_size; ii++)
            {
                double data_t = data[ii];
                double data_prev = data[ii-1];
                data_t = data_t*amp + offset;
                data_prev = data_prev*amp + offset;

                if(data_t <= tr->level)
                {
                    if(data_prev >= tr->level) // Если следующие отсчеты меньше уровня триггера
                        return ii;
                }
            }
        }
    }

    return 0; // Синхронизация не удалась
}
