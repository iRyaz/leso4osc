/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SIGNALPLOTTHREAD_H
#define SIGNALPLOTTHREAD_H

#include <QThread>
#include <QVector>
#include <QMutex>
#include "lib/spline.h"
#include "signalplot.h"

using namespace std;
using namespace tk;

/*
 * Класс поток который интерполирует отсчеты сигнала
*/

class SignalPlotThread : public QThread
{
    Q_OBJECT

    QList<OSC_ITEM> *channelLstPtr; // Указатель на список каналов
    int decimFactor;                // Текущий коэффициент децимации
    double timeStep;                // Интервал дескритизации
    double interpTimeStep;          // Интервал дискретизации после интерполяции

    std::vector<double> timePoints; // Вектор точек на временной оси
    std::vector<double> samplesY;   // Вектор точек на оси Y
    QVector<double> scopePoints;    // Вектор точек на оси Y в пределах экрана

    TRIGGER_IMAGE *trigger;         // Указатель на триггер
    spline splineClass;             // Класс из библиотеки spline.h

    /*
     * Синхронизирует отсчеты с триггером, интерполирует их и заполняет ими вектор
    */
    void addChannelData(MEASURE_CHANNEL_t *channel,
                        double interpStep, tk::spline &splineClass, QVector<double> *y);

    /*
     *  Высчитывает смещение в буфере необходимое для синхронизации в соответствие с триггером
    */
    int signalTrigger(TRIGGER_t *tr, double *data, int data_size, double offset, double amp);

    void run() Q_DECL_OVERRIDE;
    bool busyFlag;

public:
    SignalPlotThread();
    ~SignalPlotThread();
    QVector<double> &getScopePoints() { return scopePoints; }
    bool isBusy() { return busyFlag; }
    void setBusy() { busyFlag = true; }
    void resetBusy() { busyFlag = false; }

    void setChannels(QList<OSC_ITEM> *channelsIn) { channelLstPtr = channelsIn; }

    /*
     * Установить коэффициент децимации нужно перед запуском
    */
    void setDecimFactor(int decim_factor);
    int getDecimFactor() { return decimFactor; }
    void setTrigger(TRIGGER_IMAGE *t) { trigger = t; }

    /*
     * Получить шаг дискретизации после интерполяции
    */
    double getInterpTimeStep() { return interpTimeStep; }
};

#endif // SIGNALPLOTTHREAD_H
