
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.4                                                  **
****************************************************************************/

#ifndef SIGNALPLOTFORM_H
#define SIGNALPLOTFORM_H

#include <QWidget>
#include <QPen>
#include <QColor>
#include <QPixmap>
#include <QThread>
#include <QToolTip>
#include <cmath>
#include <typeinfo.h>
#include <QToolButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMenu>
#include <QMutex>

#include "lib/qcustomplot.h"
#include "signalplotcursor.h"
#include "include/osc_global.h"
#include "include/ui_config.h"
#include "base/common_func.h"
#include "signalplot.h"
#include "signalplotthread.h"
#include "../diagramgrid.h"

/*
 * SignalPlotForm class
 * Класс виджет показывающий осциллограммы сигналов
 * Данные для осциллограммы одного канала содержит структура OSC_ITEM
 * Связный список itemLst, содержит элементы OSC_ITEM для каждого канала
 * Чтобы добавить объект OSC_ITEM нужно вызвать функцию addChannel с параметорос,
 * указателем на струкуру MEASURE_CHANNEL_t. Этот на мышку, может
 * реагировать в двух режимах. Обычный режим "Arrow mode" при щелчке мышкой
 * по графику всплывает подсказка, в которой выводятся координаты на графике.
 * Также в этом режиме можно двигать ползунки курсоров. Второй режим "Trigger move mode"
 * позволяет перемещать мышкой линию триггера, курсоры в этом режиме перемещать нельзя.
 * Переключение между режимами производится выбором, в всплывающем меню,
 * по правой кнопки мыши.
 * Когда данные в буфере обновлены, следует вызывать update(), для обновление графика
*/

class SignalPlotForm : public QWidget
{
    Q_OBJECT

    enum GRAPH_INTERACTION_MODE         // Режим интерактивности графика
    {
        ARROW_INTERACTION_MODE,         // Режим стрелки - показывает координаты при щелчке левой кнопкой мыши
        TRIGGER_MOVE_INTERACTION_MODE   // Режим перемещения линии триггера
    };

    QVBoxLayout layout;
    QHBoxLayout controlLayout;
    QPen xAxisPen;
    QCustomPlot *signalPlot;    // Указаетель на QCustomPlot
    QList<OSC_ITEM> itemLst;    // Список каналов осциллограммы
    QVector<double> scopePoints;// Последовательность точек на временной оси, которые выводятся на экран

    DiagramGrid grid;           // Сетка

    /*
     * Установка сетки на диаграмму
    */
    void setupGrid(QCPAxis *baseAxisX, QCPAxis *baseAxisY, DiagramGrid& initGrid);

    /*
     * Меню вызываемое по нажатию правой кнопки мыши
    */
    QMenu graphModeMenu;
    QAction *arrowAction;
    QAction *triggerAction;
    QAction *showCursorAction;
    QAction *resetCursorAction;
    QAction *resetTriggerAction;
    QMenu selectCursorSourceMenu;

    TRIGGER_IMAGE trigger; // Отображение линии триггера
    CursorsPair<SignalPlotCursor> *plotCursors; // Курсоры
    SignalPlotThread signalThread; // Интерполяция отсчетов производится в отдельном потоке

    double samplesFreq; // Частота дискретизации

    GRAPH_INTERACTION_MODE interactionMode; // Режим итерактивности виджета

    /*
     * Событие мыши
    */
    void arrowModeInteraction(QMouseEvent *event);
    void moveTriggerInteraction(QMouseEvent *event);

    /*
     * Событие при перемещении курсора
    */
    void cursorsModeInteraction(QMouseEvent *event); // Событие перемещение курсора
    double getMovePoint(AMP_SCAN_t ampScan);

protected:
    void changeEvent(QEvent *event);

public:
    explicit SignalPlotForm(QWidget *parent = 0);
    ~SignalPlotForm();

    /*
     * Установить указатель на триггер
    */
    void setTrigger(TRIGGER_t *t) { trigger.deviceTrigger = t; }

    /*
     * Добавить канал измерения на график
    */
    void addChannel(MEASURE_CHANNEL_t *ch);

    /*
     * Сохранить график в файл
    */
    bool saveFile(const QString &fileName, FILE_FORMAT type);

    QCPItemText *notDeviceWarningText;  // Выводится если график не активен

    /*
     * Показать курсоры на диаграмме
     * В отличие от слота void showCursor(bool);
     * устанавливает кнопку в меню showCursorAction; в нужное положение
    */
    void visibleCursors(bool show);
    bool isCursorVisible();

public slots:
    void showCursor(bool);
    void setMaxAmpDiv(MEASURE_CHANNEL_t *channel);

    /*
     * Обновить график с заданным коэффициентом децимации, функция
     * запускает поток, в котором отсчтеты проходят интерполяцию, по
     * окончанию график обновляется
    */
    void update(int decim_factor);
    void threadFinish();
    void clearGraphics();

    void mousePress(QMouseEvent *event);
    void mouseMove(QMouseEvent *event);
    void mouseRelease(QMouseEvent *event);

    void setInteraction(GRAPH_INTERACTION_MODE mode);
    void setLevelTrigger(double level);
    void saveFileSlot();
    void setArrowModeInteractions(bool en);
    void setTriggerModeInteractions(bool en);
    void setCursorModeInteractions(bool en);
    void resetCursor();
    void resetTrigger();

    void selectCursorSourceSlot(QAction *action);

    void loadSettings(QSettings *settingsClass); // Загрузка сохраненных настроек графика
    void saveSettings(QSettings *settingsClass); // Сохранение настроек графика в класс QSettings
    void setEnabled(bool en);   // График задействован или не задействован (активен или не активен)

signals:
    void changeAmpScan(MEASURE_CHANNEL_t *ch);
    void changeTriggerLevel(double level);
};

#endif // SIGNALPLOTFORM_H
