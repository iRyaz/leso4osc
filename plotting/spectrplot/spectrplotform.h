/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.2                                                  **
****************************************************************************/

#ifndef SPECTRPLOTFORM_H
#define SPECTRPLOTFORM_H

#include <QWidget>
#include <QList>
#include <cmath>
#include <complex>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QToolButton>
#include <QFileDialog>
#include <QSettings>

#include "include/osc_global.h"
#include "lib/qcustomplot.h"
#include "spectrplotcursor.h"
#include "base/common_func.h"
#include "spectrplotthread.h"
#include "../diagramgrid.h"

#define SPECT_ITEM_LINE_WIDTH PLOT_ITEM_LINE_WIDTH

/*
 * Класс SpectrPlotForm строит спектр сигнала
 * Для вычисления спектра сигнала используется БПФ
 * БПФ вычисляется в отдельном классе-потоке, который связан со слотом
 * fftTransmissionFinish(). В этом слоте также отправляется сигнал
 * signalFrequencyMeasured(int channel_num, double freq_hz); для каждого канала
 * Он нужен для отображение частоты основной гармоники сигналов в других виджетах
*/

class SpectrPlotForm : public QWidget
{
    Q_OBJECT

    enum GRAPH_INTERACTION_MODE // Режим интерактивности графика
    {
        ARROW_INTERACTION_MODE, // Режим стрелки - показывает координаты при щелчке левой кнопкой мыши
        CURSORS_MODE_INTERACTION_MODE   // Режим перемещения курсоров
    };

    Y_AXIS_MODE axisMode;   // Режим оси Y
    QVBoxLayout layout;
    QList<CHANNEL_SPECTR> itemLst;  // Список элементов каналов
    QCustomPlot *spectrPlot;        // Указатель на QCustomPlot
    QPen xAxisPen;                  //
    DiagramGrid grid;               // Сетка спектрограммы
    void setupGrid(QCPAxis *baseAxisX, QCPAxis *baseAxisY, DiagramGrid& initGrid);
    QVector<double> frequencyPoints;    // Спектральные отсчеты
    double db2volt(double db);          // Перевод децибел в вольты
    SPECTR_WINDOW_TYPE spectrWindow;    // Текущее окно сглаживание спектра
    SpectrPlotThread fftThread;         // Поток для вычисления БПФ
    double samplesFreq;                 // Частота дискретизации

    void setCursorAxisMode(Y_AXIS_MODE mode);   // Установка единиц измерения для курсора

    CursorsPair<SpectrPlotCursor> *plotCursors; // Пара курсоров для спектограммы

    /*
     * Элементы меню
    */
    QMenu graphModeMenu;
    QMenu selectCursorSourceMenu;
    QAction *showCursorAction;
    QAction *resetCursorAction;

    QMenu windowTypeMenu;
    QAction *rectWindow;
    QAction *hammingWindow;
    QAction *hannWindow;
    QAction *nuatallWindow;
    QAction *flatTopWindow;

    QMenu scaleTypeMenu;
    QAction *linearScale;
    QAction *dbmScale;
    QAction *dbiScale;

    /*
     * События при нажатии или преремещении мыши
    */
    void arrowModeInteraction(QMouseEvent *event);
    void cursorsModeInteraction(QMouseEvent *event);

    QCPItemText *notDeviceWarningText;  // Выводится если график не активен

protected:
    void changeEvent(QEvent *event);

public:
    explicit SpectrPlotForm(QWidget *parent = 0);
    ~SpectrPlotForm();

    void setAxisMode(Y_AXIS_MODE mode); // Установка единиц измерения оси Y

    void addChannel(MEASURE_CHANNEL_t *ch);// Добавить канал
    bool saveFile(const QString &fileName, FILE_FORMAT type); // Сохранить спектрограмму в файл

    /*
     * Показать курсоры на диаграмме
     * В отличие от слота void showCursor(bool);
     * устанавливает кнопку в меню showCursorAction; в нужное положение
    */
    void visibleCursors(bool show);
    bool isCursorVisible();

public slots:
    void setMaxAmpDiv(MEASURE_CHANNEL_t *channel);
    void update(int decimFactor); // Обновить спектрограмму
    void clearGraphics(); // Очистить спектрограмму
    void saveFileSlot(); // Сохранить спектрограмму
    void fftTransmissionFinish(); // Поток вычисления БПФ завершен
    void loadSettings(QSettings *settingsClass); // Загрузка сохраненных настроек графика
    void saveSettings(QSettings *settingsClass); // Сохранение настроек графика в класс QSettings

    /*
     * Обработчики событий переключения элементов меню
    */
    void resetCursor();
    void selectRectWindow(bool toggle);
    void selectHammingWindow(bool toggle);
    void selectHannWindow(bool toggle);
    void selectNuatallWindow(bool toggle);
    void selectFlatTopWindow(bool toggle);
    void selectLinearScale(bool toggle);
    void selectDBmScale(bool toggle);
    void selectDBiScale(bool toggle);
    void showCursor(bool visible);
    void selectCursorSourceSlot(QAction *action);

    /*
     * Обработчики событий мыши
    */
    void mousePress(QMouseEvent *event);
    void mouseMove(QMouseEvent *event);
    void mouseRelease(QMouseEvent *event);

    void setEnabled(bool en);   // График задействован или не задействован (активен или не активен)

signals:
    /*
     * Сигнал отправляется частоту наибольшего по амплитуде бина спектра
    */
    void signalFrequencyMeasured(int channel_num, double freq_hz);
};

#endif // SPECTRPLOTFORM_H
