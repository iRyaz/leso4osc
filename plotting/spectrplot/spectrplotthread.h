/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SPECTRPLOTTHREAD_H
#define SPECTRPLOTTHREAD_H

#include <QThread>
#include <QList>
#include <complex>
#include <cmath>

#include "spectrplot.h"
#include <vector>
#include "lib/fftw3.h"

using std::vector;
using std::complex;
using std::pow;
using std::sqrt;

/*
 * Класс поток для вычислени БПФ. Перед запуском нужно инициализировать
 * указатель на список каналов функцией void setLst(QList<CHANNEL_SPECTR> *lst)
*/
class SpectrPlotThread : public QThread
{
    Q_OBJECT

    QList<CHANNEL_SPECTR> *lstPtr;
    Y_AXIS_MODE axisMode;
    SPECTR_WINDOW_TYPE windowType;

    void run() Q_DECL_OVERRIDE;

    double windowFunction(int n, int N);
    void FFT(int n, double *samples, int size);
    void _FFT(complex<double> *samples, int samplesSize);
    complex<double> *splitSamplesArray(complex<double> *array, int size, int step);
    bool busyFlag;

public:
    SpectrPlotThread();
    bool isBusy() { return busyFlag; }
    void setBusy() { busyFlag = true; }
    void resetBusy() { busyFlag = false; }
    // Установить список каналов
    void setLst(QList<CHANNEL_SPECTR> *lst) { lstPtr = lst; }

    // Установить единицы измерения амплитуды
    void setAxisMode(Y_AXIS_MODE mode) { axisMode = mode; }

    // Установить окно сглаживания
    void setWindowType(SPECTR_WINDOW_TYPE w) { windowType = w; }
};

#endif // SPECTRPLOTTHREAD_H
