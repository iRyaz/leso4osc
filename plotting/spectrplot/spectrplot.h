/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SPECTRPLOT_H
#define SPECTRPLOT_H

#include <QAction>
#include <complex>

#include "lib/qcustomplot.h"
#include "include/osc_global.h"
#include "base/common_func.h"

#define DBU_BASE 0.775              // Базовое значение логарифмической оси в dBi
#define DBM_BASE 1                  // Базовое значение логарифмической оси в dbm
#define TOP_DECIBEL_VALUE 30        // Максимально допустимое значение сигнала в децибелах
#define BOTTOM_DECIBEL_VALUE -85    // Минимально допустимое значение сигнала в децибелах

#define KHZ_COEFF 1000              // Коэффициент определения частотной оси в килогерцы

using std::complex;

struct CHANNEL_SPECTR
{
    QCPAxis *amplitudeAxis; // Указатель на ось Y
    MEASURE_CHANNEL_t *channelStruct;   // Указатель на данные канала
    QCPGraph *spectrGraph;              // Указатель на QCPGraph
    complex<double> *spectrPoints;  // Точки спектра
    double *ampPoints;              // Отсчеты сигнала
    double *srcBuffer;              // Буфер для промежуточных вычислений
    int mainBinIndex;               // Индекс самой большрй гармоники сигнала
    QAction *cursorSourceAction;    // Пункт меню для задания исходного канала для курсоров
};

// Окна сглаживание спектра
enum SPECTR_WINDOW_TYPE { RECTANGLE_WINDOW, HAMMING_WINDOW, HANN_WINDOW, NUATTALL_WINDOW, FLAT_TOP_WINDOW };
// Единицы измерения оси Y
enum Y_AXIS_MODE { LINEAR_AXIS, DBU_AXIS, DBM_AXIS };


#endif // SPECTRPLOT_H
