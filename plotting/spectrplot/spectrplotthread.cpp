#include "spectrplotthread.h"

SpectrPlotThread::SpectrPlotThread()
{
    lstPtr = NULL;
    axisMode = LINEAR_AXIS;
    windowType = RECTANGLE_WINDOW;
    busyFlag = false;
}

double SpectrPlotThread::windowFunction(int n, int N)
{
    double ret = 1;

    switch(windowType)
    {
    case RECTANGLE_WINDOW:
        ret = 1;
        break;
    case HAMMING_WINDOW:
        ret = 0.54 - 0.46*cos(2*PI*n/(N - 1));
        break;
    case HANN_WINDOW:
        ret = 0.5 - 0.5*cos(2*PI*n/(N - 1));
        break;
    case NUATTALL_WINDOW:
        ret = 0.355768 - 0.48829 * cos(2*PI*n/(N - 1)) + 0.144232*cos(4*PI*n/(N - 1))
                - 0.012604*cos(6*PI*n/(N - 1));
        break;
    case FLAT_TOP_WINDOW:
        ret = 1 - 1.93*cos(2*PI*n/(N - 1)) + 1.29*cos(4*PI*n/(N - 1)) -
                0.388*cos(6*PI*n/(N - 1)) + 0.032*cos(8*PI*n/(N - 1));
        break;
    }

    return ret;
}

void SpectrPlotThread::FFT(int n, double *samples, int size)
{
    double offset = lstPtr->at(n).channelStruct->offset;
    double amplifier = lstPtr->at(n).channelStruct->amp;

    complex<double> *inSamples = lstPtr->at(n).spectrPoints;

    for(int i(0); i < size; i++)
    {
        inSamples[i].real() = offset + (windowFunction(i, size)* samples[i]*amplifier);
        inSamples[i].imag() = 0;
    }

    _FFT(&inSamples[0], size);

    double maxAmp = 0;
    int maxAmpIndex = 0;

    for(int i(0); i < size; i++)
    {
        double amp = 4*sqrt(pow(inSamples[i].real(), 2) + pow(inSamples[i].imag(), 2))/size;

        switch(axisMode)
        {
        case LINEAR_AXIS:
            lstPtr->at(n).ampPoints[i] = amp;
            break;
        case DBU_AXIS:
            lstPtr->at(n).ampPoints[i] = 20.0f * log10(amp/DBU_BASE);
            break;
        case DBM_AXIS:
            lstPtr->at(n).ampPoints[i] = 20.0f * log10(amp/DBM_BASE);
            break;
        }

        if(amp > maxAmp && i <= size/2 && i > 0)
        {
            maxAmp = amp;
            maxAmpIndex = i;
        }
    }

    CHANNEL_SPECTR s = lstPtr->at(n);
    s.mainBinIndex = maxAmpIndex;
    lstPtr->replace(n, s);
}

void SpectrPlotThread::_FFT(complex<double> *samples, int samplesSize)
{
    if(samplesSize <= 1)
        return;

    complex<double> *first = splitSamplesArray(samples, samplesSize/2, 2);
    complex<double> *second = splitSamplesArray(samples + 1, samplesSize/2, 2);

    _FFT(first, samplesSize/2);
    _FFT(second, samplesSize/2);

    for(int i(0); i < samplesSize/2; i++)
    {
        complex<double> currentComplex(1, 0);
        if(i != 0)
        {
            currentComplex.real() = cos(2*PI*i/samplesSize);
            currentComplex.imag() = sin(2*PI*i/samplesSize);
        }

        complex<double> c1 = first[i] + (currentComplex* second[i]);
        complex<double> c2 = first[i] - (currentComplex* second[i]);
        samples[i] = c1;
        samples[i+samplesSize/2] = c2;
    }

   delete first;
   delete second;
}

complex<double>* SpectrPlotThread::splitSamplesArray(complex<double> *array, int size, int step)
{
    complex<double> *newArray = new complex<double>[size];

    for(int i(0), j(0); i < size; i++, j += step)
        newArray[i] = array[j];

    return newArray;
}

void SpectrPlotThread::run() {

    for(int i(0); i < lstPtr->size(); i++)
    {
        if(lstPtr->at(i).channelStruct->enabled)
        {
            double maxAmp = 0;
            int maxAmpIndex = 0;
            int bufferSize = lstPtr->at(i).channelStruct->sampleBufferSize;
            vector<complex<double> > data(bufferSize, 0);

            double offset = lstPtr->at(i).channelStruct->offset;
            double amplifier = lstPtr->at(i).channelStruct->amp;

            for(int j(0); j < bufferSize; j++)
            {
                data[j] = (lstPtr->at(i).srcBuffer[j] * (windowFunction(j, bufferSize) * amplifier)) + offset;
            }

            fftw_plan plan = fftw_plan_dft_1d(data.size(), (fftw_complex*) &data[0],
                    (fftw_complex*) &data[0], FFTW_FORWARD, FFTW_MEASURE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            for(int j(0); j < bufferSize; j++)
            {
                double amp = 0;
                double real = data[j].real();
                double imag = data[j].imag();
                amp = 4*(sqrt(pow(real, 2) + pow(imag, 2)))/bufferSize;

                switch(axisMode)
                {
                case LINEAR_AXIS:
                    lstPtr->at(i).ampPoints[j] = amp;
                    break;
                case DBU_AXIS:
                    lstPtr->at(i).ampPoints[j] = 20.0f * log10(amp/DBU_BASE);
                    break;
                case DBM_AXIS:
                    lstPtr->at(i).ampPoints[j] = 20.0f * log10(amp/DBM_BASE);
                    break;
                }

                if(amp > maxAmp && j <= bufferSize/2 && j > 0)
                {
                    maxAmp = amp;
                    maxAmpIndex = j;
                }
            }

            CHANNEL_SPECTR s = lstPtr->at(i);
            s.mainBinIndex = maxAmpIndex;
            lstPtr->replace(i, s);
        }
    }
}

