/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SPECTRPLOTCURSOR_H
#define SPECTRPLOTCURSOR_H

#include "spectrplot.h"
#include "../qcpcursor.h"

class SpectrPlotCursor : public PlotCursor
{
    double getLim(AMP_SCAN_t ampScan);
    double getTimeLim(int d);
    Y_AXIS_MODE axisMode;

public:
    SpectrPlotCursor(QCustomPlot *plot);
    void setAxisMode(Y_AXIS_MODE mode) { axisMode = mode; }
};

#endif // SPECTRPLOTCURSOR_H
