#include "spectrplotcursor.h"

SpectrPlotCursor::SpectrPlotCursor(QCustomPlot *plot) : PlotCursor(plot)
{

}

double SpectrPlotCursor::getLim(AMP_SCAN_t ampScan)
{
    double ret = 0;
    if(axisMode != LINEAR_AXIS)
    {
        ret = 1;
        return ret;
    }

    switch(ampScan)
    {
    case AMP_SCAN_1_500:
        ret = 1;
        break;
    case AMP_SCAN_1_200:
        ret = 0.5;
        break;
    case AMP_SCAN_1_100:
        ret = 0.2;
        break;
    case AMP_SCAN_1_50:
        ret = 0.1;
        break;
    case AMP_SCAN_1_20:
        ret = 0.05;
        break;
    case AMP_SCAN_1_10:
        ret = 0.02;
        break;
    case AMP_SCAN_1_5:
        ret = 0.01;
        break;
    case AMP_SCAN_1_2:
        ret = 0.005;
        break;
    case AMP_SCAN_1_1:
        ret = 0.001;
        break;
    }

    return ret;
}

double SpectrPlotCursor::getTimeLim(int d)
{
    double ret = 10;

    switch(d)
    {
    case 2000:
        ret = 0.05;
        break;
    case 1000:
        ret = 0.1;
        break;
    case 500:
        ret = 0.2;
        break;
    case 200:
        ret = 0.5;
        break;
    case 100:
        ret = 1;
        break;
    case 50:
        ret = 2;
        break;
    case 20:
        ret = 4;
        break;
    case 10:
        ret = 8;
        break;
    case 5:
        ret = 16;
        break;
    case 2:
        ret = 32;
        break;
    case 1:
        ret = 64;
        break;
    }

    return ret;
}


