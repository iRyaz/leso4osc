/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.21                                                 **
****************************************************************************/

#include "spectrplotform.h"

SpectrPlotForm::SpectrPlotForm(QWidget *parent) :
    QWidget(parent)
{
    spectrPlot = new QCustomPlot;
    spectrPlot->setBackground(QColor((PLOT_BACKGROUND_COLOR&0xFF000000)>>24,
                                     (PLOT_BACKGROUND_COLOR&0x00FF0000)>>16,
                                     (PLOT_BACKGROUND_COLOR&0x0000FF00)>>8,
                                     (PLOT_BACKGROUND_COLOR&0x000000FF)));

    spectrPlot->xAxis->setLabelColor(QColor((X_AXIS_LABEL_COLOR&0xFF000000)>>24,
                                            (X_AXIS_LABEL_COLOR&0x00FF0000)>>16,
                                            (X_AXIS_LABEL_COLOR&0x0000FF00)>>8,
                                            (X_AXIS_LABEL_COLOR&0x000000FF)));

    spectrPlot->xAxis->setTickLabelColor(QColor((X_AXIS_TICK_COLOR&0xFF000000)>>24,
                                                (X_AXIS_TICK_COLOR&0x00FF0000)>>16,
                                                (X_AXIS_TICK_COLOR&0x0000FF00)>>8,
                                                (X_AXIS_TICK_COLOR&0x000000FF)));

    xAxisPen.setColor(QColor((BASE_TIME_COLOR&0xFF000000)>>24,
                             (BASE_TIME_COLOR&0x00FF0000)>>16,
                             (BASE_TIME_COLOR&0x0000FF00)>>8,
                             (BASE_TIME_COLOR&0x000000FF)));

    xAxisPen.setWidthF(SPECT_ITEM_LINE_WIDTH);

    spectrPlot->xAxis->setBasePen(xAxisPen);
    spectrPlot->xAxis->setAntialiased(false);

    grid.zeroLineGridPen = DEFAULT_GRID_PEN_STYLE;

    spectrPlot->setPlottingHint(QCP::phFastPolylines);

    spectrPlot->xAxis->setLabel(tr("Frequency (kHz)"));
    spectrPlot->yAxis->setLabel(tr("Amplitude"));
    layout.addWidget(spectrPlot);
    spectrPlot->xAxis->setVisible(true);
    spectrPlot->yAxis->setVisible(false);
    setLayout(&layout);

    spectrWindow = RECTANGLE_WINDOW;
    connect(spectrPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress(QMouseEvent*)));
    connect(spectrPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMove(QMouseEvent*)));
    connect(spectrPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseRelease(QMouseEvent*)));

    setAxisMode(LINEAR_AXIS);

    connect(&fftThread, SIGNAL(finished()), this, SLOT(fftTransmissionFinish()));

    windowTypeMenu.setTitle(tr("Window type"));
    rectWindow = windowTypeMenu.addAction(QIcon(":/button_icons/window1.png"), tr("Rect"));
    hammingWindow = windowTypeMenu.addAction(QIcon(":/button_icons/window2.png"), tr("Hamming"));
    hannWindow = windowTypeMenu.addAction(QIcon(":/button_icons/window3.png"), tr("Hann"));
    nuatallWindow = windowTypeMenu.addAction(QIcon(":/button_icons/window4.png"), tr("Nuatall"));
    flatTopWindow = windowTypeMenu.addAction(QIcon(":/button_icons/window5.png"), tr("Flat"));
    rectWindow->setCheckable(true);
    rectWindow->setChecked(true);
    hammingWindow->setCheckable(true);
    hannWindow->setCheckable(true);
    nuatallWindow->setCheckable(true);
    flatTopWindow->setCheckable(true);

    connect(rectWindow, SIGNAL(triggered(bool)), this, SLOT(selectRectWindow(bool)));
    connect(hammingWindow, SIGNAL(triggered(bool)), this, SLOT(selectHammingWindow(bool)));
    connect(hannWindow, SIGNAL(triggered(bool)), this, SLOT(selectHannWindow(bool)));
    connect(nuatallWindow, SIGNAL(triggered(bool)), this, SLOT(selectNuatallWindow(bool)));
    connect(flatTopWindow, SIGNAL(triggered(bool)), this, SLOT(selectFlatTopWindow(bool)));

    scaleTypeMenu.setTitle(tr("Scale type"));
    linearScale = scaleTypeMenu.addAction(QIcon(":/button_icons/linear_axis.png"), tr("Linear"));
    dbmScale = scaleTypeMenu.addAction(QIcon(":/button_icons/dbm_axis.png"), tr("dbm"));
    dbiScale = scaleTypeMenu.addAction(QIcon(":/button_icons/dbu_axis.png"), tr("dbi"));
    linearScale->setCheckable(true);
    linearScale->setChecked(true);
    dbiScale->setCheckable(true);
    dbmScale->setCheckable(true);

    connect(linearScale, SIGNAL(triggered(bool)), this, SLOT(selectLinearScale(bool)));
    connect(dbmScale, SIGNAL(triggered(bool)), this, SLOT(selectDBmScale(bool)));
    connect(dbiScale, SIGNAL(triggered(bool)), this, SLOT(selectDBiScale(bool)));

    showCursorAction = graphModeMenu.addAction(tr("Show cursor"));
    showCursorAction->setCheckable(true);
    showCursorAction->setChecked(false);
    selectCursorSourceMenu.setTitle(tr("Cursor source"));
    graphModeMenu.addMenu(&selectCursorSourceMenu);
    graphModeMenu.addMenu(&scaleTypeMenu);
    graphModeMenu.addMenu(&windowTypeMenu);
    graphModeMenu.addSeparator();

    resetCursorAction = graphModeMenu.addAction(tr("Reset cursors"));

    connect(resetCursorAction, SIGNAL(triggered()), this, SLOT(resetCursor()));
    connect(showCursorAction, SIGNAL(toggled(bool)), this, SLOT(showCursor(bool)));
    connect(&selectCursorSourceMenu, SIGNAL(triggered(QAction*)), this, SLOT(selectCursorSourceSlot(QAction*)));

    plotCursors = new CursorsPair<SpectrPlotCursor>(spectrPlot);
    showCursorAction->setEnabled(false);

    notDeviceWarningText = new QCPItemText(spectrPlot);
    notDeviceWarningText->setText(tr("Device not connected !"));
    QFont warningFont("Warning", WARNING_LABEL_TEXT_SIZE, QFont::Bold);
    notDeviceWarningText->setFont(warningFont);
    notDeviceWarningText->setColor(QColor((WARNING_COLOR_TEXT & 0xFF000000)>> 24,
                                          (WARNING_COLOR_TEXT & 0x00FF0000)>> 16,
                                          (WARNING_COLOR_TEXT & 0x0000FF00)>> 8,
                                           WARNING_COLOR_TEXT & 0x000000FF));
    QPen warningPen;
    warningPen.setColor(QColor((WARNING_COLOR_RECT & 0xFF000000)>> 24,
                               (WARNING_COLOR_RECT & 0x00FF0000)>> 16,
                               (WARNING_COLOR_RECT & 0x0000FF00)>> 8,
                                WARNING_COLOR_TEXT & 0x000000FF));
    warningPen.setStyle(Qt::DashLine);

    warningPen.setWidthF(WARNING_RECT_LINE_WIDTH);
    notDeviceWarningText->setPen(warningPen);
    notDeviceWarningText->setVisible(false);
    notDeviceWarningText->setPositionAlignment(Qt::AlignCenter | Qt::AlignTop);
    spectrPlot->addItem(notDeviceWarningText);
    notDeviceWarningText->position->setType(QCPItemPosition::ptAxisRectRatio);
    notDeviceWarningText->position->setCoords(0.5, 0.5);
}

SpectrPlotForm::~SpectrPlotForm()
{
    fftThread.wait(5000);

    for(int n(0); n < itemLst.size(); n++)
    {
        delete itemLst.at(n).spectrPoints;
        delete itemLst.at(n).ampPoints;
    }

    qDebug() << "destroy class SpectrPlotForm - memory clean !!!";
}

void SpectrPlotForm::setAxisMode(Y_AXIS_MODE mode)
{
    axisMode = mode;
}

bool SpectrPlotForm::saveFile(const QString &fileName, FILE_FORMAT type)
{
    bool ret = false;

    switch(type)
    {
    case PNG_FORMAT:
        ret = spectrPlot->savePng(fileName);
        break;
    case PDF_FORMAT:
        ret = spectrPlot->savePdf(fileName, true);
        break;
    case JPG_FORMAT:
        ret = spectrPlot->saveJpg(fileName);
        break;
    case BMP_FORMAT:
        ret = spectrPlot->saveBmp(fileName);
        break;
    }

    return ret;
}

void SpectrPlotForm::saveFileSlot()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList filters;
    filters << JPG_FORMAT_FILTER
        << BMP_FORMAT_FILTER;
    dialog.setNameFilters(filters);
    QStringList files;
    if(dialog.exec())
    {
        files = dialog.selectedFiles();
        qDebug() << "Save Spectr to file: " << files.at(0);
        if(dialog.selectedNameFilter().compare(QString(JPG_FORMAT_FILTER)))
                saveFile(files.at(0), JPG_FORMAT);
        else
        {
            if(dialog.selectedNameFilter().compare(QString(BMP_FORMAT_FILTER)))
                    saveFile(files.at(0), BMP_FORMAT);
        }
    }
}

void SpectrPlotForm::update(int decimFactor)
{
    if(fftThread.isRunning()) return;
    if(fftThread.isBusy()) return;
    if(!isEnabled()) return;

    samplesFreq = BASE_FREQUENCY/(double)decimFactor;
    spectrPlot->xAxis->setRange(0, samplesFreq/2/KHZ_COEFF);

    plotCursors->setDecimFactor(decimFactor);
    plotCursors->update();

    bool allChannelsDisables = true;

    for(int i(0); i < itemLst.size(); i++)
    {
        int n = itemLst.at(i).channelStruct->channelNum;
        setMaxAmpDiv(itemLst.at(i).channelStruct);
        if(itemLst.at(i).channelStruct->enabled)
        {
            allChannelsDisables = false;
            spectrPlot->graph(n)->setVisible(true);
            itemLst.at(i).amplitudeAxis->setVisible(true);
            itemLst.at(i).spectrGraph->setVisible(true);
            for(int j(0); j < itemLst.at(i).channelStruct->sampleBufferSize; j++)
            {
                itemLst.at(i).srcBuffer[j] = itemLst.at(i).channelStruct->samplePtr[j];
            }
        }
        else
        {
            spectrPlot->graph(n)->setVisible(false);
            itemLst.at(i).amplitudeAxis->setVisible(false);
            itemLst.at(i).spectrGraph->setVisible(false);
        }
    }

    fftThread.setWindowType(spectrWindow);
    fftThread.setAxisMode(axisMode);
    fftThread.setLst(&itemLst);
    fftThread.setBusy();
    fftThread.start(QThread::HighestPriority);

    if(allChannelsDisables)
        itemLst.at(0).amplitudeAxis->setVisible(true);
}

void SpectrPlotForm::fftTransmissionFinish()
{
    int size = itemLst.at(0).channelStruct->sampleBufferSize;
    double freqStep = samplesFreq/size/KHZ_COEFF;

    for(int n(0); n < itemLst.size(); n++)
    {
        QVector<double> x, y;
        itemLst.at(n).spectrGraph->clearData();
        for(int i(0); i < size; i++)
        {
            x.append(i*freqStep);
            if(itemLst.at(n).ampPoints[i] < BOTTOM_DECIBEL_VALUE)
                y.append(BOTTOM_DECIBEL_VALUE);
            else
            {
                y.append(itemLst.at(n).ampPoints[i]);
            }
        }

        if(itemLst.at(n).channelStruct->enabled)
        {
            if(itemLst.at(n).amplitudeAxis == plotCursors->getYAxis())
            {
                showCursorAction->setEnabled(true);
            }
        }
        else
        {
            if(itemLst.at(n).amplitudeAxis == plotCursors->getYAxis())
            {
                showCursorAction->setEnabled(false);
                showCursorAction->setChecked(false);
                plotCursors->setVisible(false);
                showCursor(false);
            }
        }

        int maxBinIndex = itemLst.at(n).mainBinIndex; // Индекс наибольшего бина
        itemLst.at(n).spectrGraph->setData(x, y);
        emit signalFrequencyMeasured(n, maxBinIndex*samplesFreq/size);
    }

    spectrPlot->replot(QCustomPlot::rpHint);
    fftThread.resetBusy();
}

void SpectrPlotForm::clearGraphics()
{
    for(int i(0); i < spectrPlot->graphCount(); i++)
    {
        spectrPlot->graph(i)->clearData();
    }
}

void SpectrPlotForm::addChannel(MEASURE_CHANNEL_t *ch)
{
    CHANNEL_SPECTR newItem;

    int color_int;
    QCPAxis *axis;
    QCPGraph *g;
    QPen pen;

    pen.setWidthF(0);

    color_int = ((ch->color)&0xFFFFFF00)>>8;
    pen.setColor(QColor((color_int>>16)&0xFF, (color_int>>8)&0xFF, color_int&0xFF));
    pen.setWidth(SPECT_ITEM_LINE_WIDTH);

    if((spectrPlot->graphCount())%2)
        axis = spectrPlot->axisRects().at(0)->addAxis(QCPAxis::atRight);
    else
        axis = spectrPlot->axisRects().at(0)->addAxis(QCPAxis::atLeft);

    axis->setVisible(false);
    axis->grid()->setVisible(true);
    axis->setAntialiased(false);
    axis->setTickLabelColor(QColor((color_int>>16)&0xFF, (color_int>>8)&0xFF, color_int&0xFF));
    setupGrid(spectrPlot->xAxis, axis, grid);

    g = spectrPlot->addGraph();
    g->setAdaptiveSampling(false);
    g->rescaleAxes(true);

    axis->setAutoTickStep(false);
    axis->setTickStep(5.0f);
    axis->setRange(0, 20);
    newItem.amplitudeAxis = axis;

    axis->setBasePen(pen);
    axis->setTickPen(pen);
    axis->setSubTickPen(pen);
    axis->setAntialiased(false);

    g->setAntialiased(false);
    g->setAntialiasedFill(false);
    g->setPen(pen);
    g->setValueAxis(axis);
    g->setKeyAxis(spectrPlot->xAxis);
    g->rescaleKeyAxis(true);
    g->setName(tr("Channel ") + QString(ch->id));

    QCPGraph *spectrGraph = new QCPGraph(g->keyAxis(), g->valueAxis());
    spectrGraph->setAntialiased(false);
    spectrGraph->setPen(pen);
    newItem.spectrGraph = spectrGraph;
    newItem.srcBuffer = new double[MAX_CHANNEL_BUFFER_SIZE];

    newItem.spectrPoints = new complex<double>[MAX_CHANNEL_BUFFER_SIZE];
    newItem.ampPoints = new double[MAX_CHANNEL_BUFFER_SIZE];
    newItem.mainBinIndex = 0;

    newItem.cursorSourceAction = selectCursorSourceMenu.addAction(tr("Channel ") + QString(ch->id));
    newItem.cursorSourceAction->setCheckable(true);
    if(ch->channelNum == 0)
    {
        plotCursors->setSourceChannel(ch);
        plotCursors->setYAxis(axis);
        newItem.cursorSourceAction->setChecked(true);
    }

    newItem.channelStruct = ch;
    itemLst.append(newItem);
}

void SpectrPlotForm::setMaxAmpDiv(MEASURE_CHANNEL_t *channel)
{
    double maxAmp = MAX_AMP_DIV_1_500;
    double maxDBU = TOP_DECIBEL_VALUE;
    double minDBU = BOTTOM_DECIBEL_VALUE;

    switch(channel->ampScan)
    {
    case AMP_SCAN_1_500:
        maxAmp = MAX_AMP_DIV_1_500;
        maxDBU = 35;
        minDBU = -20;
        break;
    case AMP_SCAN_1_200:
        maxAmp = MAX_AMP_DIV_1_200;
        maxDBU = 25;
        minDBU = -30;
        break;
    case AMP_SCAN_1_100:
        maxAmp = MAX_AMP_DIV_1_100;
        maxDBU = 20;
        minDBU = -35;
        break;
    case AMP_SCAN_1_50:
        maxAmp = MAX_AMP_DIV_1_50;
        maxDBU = 15;
        minDBU = -40;
        break;
    case AMP_SCAN_1_20:
        maxAmp = MAX_AMP_DIV_1_20;
        maxDBU = 10;
        minDBU = -45;
        break;
    case AMP_SCAN_1_10:
        maxAmp = MAX_AMP_DIV_1_10;
        maxDBU = 5;
        minDBU = -50;
        break;
    case AMP_SCAN_1_5:
        maxAmp = MAX_AMP_DIV_1_5;
        maxDBU = 0;
        minDBU = -55;
        break;
    case AMP_SCAN_1_2:
        maxAmp = MAX_AMP_DIV_1_2;
        maxDBU = -5;
        minDBU = -60;
        break;
    case AMP_SCAN_1_1:
        maxAmp = MAX_AMP_DIV_1_1;
        maxDBU = -10;
        minDBU = -65;
        break;
    }

    if(channel->channelNum < itemLst.size())
    {
        if(axisMode == LINEAR_AXIS)
        {
            itemLst.at(channel->channelNum).amplitudeAxis->setTickStep(maxAmp/4);
            itemLst.at(channel->channelNum).amplitudeAxis->setRange(0, 2*maxAmp);
            itemLst.at(channel->channelNum).amplitudeAxis->setScaleType(QCPAxis::stLinear);
        }
        else
        {
            itemLst.at(channel->channelNum).amplitudeAxis->setTickStep(5);
            itemLst.at(channel->channelNum).amplitudeAxis->setRange(minDBU, maxDBU);
            CHANNEL_SPECTR currentItem = itemLst.at(channel->channelNum);
            itemLst.replace(channel->channelNum, currentItem);
        }
    }
    else
    {
        qDebug() << "Channel: " << channel->channelNum << " " <<
                    "not found in plot class";
    }
}

void SpectrPlotForm::setupGrid(QCPAxis *baseAxisX, QCPAxis *baseAxisY, DiagramGrid &initGrid)
{
    baseAxisX->grid()->setSubGridVisible(true);
    baseAxisX->grid()->setPen(initGrid.gridPen);
    baseAxisX->grid()->setSubGridPen(initGrid.subGridPen);
    baseAxisX->grid()->setZeroLinePen(initGrid.gridPen);

    baseAxisY->grid()->setSubGridVisible(false);
    baseAxisY->grid()->setPen(initGrid.gridPen);
    baseAxisY->grid()->setSubGridPen(initGrid.gridPen);
    baseAxisY->grid()->setZeroLinePen(initGrid.gridPen);

    baseAxisX->grid()->setAntialiased(false);
    baseAxisY->grid()->setAntialiased(false);
}

void SpectrPlotForm::setCursorAxisMode(Y_AXIS_MODE mode)
{
    plotCursors->firstCursor->setAxisMode(mode);
    plotCursors->secondCursor->setAxisMode(mode);
}

void SpectrPlotForm::selectLinearScale(bool toggle)
{
    if(toggle)
    {
        dbiScale->setChecked(false);
        dbmScale->setChecked(false);
        axisMode = LINEAR_AXIS;
        setCursorAxisMode(LINEAR_AXIS);
    }
    else
    {
        linearScale->setChecked(true);
        axisMode = LINEAR_AXIS;
        setCursorAxisMode(LINEAR_AXIS);
    }
}

void SpectrPlotForm::selectDBmScale(bool toggle)
{
    if(toggle)
    {
        dbiScale->setChecked(false);
        linearScale->setChecked(false);
        axisMode = DBM_AXIS;
        setCursorAxisMode(DBM_AXIS);
    }
    else
    {
        linearScale->setChecked(true);
        axisMode = LINEAR_AXIS;
        setCursorAxisMode(LINEAR_AXIS);
    }
}

void SpectrPlotForm::selectDBiScale(bool toggle)
{
    if(toggle)
    {
        linearScale->setChecked(false);
        dbmScale->setChecked(false);
        axisMode = DBU_AXIS;
        setCursorAxisMode(DBU_AXIS);
    }
    else
    {
        linearScale->setChecked(true);
        axisMode = LINEAR_AXIS;
        setCursorAxisMode(LINEAR_AXIS);
    }
}

void SpectrPlotForm::selectRectWindow(bool toggle)
{
    if(toggle)
    {
        hammingWindow->setChecked(false);
        hannWindow->setChecked(false);
        nuatallWindow->setChecked(false);
        flatTopWindow->setChecked(false);
        spectrWindow = RECTANGLE_WINDOW;
    }
    else
    {
        spectrWindow = RECTANGLE_WINDOW;
        rectWindow->setChecked(true);
    }
}

void SpectrPlotForm::selectHammingWindow(bool toggle)
{
    if(toggle)
    {
        rectWindow->setChecked(false);
        hannWindow->setChecked(false);
        nuatallWindow->setChecked(false);
        flatTopWindow->setChecked(false);

        spectrWindow = HAMMING_WINDOW;
    }
    else
    {
        spectrWindow = RECTANGLE_WINDOW;
        rectWindow->setChecked(true);
    }
}

void SpectrPlotForm::selectHannWindow(bool toggle)
{
    if(toggle)
    {
        hammingWindow->setChecked(false);
        rectWindow->setChecked(false);
        nuatallWindow->setChecked(false);
        flatTopWindow->setChecked(false);

        spectrWindow = HANN_WINDOW;
    }
    else
    {
        rectWindow->setChecked(true);
        spectrWindow = RECTANGLE_WINDOW;
    }
}

void SpectrPlotForm::selectNuatallWindow(bool toggle)
{
    if(toggle)
    {
        hammingWindow->setChecked(false);
        hannWindow->setChecked(false);
        rectWindow->setChecked(false);
        flatTopWindow->setChecked(false);

        spectrWindow = NUATTALL_WINDOW;
    }
    else
    {
        spectrWindow = RECTANGLE_WINDOW;
        rectWindow->setChecked(true);
    }
}

void SpectrPlotForm::selectFlatTopWindow(bool toggle)
{
    if(toggle)
    {
        hammingWindow->setChecked(false);
        hannWindow->setChecked(false);
        nuatallWindow->setChecked(false);
        rectWindow->setChecked(false);

        spectrWindow = FLAT_TOP_WINDOW;
    }
    else
    {
        spectrWindow = RECTANGLE_WINDOW;
        rectWindow->setChecked(true);
    }
}

void SpectrPlotForm::arrowModeInteraction(QMouseEvent *event)
{
    QCPAxis *channelXAxis = spectrPlot->xAxis;
    QString messageToolTip = tr("Frequency: ") +
            getFrequency(channelXAxis->pixelToCoord(event->pos().x())*KHZ_COEFF) + "\n";

    for(int i(0); i < itemLst.size(); i++)
    {
        MEASURE_CHANNEL_t *ch = itemLst.at(i).channelStruct;
        if(ch->enabled)
        {
            QCPAxis *channelYAxis = itemLst.at(i).amplitudeAxis;

            switch(axisMode)
            {
            case LINEAR_AXIS:
                messageToolTip +=
                    tr("Channel ") + QString(ch->id) + " :    " +
                    QString::number(ceil(channelYAxis->pixelToCoord(event->pos().y()), ch->ampScan)) +
                    tr(" V ") + " \n";
                break;
            case DBU_AXIS:
                messageToolTip +=
                    tr("Channel ") + QString(ch->id) + " :    " +
                    QString::number(ceil(channelYAxis->pixelToCoord(event->pos().y()), ch->ampScan)) +
                    tr(" dBu    ") + QString::number(db2volt(ceil(channelYAxis->pixelToCoord(event->pos().y()))))
                    + tr(" V") + "\n";
                break;
            case DBM_AXIS:
                messageToolTip +=
                    tr("Channel ") + QString(ch->id) + " :    " +
                    QString::number(ceil(channelYAxis->pixelToCoord(event->pos().y()), ch->ampScan)) +
                    tr(" dBm    ") + QString::number(db2volt(ceil(channelYAxis->pixelToCoord(event->pos().y()))))
                    + tr(" V") + "\n";
                break;
            }
        }
    }

    QToolTip::showText(event->globalPos(), messageToolTip);
}

void SpectrPlotForm::cursorsModeInteraction(QMouseEvent *event)
{
    plotCursors->mousePress(event);
}

void SpectrPlotForm::mousePress(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        if(plotCursors->isVisible())
            plotCursors->mousePress(event);
        if(!plotCursors->firstCursor->isSelect() && !plotCursors->secondCursor->isSelect())
            arrowModeInteraction(event);
    }

    if(event->button() == Qt::RightButton)
    {
        graphModeMenu.popup(event->globalPos());
    }
}

void SpectrPlotForm::mouseMove(QMouseEvent *event)
{
    if(plotCursors->isVisible())
        plotCursors->mouseMove(event);
}

void SpectrPlotForm::mouseRelease(QMouseEvent *event)
{
    if(plotCursors->isVisible())
        plotCursors->mouseRelease(event);
}

double SpectrPlotForm::db2volt(double db)
{
    if(axisMode != LINEAR_AXIS)
    {
        if(axisMode == DBU_AXIS)
            return std::pow(10, db/20)*DBU_BASE;
        if(axisMode == DBM_AXIS)
            return std::pow(10, db/20)*DBM_BASE;
    }
    return db;
}

void SpectrPlotForm::loadSettings(QSettings *settingsClass)
{
    settingsClass->beginGroup("SPECTR");
    switch(settingsClass->value("WINDOW_TYPE").toInt())
    {
    case 0:
        rectWindow->setChecked(true);
        selectRectWindow(true);
        break;
    case 1:
        hammingWindow->setChecked(true);
        selectHammingWindow(true);
        break;
    case 2:
        hannWindow->setChecked(true);
        selectHannWindow(true);
        break;
    case 3:
        nuatallWindow->setChecked(true);
        selectNuatallWindow(true);
        break;
    case 4:
        flatTopWindow->setChecked(true);
        selectFlatTopWindow(true);
        break;
    }

    switch(settingsClass->value("Y_AXIS_MODE").toInt())
    {
    case 0:
        linearScale->setChecked(true);
        selectLinearScale(true);
        break;
    case 1:
        dbmScale->setChecked(true);
        selectDBmScale(true);
        break;
    case 2:
        dbiScale->setChecked(true);
        selectDBiScale(true);
        break;
    }

    if(settingsClass->value("CURSOR_EN").toBool())
    {
        int srcChannel = settingsClass->value("CURSOR_SRC_CHANNEL_NUM").toDouble();
        if(itemLst.at(srcChannel).channelStruct->enabled)
        {
            showCursorAction->setChecked(true);
            plotCursors->setVisible(true);
            itemLst.at(srcChannel).cursorSourceAction->setChecked(true);
            selectCursorSourceSlot(itemLst.at(srcChannel).cursorSourceAction);
            double firstX = settingsClass->value("CURSOR_FIRST_X").toDouble();
            double firstY = settingsClass->value("CURSOR_FIRST_Y").toDouble();
            double secondX = settingsClass->value("CURSOR_SECOND_X").toDouble();
            double secondY = settingsClass->value("CURSOR_SECOND_Y").toDouble();
            plotCursors->firstCursor->setAxisCoords(firstX, firstY);
            plotCursors->secondCursor->setAxisCoords(secondX, secondY);
        }
    }
    else
    {
        plotCursors->setVisible(false);
        showCursorAction->setChecked(false);
    }

    settingsClass->endGroup();
}

void SpectrPlotForm::saveSettings(QSettings *settingsClass)
{
    settingsClass->beginGroup("SPECTR");
    int axisModeint = 0;
    switch(axisMode)
    {
    case LINEAR_AXIS:
        axisModeint = 0;
        break;
    case DBM_AXIS:
        axisModeint = 1;
        break;
    case DBU_AXIS:
        axisModeint = 2;
        break;
    }

    settingsClass->setValue("Y_AXIS_MODE", axisModeint);

    int windowTypeint = 0;
    switch(spectrWindow)
    {
    case RECTANGLE_WINDOW:
        windowTypeint = 0;
        break;
    case HAMMING_WINDOW:
        windowTypeint = 1;
        break;
    case HANN_WINDOW:
        windowTypeint = 2;
        break;
    case NUATTALL_WINDOW:
        windowTypeint = 3;
        break;
    case FLAT_TOP_WINDOW:
        windowTypeint = 4;
        break;
    }

    settingsClass->setValue("WINDOW_TYPE", windowTypeint);
    settingsClass->setValue("CURSOR_EN", plotCursors->isVisible());
    int cursorSrcChannel = 0;
    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
        {
            cursorSrcChannel = i;
            break;
        }
    }

    settingsClass->setValue("CURSOR_SRC_CHANNEL_NUM", cursorSrcChannel);
    settingsClass->setValue("CURSOR_FIRST_X", plotCursors->firstCursor->getX());
    settingsClass->setValue("CURSOR_FIRST_Y", plotCursors->firstCursor->getY());
    settingsClass->setValue("CURSOR_SECOND_X", plotCursors->secondCursor->getX());
    settingsClass->setValue("CURSOR_SECOND_Y", plotCursors->secondCursor->getY());
    settingsClass->endGroup();
}

void SpectrPlotForm::resetCursor()
{
    plotCursors->resetCursors();
}

void SpectrPlotForm::showCursor(bool visible)
{
    if(visible == false)
    {
        plotCursors->setVisible(false);
        spectrPlot->setCursor(Qt::ArrowCursor);
    }

    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
            selectCursorSourceSlot(itemLst.at(i).cursorSourceAction);
    }

    plotCursors->showCursor(visible);
}

void SpectrPlotForm::selectCursorSourceSlot(QAction *action)
{
    bool allChannelUncheck = false;
    for(int i(0); i < itemLst.size(); i++)
    {
        if(itemLst.at(i).cursorSourceAction->isChecked())
            allChannelUncheck = true;

        if(itemLst.at(i).cursorSourceAction == action)
        {
            QCPAxis *axis= itemLst.at(i).amplitudeAxis;
            plotCursors->setYAxis(axis);
            plotCursors->setAmpScan(itemLst.at(i).channelStruct->ampScan);
            plotCursors->setSourceChannel(itemLst.at(i).channelStruct);
            plotCursors->resetCursors();
            if(!itemLst.at(i).channelStruct->enabled)
            {
                showCursorAction->setChecked(false);
                showCursorAction->setEnabled(false);
            }
        }
        else
        {
            itemLst.at(i).cursorSourceAction->setChecked(false);
        }
    }

    if(allChannelUncheck == false)
    {
        itemLst.at(0).cursorSourceAction->setChecked(true);
        selectCursorSourceMenu.actions().at(0)->setChecked(true);

        QCPAxis *axis = itemLst.at(0).amplitudeAxis;
        plotCursors->setYAxis(axis);
        plotCursors->setAmpScan(itemLst.at(0).channelStruct->ampScan);
        plotCursors->setSourceChannel(itemLst.at(0).channelStruct);
        plotCursors->resetCursors();
        if(!itemLst.at(0).channelStruct->enabled)
        {
            showCursorAction->setChecked(false);
            showCursorAction->setEnabled(false);
        }
    }
}

void SpectrPlotForm::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        spectrPlot->xAxis->setLabel(tr("Frequency (kHz)"));
        spectrPlot->yAxis->setLabel(tr("Amplitude"));

        windowTypeMenu.setTitle(tr("Window type"));
        rectWindow->setText(tr("Rect"));
        hammingWindow->setText(tr("Hamming"));
        hannWindow->setText(tr("Hann"));
        nuatallWindow->setText(tr("Nuatall"));
        flatTopWindow->setText(tr("Flat"));
        scaleTypeMenu.setTitle(tr("Scale type"));
        linearScale->setText(tr("Linear"));
        dbmScale->setText(tr("dbm"));
        dbiScale->setText(tr("dbi"));
        showCursorAction->setText(tr("Show cursor"));
        selectCursorSourceMenu.setTitle(tr("Cursor source"));
        resetCursorAction->setText(tr("Reset cursors"));
        notDeviceWarningText->setText(tr("Device not connected !"));

        for(int i(0); i < itemLst.size(); i++)
        {
            QString id = QString(itemLst.at(i).channelStruct->id);
            itemLst.at(i).cursorSourceAction->setText(tr("Channel ") + id);
        }
    }
    else
        QWidget::changeEvent(event);
}

void SpectrPlotForm::setEnabled(bool en)
{
    bool allChannelDisable = true;
    QWidget::setEnabled(en);

    for(int i(0); i < itemLst.size(); i++)
    {
        spectrPlot->graph(i)->setVisible(en);
        itemLst.at(i).amplitudeAxis->grid()->setVisible(en);
        if(itemLst.at(i).channelStruct->enabled)
        {
            allChannelDisable = false;
            break;
        }
    }

    if(allChannelDisable)
        itemLst.at(0).amplitudeAxis->setVisible(true);

    notDeviceWarningText->setVisible(!en);
    notDeviceWarningText->setText(tr("Device not connected !"));
    spectrPlot->xAxis->grid()->setVisible(en);
    itemLst.at(0).amplitudeAxis->grid()->setVisible(en);
    if(!en)
    {
        for(int i(0); i < itemLst.size(); i++)
            spectrPlot->graph(i)->clearData();
        plotCursors->setVisible(false);
        spectrPlot->replot();
    }
}

void SpectrPlotForm::visibleCursors(bool show)
{
    showCursorAction->setChecked(show);
}

bool SpectrPlotForm::isCursorVisible()
{
    return plotCursors->isVisible();
}
