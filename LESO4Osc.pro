#-------------------------------------------------
#
# Project created by QtCreator 2015-06-27T12:30:11
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LESOscope
TEMPLATE = app
CONFIG += thread

RC_FILE += ./windows_resource.rc

SOURCES += main.cpp\
    ui/lesomainwindow.cpp \
    lib/qcustomplot.cpp \
    ui/toolbar/settingstoolbar.cpp \
    ui/toolbar/channeltoolbar.cpp \
    plotting/spectrplot/spectrplotform.cpp \
    plotting/signalplot/signalplotform.cpp \
    ui/lesotabwidget.cpp \
    ui/toolbar/playtoolbar.cpp \
    base/signalsource.cpp \
    ui/connectdevicedialog.cpp \
    ui/toolbar/triggertoolbar.cpp \
    ui/channelpopupwidget.cpp \
    base/measurewidget.cpp \
    ui/editmeasuredialog.cpp \
    base/common_func.cpp \
    plotting/qcpcursor.cpp \
    plotting/signalplot/signalplotthread.cpp \
    plotting/spectrplot/spectrplotthread.cpp \
    plotting/signalplot/signalplotcursor.cpp \
    plotting/spectrplot/spectrplotcursor.cpp \
    lesoaboutdialog.cpp

HEADERS  += ui/lesomainwindow.h \
    lib/qcustomplot.h \
    ui/toolbar/settingstoolbar.h \
    ui/toolbar/channeltoolbar.h \
    plotting/spectrplot/spectrplotform.h \
    plotting/signalplot/signalplotform.h \
    ui/lesotabwidget.h \
    ui/toolbar/playtoolbar.h \
    include/osc_global.h \
    base/signalsource.h \
    ui/connectdevicedialog.h \
    ui/toolbar/triggertoolbar.h \
    ui/channelpopupwidget.h \
    base/measurewidget.h \
    ui/editmeasuredialog.h \
    plotting/diagramgrid.h \
    base/common_func.h \
    lib/spline.h \
    plotting/qcpcursor.h \
    plotting/signalplot/signalplotthread.h \
    plotting/signalplot/signalplot.h \
    plotting/spectrplot/spectrplotthread.h \
    plotting/spectrplot/spectrplot.h \
    plotting/signalplot/signalplotcursor.h \
    plotting/spectrplot/spectrplotcursor.h \
    include/ui_config.h \
    lesoaboutdialog.h \
    lib/fftw3.h

FORMS    += ui/lesomainwindow.ui \
    ui/connectdevicedialog.ui \
    ui/editmeasuredialog.ui

RESOURCES += \
    leso4_resource.qrc

INCLUDEPATH += ./lib/leso4_api/

win32:LIBS += ./libLESO4.dll
win32:LIBS += ./libfftw3-3.dll
linux:LIBS += ./libLESO4.so
#linux:LIBS += ./libfftw3-3
linux:LIBS += /usr/lib/x86_64-linux-gnu/libfftw3.so.3
#linux:LIBS += -lLESO4

OTHER_FILES += \
    about_en.html \
    about_ru.html
