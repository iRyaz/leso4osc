/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "measurewidget.h"

MeasureWidget::MeasureWidget()
{
    setLayout(&layout);

    measureSettings = new QSettings(MEASURE_SETTINGS_FILE, QSettings::IniFormat);
    table = new QTableWidget;
    table->setColumnCount(6);
    QStringList m_TableHeader;
    m_TableHeader << "N" << tr("Channel") << tr("Parameter")
                  << tr("Value") << tr("Capture num") << tr("Comment");
    table->setHorizontalHeaderLabels(m_TableHeader);

    table->horizontalHeader()->resizeSections(QHeaderView::Stretch);
    table->setColumnWidth(5, width());
    table->verticalHeader()->setVisible(false);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setContextMenuPolicy(Qt::CustomContextMenu);
    layout.addWidget(table);
    setFloating(false);
    setWidget(table);
    setWindowTitle(tr("Measurements"));
    setAllowedAreas(Qt::BottomDockWidgetArea);
    setFeatures(QDockWidget::DockWidgetClosable);

    addMeasureAction = new QAction(tr("Add measurement"), this);
    editMeasureAction = new QAction(tr("Edit measurement"), this);
    removeMeasureAction = new QAction(tr("Remove measurement"), this);
    tableMenu.addAction(addMeasureAction);
    tableMenu.addAction(editMeasureAction);
    tableMenu.addAction(removeMeasureAction);

    connect(table, SIGNAL(cellClicked(int,int)), this, SLOT(cellChangeSlot(int,int)));
    connect(table, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showMenu(QPoint)));
    connect(removeMeasureAction, SIGNAL(triggered()), this, SLOT(deleteSelectMeasure()));

    selectRow = 0;
}

MeasureWidget::~MeasureWidget()
{
    measureSettings->clear();
    QList<MEASURE_t> *lst = getMeasureList();
    for(int i(0); i < lst->size(); i++)
    {
        measureSettings->beginGroup("Measure-" + QString::number(i));
        measureSettings->setValue("Channel", lst->at(i).channel->channelNum);
        measureSettings->setValue("Type", lst->at(i).type);
        measureSettings->setValue("CaptureNum", lst->at(i).captureNum);
        measureSettings->setValue("Comment", lst->at(i).comment);
        measureSettings->endGroup();
    }

    measureSettings->sync();
}

void MeasureWidget::loadSettings()
{
    QStringList groups = measureSettings->childGroups();
    if(groups.empty())
        return;

    for(int i(0); i < groups.size(); i++)
    {
        MEASURE_t newMeasure;
        measureSettings->beginGroup(groups.at(i));
        newMeasure.type = (MEASURE_TYPE)measureSettings->value("Type").toInt();
        newMeasure.captureNum = measureSettings->value("CaptureNum").toInt();
        newMeasure.comment = measureSettings->value("Comment").toString();
        int channelIndex = measureSettings->value("Channel").toInt();
        if(channelIndex > channelsLst.size()-1)
        {
            qDebug() << "MeasureWidget: Channels not init";
            return;
        }
        newMeasure.channel = channelsLst.at(channelIndex);
        measureSettings->endGroup();
        addMeasure(newMeasure);
    }

    updateTable();
}

void MeasureWidget::addChannel(MEASURE_CHANNEL_t *channel)
{
    channelsLst.append(channel);
}

void MeasureWidget::setFrequencyMeasure(int channel_num, double freq)
{
    for(int i(0); i < measureLst.size(); i++)
    {
        MEASURE_t currentMeasure = measureLst.at(i);
        if(currentMeasure.channel->channelNum == channel_num && currentMeasure.type == FREQUENCY)
            showMeasure(i, currentMeasure.channel->enabled, getFrequency(freq, 3));
    }
}

void MeasureWidget::update()
{
    for(int i(0); i < measureLst.size(); i++)
    {
        MEASURE_t currentMeasure = measureLst.at(i);

        if(!currentMeasure.channel->enabled)
        {
            showMeasure(i, false, "-");
            continue;
        }

        double *currentSamples = currentMeasure.channel->samplePtr;
        int samplesSize = currentMeasure.channel->sampleBufferSize;

        AMP_MEASURE amp = calculateAmp(currentSamples, samplesSize);

        switch(currentMeasure.type)
        {
        case AMPLITUDE:
            middleCaptureNum(i, currentMeasure, amp.amp, tr(" V"));
            break;
        case MIN_VALUE:
            middleCaptureNum(i, currentMeasure, amp.min, tr(" V"));
            break;
        case MAX_VALUE:
            middleCaptureNum(i, currentMeasure, amp.max, tr(" V"));
            break;
        case MIDDLE_VALUE:
            middleCaptureNum(i, currentMeasure, amp.middleValue, tr(" V"));
            break;
        case MIDDLE_QUAD_VALUE: {
            double quad = calculateMiddleQuad(currentSamples, samplesSize, amp.middleValue);
            middleCaptureNum(i, currentMeasure, quad, tr(" V"));
            }
            break;
        case FREQUENCY: {
            if(!currentMeasure.channel->enabled)
                showMeasure(i, currentMeasure.channel->enabled, 0);
            }
            break;
        }
        measureLst.replace(i, currentMeasure);
    }
}

void MeasureWidget::middleCaptureNum(int measureNum, MEASURE_t &currentMeasure, double measureVal, QString prefix)
{
    if(currentMeasure.captureCount < currentMeasure.captureNum)
    {
        currentMeasure.captureCount++;
        currentMeasure.currentVal += measureVal;
    } else {
        currentMeasure.captureCount = 0;
        showMeasure(measureNum, currentMeasure.channel->enabled,
            QString::number(digitRound(currentMeasure.currentVal/(currentMeasure.captureNum),
                                       currentMeasure.channel)) + prefix);
        currentMeasure.currentVal = 0;
    }
}

void MeasureWidget::showMeasure(int measureNum, bool en, double val)
{
    if(en)
        table->item(measureNum, VALUE_NUM_ROW)->setText(QString::number(val));
    else
        table->item(measureNum, VALUE_NUM_ROW)->setText("-");
}

void MeasureWidget::showMeasure(int measureNum, bool en, QString textVal)
{
    if(en)
        table->item(measureNum, VALUE_NUM_ROW)->setText(textVal);
    else
        table->item(measureNum, VALUE_NUM_ROW)->setText("-");
}

void MeasureWidget::addMeasure(MEASURE_t measure)
{
    table->insertRow(measureLst.size());
    int currentRow = measureLst.size();
    int channel_color = ((measure.channel->color)&0xFFFFFF00)>>8;
    table->setItem(currentRow, NUMBER_ROW, new QTableWidgetItem(QString::number(currentRow+1)));
    table->setItem(currentRow, CHANNEL_ROW, new QTableWidgetItem(getChannelStr(measure.channel)));
    table->setItem(currentRow, MEASURE_TYPE_ROW, new QTableWidgetItem(getMeasureTypeStr(measure.type)));
    table->setItem(currentRow, VALUE_NUM_ROW, new QTableWidgetItem(QString::number(0)));
    table->setItem(currentRow, CAPTURE_ROW, new QTableWidgetItem(QString::number(measure.captureNum)));
    table->setItem(currentRow, COMMENT_ROW, new QTableWidgetItem(measure.comment));

    table->item(currentRow, NUMBER_ROW)->setTextColor(channel_color);
    table->item(currentRow, CHANNEL_ROW)->setTextColor(channel_color);
    table->item(currentRow, MEASURE_TYPE_ROW)->setTextColor(channel_color);
    table->item(currentRow, VALUE_NUM_ROW)->setTextColor(channel_color);
    table->item(currentRow, CAPTURE_ROW)->setTextColor(channel_color);
    table->item(currentRow, COMMENT_ROW)->setTextColor(channel_color);

    measureLst.append(measure);
}

void MeasureWidget::updateTable()
{
    for(int i(0); i < measureLst.size(); i++)
    {
        MEASURE_t measure = measureLst.at(i);
        int channel_color = ((measureLst.at(i).channel->color)&0xFFFFFF00)>>8;
        measure.captureCount = 0;
        table->item(i, CHANNEL_ROW)->setText(getChannelStr(measure.channel));
        table->item(i, MEASURE_TYPE_ROW)->setText(getMeasureTypeStr(measure.type));
        table->item(i, CAPTURE_ROW)->setText(QString::number(measure.captureNum));
        table->item(i, COMMENT_ROW)->setText(measure.comment);

        // Update Row Color
        table->item(i, NUMBER_ROW)->setTextColor(channel_color);
        table->item(i, CHANNEL_ROW)->setTextColor(channel_color);
        table->item(i, MEASURE_TYPE_ROW)->setTextColor(channel_color);
        table->item(i, VALUE_NUM_ROW)->setTextColor(channel_color);
        table->item(i, CAPTURE_ROW)->setTextColor(channel_color);
        table->item(i, COMMENT_ROW)->setTextColor(channel_color);
    }

    table->resizeColumnsToContents();
}

AMP_MEASURE MeasureWidget::calculateAmp(double *samplesPtr, int size)
{
    AMP_MEASURE ret;
    double middle = 0.0f;
    double min = 0, max = 0;
    for(int i(BUFFER_OFFSET); i < size; i++)
    {
        if(samplesPtr[i] > max)
            max = samplesPtr[i];
        if(samplesPtr[i] < min)
            min = samplesPtr[i];

        middle += samplesPtr[i];
    }

    ret.amp = max - min;
    ret.max = max;
    ret.min = min;
    ret.middleValue = middle/size;

    return ret;
}

double MeasureWidget::calculateMiddleQuad(double *samplePtr, int size, double middle)
{
    double ret = 0;

    for(int i(BUFFER_OFFSET); i < size; i++)
    {
        ret += std::pow(samplePtr[i] - middle, 2);
    }

    if(ret < 0)
        ret *= -1;

    return std::sqrt(ret/size);
}

void MeasureWidget::setDecimFactor(int decim)
{
    sampleFrequency = BASE_FREQUENCY/decim;
}

void MeasureWidget::cellChangeSlot(int row, int column)
{
    Q_UNUSED(column)
    selectRow = row;
}

void MeasureWidget::deleteSelectMeasure()
{
    measureLst.removeAt(getSelectMeasureNum());
    table->removeRow(getSelectMeasureNum());
}

void MeasureWidget::showMenu(const QPoint &p)
{
    tableMenu.popup(table->viewport()->mapToGlobal(p));
}

void MeasureWidget::resizeEvent(QResizeEvent *event)
{
    table->setColumnWidth(5, event->size().width()/3);
}

void MeasureWidget::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        addMeasureAction->setText(tr("Add measurement"));
        editMeasureAction->setText(tr("Edit measurement"));
        removeMeasureAction->setText(tr("Remove measurement"));

        table->horizontalHeaderItem(1)->setText(tr("Channel"));
        table->horizontalHeaderItem(2)->setText(tr("Parameter"));
        table->horizontalHeaderItem(3)->setText(tr("Value"));
        table->horizontalHeaderItem(4)->setText(tr("Capture num"));
        table->horizontalHeaderItem(5)->setText(tr("Comment"));

        for(int i(0); i < measureLst.size(); i++)
        {
            MEASURE_t measure = measureLst.at(i);
            measure.captureCount = 0;
            table->item(i, MEASURE_TYPE_ROW)->setText(getMeasureTypeStr(measure.type));
        }

        table->resizeColumnsToContents();
    }
    else
        QWidget::changeEvent(event);
}
