/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef COMMON_FUNC
#define COMMON_FUNC

#include <QObject>
#include <QEvent>
#include <QKeyEvent>
#include <QString>
#include <cmath>

#include "include/osc_global.h"

#define PI 3.14159265358979323846264338f // ПИ

double module(double x); // Модуль числа
double ceil(double x, AMP_SCAN_t amp); // Округление значения амплитуды
double digitRound(double val, MEASURE_CHANNEL_t *ch);

QString getTime(double time);
QString getFrequency(double freq, int ceil_c = 1);
QString getChannelStr(MEASURE_CHANNEL_t *ch);
QString getMeasureTypeStr(MEASURE_TYPE type);
AMP_DIMENSION getAmplitudeDimension(double amp);
QString getAmplitudeDimensionStr(AMP_DIMENSION dimension);

// Функция заглушка
// Вызывается в обработчике событий виджета
// Нужна чтобы игнорировать нажатие определенных в функции кнопок
bool eventKeyStub(QObject *target, QEvent *event);

#endif // COMMON_FUNC
