
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "common_func.h"

double module(double x)
{
    if(x < 0)
        return -1*x;
    return x;
}

double ceil(double x, AMP_SCAN_t amp)
{
    double ceil_coeff = 10;

    switch(amp)
    {
    case AMP_SCAN_1_500: ceil_coeff = 10; break;
    case AMP_SCAN_1_200: ceil_coeff = 10; break;
    case AMP_SCAN_1_100: ceil_coeff = 10; break;
    case AMP_SCAN_1_50: ceil_coeff  = 100; break;
    case AMP_SCAN_1_20: ceil_coeff = 100; break;
    case AMP_SCAN_1_10: ceil_coeff = 100; break;
    case AMP_SCAN_1_5: ceil_coeff = 1000; break;
    case AMP_SCAN_1_2: ceil_coeff = 1000; break;
    case AMP_SCAN_1_1: ceil_coeff = 1000; break;
    }

    return std::ceil(x*ceil_coeff)/ceil_coeff;
}

QString getTime(double time)
{
    QString ret;

    if(time < 0.09 )
        ret = QString::number(time *  1000) + " " + QObject::tr("ms");
    if(time < 0.001)
        ret = QString::number(time * 1000000) + " " + QObject::tr("us");

    return ret;
}

QString getFrequency(double freq, int ceil_c)
{
    QString ret;
    double c = std::pow(10, ceil_c);

    ret = QString::number(ceil(freq*c)/c) + " " + QObject::tr("Hz");

    if(freq > 500)
    {
        ret = QString::number(ceil(c*freq/1000)/c) + " " + QObject::tr("kHz");
    }
    if(freq > 500000)
    {
        ret = QString::number(ceil(c*freq/1000000)/c) + " " + QObject::tr("MHz");
    }

    return ret;
}

QString getChannelStr(MEASURE_CHANNEL_t *ch)
{
    QString ret;
    switch(ch->channelNum)
    {
    case CH_A:
        ret = QObject::tr("Channel A");
        break;
    case CH_B:
        ret = QObject::tr("Channel B");
        break;
    case CH_C:
        ret = QObject::tr("Channel C");
        break;
    case CH_D:
        ret = QObject::tr("Channel D");
        break;
    default:
        ret = QObject::tr("Channel A");
    }

    return ret;
}

QString getMeasureTypeStr(MEASURE_TYPE type)
{
    QString ret;

    switch(type)
    {
    case AMPLITUDE:
        ret = QObject::tr("Amplitude");
        break;
    case  MIN_VALUE:
        ret = QObject::tr("Minimum value");
        break;
    case MAX_VALUE:
        ret = QObject::tr("Maximin value");
        break;
    case MIDDLE_VALUE:
        ret = QObject::tr("Middle value");
        break;
    case MIDDLE_QUAD_VALUE:
        ret = QObject::tr("Middle quad value");
        break;
    case FREQUENCY:
        ret = QObject::tr("Frequency");
        break;

    default:
        ret = QObject::tr("Amplitude");
    }

    return ret;
}

AMP_DIMENSION getAmplitudeDimension(double amp)
{
    if(amp < 0.0001f)
        return uV;
    if(amp < 0.1f)
        return mV;
    if(amp < 1.0f)
        return mV;
    return V;
}

QString getAmplitudeDimensionStr(AMP_DIMENSION dimension)
{
    QString ret;
    switch(dimension)
    {
    case V: ret = QObject::tr("V"); break;
    case mV: ret = QObject::tr("mV"); break;
    case uV: ret = QObject::tr("uV"); break;
    }

    return ret;
}

double digitRound(double val, MEASURE_CHANNEL_t *ch)
{
    Q_UNUSED(ch)
    /*double ceil_coeff = 10;

    switch(ch->ampScan)
    {
    case AMP_SCAN_1_500: ceil_coeff = 10; break;
    case AMP_SCAN_1_200: ceil_coeff = 10; break;
    case AMP_SCAN_1_100: ceil_coeff = 10; break;
    case AMP_SCAN_1_50: ceil_coeff  = 100; break;
    case AMP_SCAN_1_20: ceil_coeff = 100; break;
    case AMP_SCAN_1_10: ceil_coeff = 100; break;
    case AMP_SCAN_1_5: ceil_coeff = 1000; break;
    case AMP_SCAN_1_2: ceil_coeff = 1000; break;
    case AMP_SCAN_1_1: ceil_coeff = 1000; break;
    }*/

    return std::ceil(val * 100)/100;
}

bool eventKeyStub(QObject *target, QEvent *event)
{
    Q_UNUSED(target)
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = (QKeyEvent*)event;
        if(keyEvent->key() == Qt::Key_Space)
        {
            event->ignore();
            return true;
        }

        if(keyEvent->key() == Qt::Key_K)
        {
            event->ignore();
            return true;
        }
    }

    return false;
}
