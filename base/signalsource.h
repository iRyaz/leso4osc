/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef SIGNALSOURCE_H
#define SIGNALSOURCE_H

#include <QDebug>
#include <QThread>
#include <QObject>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include "include/osc_global.h"
#include "lib/leso4.h"
#include "common_func.h"

using namespace std;

#define LESO_DESCRIPTOR "LESO4.1_ER"

/*
 * Увеличение этих интервалов, позволяет снизить загрузку CPU,
 * но увеличивает время обновления графиков
*/
#define DEMO_MODE_DELAY_MS 50      // Интервал обновления экрана в демо режиме (в милисекундах)
#define TIME_UPDATE_SCOPE_MS 50    // Интервал обновления экрана, без учета задержки самого прибора (в милисекундах)

#define SAMPLES_BYTE_OFFSET 45

#define FREQ_A 700000   // Частота сигнала канала A в демо режиме
#define FREQ_B 550000   // Частота сигнала канала A в демо режиме
#define FREQ_C 420000   // Частота сигнала канала A в демо режиме
#define FREQ_D 100000   // Частота сигнала канала A в демо режиме

class SignalSource : public QThread
{
    Q_OBJECT

    SIGNAL_SOURCE_TYPE deviceType;  // Тип ус-ва
    DEVICE_VERSION dev_version;     // Ревизия
    QString deviceDescriptor;

    int rxBytes;                    // Фактическое количество прочитанных данных
    bool initFlag;                  // Девайс инициализирован ?

    int samplesNum;                 // Количество отсчетов на канал
    int samplesNumDevice;
    sample_frequency DeviceDecimFactor; // Частота дискретизации сигнала

    double *ABufferPtr;     // Указатель на буфер канала A
    double *BBufferPtr;     // Указатель на буфер канала B
    double *CBufferPtr;     // Указатель на буфер канала C
    double *DBufferPtr;     // Указатель на буфер канала D

    bool dataReceiveFlag;   // Флаг приема данных
    bool stopReceiveFlag;   // Флаг разрешение приема
    void run() Q_DECL_OVERRIDE;     // Вызывается при запуске потока

    double rand_phase;      // Фаза сигнала для демо режима

public:

    enum SS_STATUS {SS_FAULT = -1, SS_OK = 1};

    SignalSource();
    ~SignalSource();

    DEVICE_VERSION version() { return dev_version; }
    int getSamplesNum() { return samplesNum; }
    bool isDemoMode() { return deviceDescriptor == DEMO_MODE_DESCRIPTOR; }
    bool isInit() { return initFlag; }
    void setSamplesFrequncy(sample_frequency time_scan);
    QString getDescriptor() { return deviceDescriptor; }
    SS_STATUS init(QString descriptor);
    SS_STATUS setPointNum(int pointNum);
    SS_STATUS setAmplitudeScan(CHANNEL_t ch, AMP_SCAN_t ampScan);
    SS_STATUS readSamplesBuffer();
    SS_STATUS toggleChannel(CHANNEL_t ch, bool en);
    SS_STATUS setCloseInput(CHANNEL_t channel, bool closed);

    double *getSamplesA();
    double *getSamplesB();
    double *getSamplesC();
    double *getSamplesD();
    double *getDataPtr(CHANNEL_t ch);
    int getChannelBufferSize();
    int getCurrentDecimFactor();
    int getRXBytes() { return rxBytes; }
    void closeDevice();
    int getChannelsNum() { return CHANNEL_NUM; }

    friend class ConnectDeviceDialog;
    friend class LESOMainWindow;
};

#endif // SIGNALSOURCE_H
