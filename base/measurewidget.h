/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef MEASUREWIDGET_H
#define MEASUREWIDGET_H

#include <QDockWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTableWidget>
#include <QHeaderView>
#include <QApplication>
#include <QDesktopWidget>
#include <QList>
#include <QEvent>
#include <QDebug>
#include <cmath>
#include <QMenu>
#include <QAction>
#include <QSettings>
#include <QFile>
#include <QResizeEvent>
#include "include/osc_global.h"
#include "common_func.h"

#define NUMBER_ROW 0
#define CHANNEL_ROW 1
#define MEASURE_TYPE_ROW 2
#define VALUE_NUM_ROW 3
#define CAPTURE_ROW 4
#define COMMENT_ROW 5

#define MEASURE_SETTINGS_FILE "settings/measures.settings"  // Файл для сохранения измерений

typedef QList<MEASURE_CHANNEL_t*> CHANNELS_LST;

/*
 * Структура представляющая измеренися в таблице
*/
struct MEASURE_t
{
    MEASURE_t() { captureCount = 0; currentVal = 0; }
    MEASURE_CHANNEL_t *channel; // Указатель на канал
    MEASURE_TYPE type;          // Тип измерений
    int captureNum;             // Количество захватов
    QString comment;            // Коментарии

private:
    int captureCount;           // Счетчик захватов
    double currentVal;          // Текущее значения измеряемой величины
    friend class MeasureWidget;
};

/*
 * Структура для хранения значений амплитуды
 * Эти параметры можно вычислять вызовом одной функции
*/
struct AMP_MEASURE
{
    double max; // Максимальная амплитудв
    double min; // Минимальная амплитуда
    double amp; // Текущая амплитуда
    double middleValue; // Средннее значение амплитуды
};

/*
 * Таблица измерений, на основе класса QDockWidget
 * В классе также выполняются рсчеты: Амплитуды,
 * Максимального и минимального значения, среднего значения,
 * среднеквадратичного. Расчет частоты выполняется уже в другом классе,
 * который для того чтобы добавить измеренное значение в таблицу, должен быть подключен
 * к слоту void setFrequencyMeasure(int channel_num, double freq)
*/
class MeasureWidget : public QDockWidget
{
    Q_OBJECT

    QSettings *measureSettings;
    double sampleFrequency;
    QVBoxLayout layout;
    QTableWidget *table;
    CHANNELS_LST channelsLst;
    QList<MEASURE_t> measureLst;

    /*
     * Расчет амплитуды сигнала по отсчетам
     * samplesPtr - указатель на отсчеты сигнала
     * size - размер отсчетов
    */
    AMP_MEASURE calculateAmp(double *samplesPtr, int size);

    /*
     * Расчет Средне квадратичного значения сигнала по отсчетам
     * samplesPtr - указатель на отсчеты сигнала
     * size - размер отсчетов
     * middle - Уже вычисленное среднее значение отсчетов этого сигнала
    */
    double calculateMiddleQuad(double *samplePtr, int size, double middle);
    //double calculateFrequency(double *samplePtr, int size);
    void showMeasure(int measureNum, bool en, double val);
    void showMeasure(int measureNum, bool en, QString textVal);

    /*
     * Сделать одну итерацию усреднения (Число итераций указывается в параметрах измерения)
     * Если итерация последняя, то измеряемое значение делится на количество итераций и сохраняется в структуре
     * measureNum - номер измерения
     * currentMeasure - параметры измерения
     * measureVal - измеренное значение
     * postfix - размерность , которая идет после числа и ее следует показать в таблице (Вольт, Частота)
    */
    void middleCaptureNum(int measureNum, MEASURE_t &currentMeasure, double measureVal, QString postfix);

    int selectRow;  // Выбранное измерение

    QMenu tableMenu;
    QAction *addMeasureAction;
    QAction *editMeasureAction;
    QAction *removeMeasureAction;

protected:
    void changeEvent(QEvent *event);

public:
    explicit MeasureWidget();
    ~MeasureWidget();

    void loadSettings();    // Загрузить список измерений из класса настроек QSettings
    void addChannel(MEASURE_CHANNEL_t *channel);    // Добавить канал измерения устройства
    void update();  // Обновить измеряемые значения в таблице
    void addMeasure(MEASURE_t measure); // Добавить измерения в таблицу
    CHANNELS_LST *getChannels() { return &channelsLst; }    // Возвратить список каналов измерения
    int getSelectMeasureNum() { return selectRow; } // Возвратить номер измерения, которое выбрано мышкой в таблице
    QList<MEASURE_t> *getMeasureList() { return &measureLst; }  // Возвратить список измерений
    void updateTable(); // Обновить всю таблицу, вызывается после добавления нового измерения
    QAction *getAddMeasureAction() { return addMeasureAction; } // Действий в выпадающем меню (Добавить измерения)
    QAction *getEditMeasureAction() { return editMeasureAction; }// Действий в выпадающем меню (Редактировать измерения)
    QAction *getRemoveMeasureAction() { return removeMeasureAction; }// Действий в выпадающем меню (Удалить измерения)

signals:

public slots:
    /*
     * Установить коэффициент децимации
     * (Частота дискретизации сигнала = Базовая частота / коэффициент децимации)
     * Необходим для выполнения измерений
    */
    void setDecimFactor(int decim);
    void cellChangeSlot(int row, int column);
    void deleteSelectMeasure();
    void showMenu(const QPoint& p); // Показать меню
    /*
     * Установть значение частоты для определенного канала
     * Частота сигнала рассчитывается в другом классе
    */
    void setFrequencyMeasure(int channel_num, double freq);

protected:
    void resizeEvent(QResizeEvent *event);
};

#endif // MEASUREWIDGET_H
