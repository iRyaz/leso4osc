/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#include "signalsource.h"

SignalSource::SignalSource()
{
    rxBytes = 0;
    initFlag = false;
    samplesNum = MAX_CHANNEL_BUFFER_SIZE;
    samplesNumDevice = MAX_CHANNEL_BUFFER_SIZE;
    DeviceDecimFactor = sampe_frequency_250kHz;
    ABufferPtr = new double[MAX_CHANNEL_BUFFER_SIZE];
    BBufferPtr = new double[MAX_CHANNEL_BUFFER_SIZE];
    CBufferPtr = new double[MAX_CHANNEL_BUFFER_SIZE];
    DBufferPtr = new double[MAX_CHANNEL_BUFFER_SIZE];
    rand_phase = 360;
}

SignalSource::~SignalSource()
{

}

SignalSource::SS_STATUS SignalSource::init(QString descriptor)
{
    SS_STATUS ret = SS_FAULT;
    deviceDescriptor = descriptor;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        initFlag = true;
        ret = SS_OK;
    }
    else
    {
        if(deviceDescriptor == "LESO4.1_ER")
        {
            if(leso4IsOpen() == true)
            {
                ret = SS_OK;
                initFlag = true;
                dev_version = leso4GetVersion();
            }
            else
            {

                if(leso4Open("LESO4.1_ER") == LESO4_OK)
                {
                    ret = SS_OK;
                    initFlag = true;
                    dev_version = leso4GetVersion();
                }
                else
                {
                    ret = SS_FAULT;
                    initFlag = false;
                }
            }
        }
    }

    return ret;
}

SignalSource::SS_STATUS SignalSource::setPointNum(int pointNum)
{
    SS_STATUS ret = SS_OK;
    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        samplesNum = pointNum;
    }
    else
    {
        samplesNumDevice = pointNum;
    }

    return ret;
}

void SignalSource::setSamplesFrequncy(sample_frequency time_scan)
{
    DeviceDecimFactor = time_scan;
}

SignalSource::SS_STATUS SignalSource::setAmplitudeScan(CHANNEL_t ch, AMP_SCAN_t ampScan)
{
    SS_STATUS ret = SS_FAULT;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = SS_OK;
    }
    else
    {
        max_voltage lesoAmpScanIndex = max_voltage_20V;
        switch(ampScan)
        {
        case AMP_SCAN_1_500: lesoAmpScanIndex = max_voltage_20V; break;
        case AMP_SCAN_1_200: lesoAmpScanIndex = max_voltage_8V; break;
        case AMP_SCAN_1_100: lesoAmpScanIndex = max_voltage_4V; break;
        case AMP_SCAN_1_50: lesoAmpScanIndex = max_voltage_2V; break;
        case AMP_SCAN_1_20: lesoAmpScanIndex = max_voltage_800mV; break;
        case AMP_SCAN_1_10: lesoAmpScanIndex = max_voltage_400mV; break;
        case AMP_SCAN_1_5: lesoAmpScanIndex = max_voltage_200mV; break;
        case AMP_SCAN_1_2: lesoAmpScanIndex = max_voltage_80mV; break;
        case AMP_SCAN_1_1: lesoAmpScanIndex = max_voltage_40mV; break;
        }

        if(leso4SetAmplitudeScan((int)ch, lesoAmpScanIndex) == LESO4_OK)
            ret = SS_OK;
    }

    return ret;
}

SignalSource::SS_STATUS SignalSource::readSamplesBuffer()
{
    SS_STATUS ret = SS_FAULT;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        rand_phase -= 36;
        double s_freq = BASE_FREQUENCY/DeviceDecimFactor;
        double s_time = 1/s_freq;

        for(int i(0); i < samplesNum; i++)
        {
            ABufferPtr[i] = 3 * sin(s_time*i*2*PI*FREQ_A + rand_phase * PI/180);
            BBufferPtr[i] = 0.3 * cos(s_time*i*2*PI*FREQ_B + rand_phase * PI/180);
            CBufferPtr[i] = 0.8 * sin(s_time*i*2*PI*FREQ_C + rand_phase * PI/180);
            DBufferPtr[i] = 0.04 * cos(s_time*i*2*PI*FREQ_D + rand_phase * PI/180);
        }

        ret = SS_OK;
        rxBytes = samplesNum *2*4;
    }
    else
    {
        rxBytes = leso4ReadFIFO();
        if(leso4GetLastErrorCode() == NO_ERROR_CODE)
        {
            ret = SS_OK;
        }
        else
            rxBytes = 0;
    }

    return ret;
}

SignalSource::SS_STATUS SignalSource::toggleChannel(CHANNEL_t ch, bool en)
{
    SS_STATUS ret = SS_FAULT;

    int channel = CHANNEL_A;
    switch(ch)
    {
    case CH_A: channel = CHANNEL_A; break;
    case CH_B: channel = CHANNEL_B; break;
    case CH_C: channel = CHANNEL_C; break;
    case CH_D: channel = CHANNEL_D; break;
    }

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
    }
    else
    {
        if(!leso4IsOpen())
            return ret;

        if(en)
        {
            if(!leso4IsChannelEnable(channel))
            {
                if(leso4EnableChannel(channel) == LESO4_OK)
                {
                    ret = SS_OK;
                }
            }
        }
        else
        {
            if(leso4DisableChannel(channel) == LESO4_OK)
            {
                ret = SS_OK;
            }
            else
                rxBytes = 0;
        }
    }

    return ret;
}

SignalSource::SS_STATUS SignalSource::setCloseInput(CHANNEL_t channel, bool closed)
{
    SS_STATUS ret = SS_FAULT;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = SS_OK;
    }
    else
    {
        if(closed)
        {
            if(leso4SetInputMode(channel, input_mode_close) == LESO4_OK)
                ret = SS_OK;
        }
        else
        {
            if(leso4SetInputMode(channel, input_mode_open) == LESO4_OK)
                ret = SS_OK;
        }
    }

    return ret;
}

double *SignalSource::getSamplesA()
{
    double* ret;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = ABufferPtr;
    }
    else
    {
        ret = (double *)leso4GetData(NULL, CHANNEL_A);
    }

    return ret;
}

double *SignalSource::getSamplesB()
{
    double* ret;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = BBufferPtr;
    }
    else
    {
        ret = (double *)leso4GetData(NULL, CHANNEL_B);
    }

    return ret;
}

double *SignalSource::getSamplesC()
{
    double* ret;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = CBufferPtr;
    }
    else
    {
        ret = (double *)leso4GetData(NULL, CHANNEL_C);
    }

    return ret;
}

double *SignalSource::getSamplesD()
{
    double* ret;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = DBufferPtr;
    }
    else
    {
        ret = (double *)leso4GetData(NULL, CHANNEL_D);
    }

    return ret;
}

int SignalSource::getChannelBufferSize()
{
    int ret;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = samplesNum;
    }
    else
    {
        ret = leso4GetChannelFIFOSize();
    }

    return ret;
}

void SignalSource::closeDevice()
{
    initFlag = false;
    if(deviceDescriptor != DEMO_MODE_DESCRIPTOR)
        leso4CloseDevice();
}

int SignalSource::getCurrentDecimFactor()
{
    int ret = 0;

    if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
    {
        ret = DeviceDecimFactor;
    }
    else
    {
        ret = (int)leso4GetSamplFreq();
    }

    return ret;
}

double *SignalSource::getDataPtr(CHANNEL_t ch)
{
    if(ch >= getChannelsNum())
    {
        qDebug() << "Channel: " << ch << " incorrect !!!!!";
    }

    double *ret;
    switch(ch)
    {
    case CH_A:
        if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
            ret = ABufferPtr;
        else
            ret = (double *)leso4GetData(NULL, CHANNEL_A);
        break;
    case CH_B:
        if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
            ret = BBufferPtr;
        else
            ret = (double *)leso4GetData(NULL, CHANNEL_B);
        break;
    case CH_C:
        if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
            ret = CBufferPtr;
        else
            ret = (double *)leso4GetData(NULL, CHANNEL_C);
        break;
    case CH_D:
        if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
            ret = DBufferPtr;
        else
            ret = (double *)leso4GetData(NULL, CHANNEL_D);
        break;
	default: {
		if(deviceDescriptor == DEMO_MODE_DESCRIPTOR)
            ret = ABufferPtr;
        else
            ret = (double *)leso4GetData(NULL, CHANNEL_A);
		}
	}

    return ret;
}

/*
 * Вызывается функция потока run()
 * Идет чтение данных с устройства
 * Как только прочитано требуемое число отсчетов
 * то посылается сигнал readDataFinished()
*/
void SignalSource::run()
{
    rxBytes = 0;
    while(getRXBytes() != samplesNum*2*4)
    {
        if(isInit())
        {
            if(readSamplesBuffer() == SS_OK)
                initFlag = true;
            else
                initFlag = false;
        }
        else
            break;
    }

    if(deviceDescriptor != DEMO_MODE_DESCRIPTOR)
    {
        if(leso4IsOpen())
        {
            leso4SetSamplFreq(DeviceDecimFactor);   // Переключение временной развертки
            if(samplesNumDevice != samplesNum)
            {
                leso4SetSamplesNum(samplesNumDevice);
                samplesNum = samplesNumDevice;
            }
        }

        QThread::msleep(TIME_UPDATE_SCOPE_MS);  // Дополнительная задержка
        return;
    }

    QThread::msleep(DEMO_MODE_DELAY_MS);        // Задержка только в демо режиме
}
