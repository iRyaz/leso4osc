#ifndef UI_CONFIG_H
#define UI_CONFIG_H

#define TOOL_BAR_SPACING 10                 // Расстояние между элементами в QToolBar

#define TOOL_BARS_COLOR 0xECF0F1            // Цвет фона панелей инструментов
#define BASE_TIME_COLOR 0x912E6FFF          // Цвет элементов относящихся к временным параметрам

#define CHANNEL_A_COLOR 0xFF820A00            // Цвет элементов относящихся к каналу A
#define CHANNEL_B_COLOR 0x11CA3B00            // Цвет элементов относящихся к каналу B
#define CHANNEL_C_COLOR 0x16CAD300            // Цвет элементов относящихся к каналу C
#define CHANNEL_D_COLOR 0xFF000000            // Цвет элементов относящихся к каналу D

#define TRIGGER_LINE_WIDTH 2                // Толщина линии триггера
#define TRIGGER_LINE_QT_STYLE Qt::DotLine   // Стиль линии триггера
#define TRIGGER_LINE_COLOR 0xFF0000F0       // Цвет линии триггера

#define PLOT_BACKGROUND_COLOR 0x000000FF    // Фон графика
#define PLOT_ITEM_LINE_WIDTH 1              // Толщина линий осей и элементов графика

#define X_AXIS_LABEL_COLOR 0xFFFFFFFF       // Цвет текста название оси
#define X_AXIS_TICK_COLOR BASE_TIME_COLOR   // Цвет текста надписей значений осей

#define DEFAULT_GRID_COLOR 0x6C3A9A7C           // Цвет сетки
#define DEFAULT_GRID_PEN_WIDTH 1.0f             // Толщина сетки
#define DEFAULT_GRID_PEN_STYLE Qt::SolidLine    // Стиль линии сетки

#define DEFAULT_ZERO_LINE_GRID_WIDTH 2.0f           // Толщина нулевой линии
#define DEFAULT_ZERO_LINE_GRID_STYLE Qt::SolidLine  // Стиль нулевой линии

#define DEFAULT_SUB_GRID_COLOR 0x6C3A9A7C       // Цвет вспомогательной сетки
#define DEFAULT_SUB_GRID_WIDTH 0.01f            // Толщина вспомагательной сетки
#define DEFAULT_SUB_GRID_STYLE Qt::SolidLine    // Стиль вспомагательной сетки

#define CURSOR_LINE_WIDTH 0.7               // Толщина линий курсора
#define CURSOR_LINE_COLOR 0x7BF51FFF        // Цвет линий курсора
#define CURSOR_LINE_STYLE Qt::DashDotLine   // Стиль линий курсора

#define CURSOR_POINT_RADIUS 15              // Радиус точки пересечения линий
#define CURSOR_POINT_TRANSPARENT 30         // Прозрачность точки пересечения линий
#define CURSOR_POINT_PEN_WIDTH 2.0f         // Ширина линии обвода точки пересечения

#define CURSOR_TEXT_LABEL_COLOR 0x000000F0  // Цвет фона рамки текста курсоров

#define WARNING_LABEL_TEXT_SIZE 18          // Размер текста сообщения при неактивном графике
#define WARNING_RECT_LINE_WIDTH 1.5f        // Ширина линии рамки текта предупреждения
#define WARNING_COLOR_TEXT 0xFF0000FF       // Цвет текста сообщения при неактивном графике
#define WARNING_COLOR_RECT 0xFFFF00FF       // Цвет рамки текста сообщения при неактивном графике


#endif // UI_CONFIG_H

