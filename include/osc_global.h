
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef OSC_GLOBAL
#define OSC_GLOBAL

#define PROGRAM_VERSION "0.7.5"

#define DEMO_MODE_DESCRIPTOR "Virtual"
#define SETTINGS_DIR "settings"         // Директория для сохранения настроек

#define RECEIVE_TIMER_TIMEOUT 10        // Время таймера, который проверяет заполнение буфера

#define SCOPE_SIZE 200                  // Размер экрана по умолчания. Измеряется в точках

#define MAX_CHANNEL_BUFFER_SIZE 8192    // Максимальный размер буфера для одного канала

#define REPLOT_FLAG QCustomPlot::rpQueued

enum DIAGRAM_t { SIGNAL_DIAGRAM = 0, SPECTR_DIAGRAM };
enum CHANNEL_t { CH_A = 0, CH_B = 1, CH_C = 2, CH_D = 3 };

enum TRIGGER_MODE { NONE = 0, AUTO = 1 };
enum TRIGGER_EDGE_MODE { FALLING_EDGE = 0, RISING_EDGE = 1 };

enum GRAPH_STYLE { LINE_GRAPH_STYLE, POINT_GRAPH_STYLE };

enum FILE_FORMAT { PDF_FORMAT = 0, JPG_FORMAT, BMP_FORMAT, PNG_FORMAT };

enum MEASURE_TYPE
{
    AMPLITUDE = 0,                  // Амплитуда
    MIN_VALUE = 1,                  // Минимальное значение
    MAX_VALUE = 2,                  // Максимальное значение
    MIDDLE_VALUE = 3,               // Среднее значение отсчета
    MIDDLE_QUAD_VALUE = 4,          // Средне-квадратичное значение
    FREQUENCY = 5                   // Частота
};

enum AMP_DIMENSION { V = 1, mV = 1000, uV = 1000000 };

struct TRIGGER_t
{
    bool en;                        // Включить триггер
    TRIGGER_t() { en = false; mode = NONE; edge = RISING_EDGE; level = 0.0f; ch = CH_A; }
    TRIGGER_MODE mode;              // Режим триггера
    TRIGGER_EDGE_MODE edge;         // Фронт по которому идет синхронизация
    CHANNEL_t ch;                   // Номер канала
    double level;                   // Уровень триггера
};

typedef int DEVICE_VERSION;

enum SIGNAL_SOURCE_TYPE {VIRTUAL = 0, LESO_DEVICE = 1}; // Тип источника сигнала

enum AMP_SCAN_t                     // Развертка по амплитуде
{
    AMP_SCAN_1_500 = 0,
    AMP_SCAN_1_200,
    AMP_SCAN_1_100,
    AMP_SCAN_1_50,
    AMP_SCAN_1_20,
    AMP_SCAN_1_10,
    AMP_SCAN_1_5,
    AMP_SCAN_1_2,
    AMP_SCAN_1_1
};

struct MEASURE_CHANNEL_t
{
    MEASURE_CHANNEL_t()
    {
        ampScan = AMP_SCAN_1_500;
        enabled = false;
        inputMode = 0;
        offset = 0;
        amp = 1;
    }

    CHANNEL_t channelNum;   // Номер канала
    bool enabled;           // Канал включен ?
    AMP_SCAN_t ampScan;     // Развертка по времени
    int color;              // Цвет графика
    int inputMode;          // Режим входа (Открытый/Закрытый)
    double *samplePtr;      // Указатель на буфер
    int sampleBufferSize;   // Размер буфера
    char *id;               // Буква обозначающая канал
    char *ADCIconName;      // Иконка Обозначающая канал
    double offset;          // Смещение относительно нуля (Постоянная составляющая)
    double amp;             // Коэффициент усиления (Масштаб Y)
};

#define MAX_AMP_DIV_1_500 20.0
#define MAX_AMP_DIV_1_200 8.0f
#define MAX_AMP_DIV_1_100 4.0f
#define MAX_AMP_DIV_1_50 2.0f
#define MAX_AMP_DIV_1_20 0.8
#define MAX_AMP_DIV_1_10 0.4
#define MAX_AMP_DIV_1_5 0.2
#define MAX_AMP_DIV_1_2 0.08
#define MAX_AMP_DIV_1_1 0.04

#define BUFFER_OFFSET 45            // Смещение с которого начинается чтение отсчетов в буфере

#define BASE_FREQUENCY 50000000.0f  // Тактовая частота ПЛИС
#define FREQUENCY_FACTOR 10         // Коэффициент определяющий во сколько раз частота дискретизации больше частоты сигнала

#define BMP_FORMAT_FILTER "(*.bmp)"
#define JPG_FORMAT_FILTER "(*.jpg)"

#define US_COEFF 1000000 // Коэффициент отображение временной оси в микросекунды

#endif // OSC_GLOBAL

