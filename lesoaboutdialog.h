
/***************************************************************************
**                                                                        **
**                 LESO4Osc, virtual oscilloscope                         **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Ilya Ryasanov                                        **
**  Website/Contact: ryasanov@gmail.com                                   **
**             Date: 11.07.15                                             **
**          Version: 0.1                                                  **
****************************************************************************/

#ifndef LESOABOUTDIALOG_H
#define LESOABOUTDIALOG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QPicture>
#include <QImage>
#include <QTextEdit>
#include <QTextStream>
#include <QDebug>
#include <QFile>
#include <QTextCursor>

#include "include/osc_global.h"

#define DIALOG_WIDTH 700
#define DIALOG_HEIGHT 400
#define SPACE_POST_OK_BUTTON 650

class LESOAboutDialog : public QDialog
{
    Q_OBJECT

    QTextEdit *aboutLabel;
    QVBoxLayout vLayout;
    QHBoxLayout buttonLayout;
    QPushButton okButton;
    void moveToBeginText();

public:
    LESOAboutDialog(QWidget *parent = 0);
    ~LESOAboutDialog();

    enum UI_LANG { LANG_EN = 0, LANG_RU = 1 };

public slots:
    void show();

public:
     UI_LANG currentLang;
     void setLang(UI_LANG l) { currentLang = l; }
};

#endif // LESOABOUTDIALOG_H
